package com.tg.wls.startup;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

import javax.management.ObjectName;

public class MonServerThread implements Runnable {

	private static int MON_INTERVAL_MS = 1000 * 5;	// default 5sec
	private static String STATE_FILE_PATH = "/tmp/health";
	private static boolean PERF_MON_ENABLE = true;
	
	private String serverName;
	private String serverStats;
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(System.getenv("MON_INTERVAL_MS") != null)
		{
			MON_INTERVAL_MS = Integer.parseInt(System.getenv("MON_INTERVAL_MS"));
		}
		
		if(System.getenv("STATE_FILE_PATH") != null)
		{
			STATE_FILE_PATH = System.getenv("STATE_FILE_PATH");
		}
		
		if(System.getenv("PERF_MON_ENABLE") != null)
		{
			PERF_MON_ENABLE = Boolean.parseBoolean(System.getenv("PERF_MON_ENABLE"));
		}
		
		while(true)
		{
			try
			{
				Thread.sleep(MON_INTERVAL_MS);
				mon();
				if(PERF_MON_ENABLE)
				{
					getPerfMon();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * monitoring wls instance
	 * logging specific text file
	 * @throws Exception
	 */
	public void mon() throws Exception
	{
		ObjectName server = getServerRuntime();
		serverName = (String) OpenShiftStartup.runtimeServer.getAttribute(server, "Name");
		serverStats = (String) OpenShiftStartup.runtimeServer.getAttribute(server, "State");
		if(OpenShiftStartup.DEBUG)
			System.out.println("<------- this server [" + serverName + "] status : [" + serverStats + "] ------------------>");

		
		if("RUNNING".equals(serverStats))
		{
			FileWriter fw = new FileWriter(STATE_FILE_PATH);
			fw.write(serverStats);
			fw.flush();
			fw.close();
		}
		else
		{
			File file = new File(STATE_FILE_PATH);
			if(file.exists())
			{
				file.delete();
			}
		}
	}
	
	/**
	 * get performance monitor for instance
	 * 
	 * 
	 * @throws Exception
	 */
	public void getPerfMon() throws Exception
	{
		ObjectName server = getServerRuntime();
		ObjectName threadPoolRuntime = (ObjectName)OpenShiftStartup.runtimeServer.getAttribute(server, "ThreadPoolRuntime");
		Double throughput = (Double)OpenShiftStartup.runtimeServer.getAttribute(threadPoolRuntime, "Throughput");
		Integer queueLength = (Integer)OpenShiftStartup.runtimeServer.getAttribute(threadPoolRuntime, "QueueLength");
		Integer stuckThreadCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(threadPoolRuntime, "StuckThreadCount");
		Integer executeThreadIdleCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(threadPoolRuntime, "ExecuteThreadIdleCount");
		Integer executeThreadTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(threadPoolRuntime, "ExecuteThreadTotalCount");
		
		ObjectName jvmRuntime = (ObjectName)OpenShiftStartup.runtimeServer.getAttribute(server, "JVMRuntime");
		Long heapFreeCurrent = (Long)OpenShiftStartup.runtimeServer.getAttribute(jvmRuntime, "HeapFreeCurrent");
		Integer heapFreePercent = (Integer)OpenShiftStartup.runtimeServer.getAttribute(jvmRuntime, "HeapFreePercent");
		Long heapSizeCurrent = (Long)OpenShiftStartup.runtimeServer.getAttribute(jvmRuntime, "HeapSizeCurrent");
		Long heapSizeMax = (Long)OpenShiftStartup.runtimeServer.getAttribute(jvmRuntime, "HeapSizeMax");
		
		ObjectName[] applicationRuntimes = (ObjectName[])OpenShiftStartup.runtimeServer.getAttribute(server, "ApplicationRuntimes");
		StringBuffer appMonStr = new StringBuffer();
		for(int i = 0; i < applicationRuntimes.length; i++)
		{
			String name = (String)OpenShiftStartup.runtimeServer.getAttribute(applicationRuntimes[i], "Name");
			if(name != null && (
							name.startsWith("bea_wls_") || 
							name.startsWith("jms-internal-") || 
							name.startsWith("wls-management-") || 
							name.equals("mejb")))
				continue;
			
			ObjectName[] componentRuntimes = (ObjectName[])OpenShiftStartup.runtimeServer.getAttribute(applicationRuntimes[i], "ComponentRuntimes");
			for(int k = 0; k < componentRuntimes.length; k++)
			{
				String type = (String)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "Type");
				if(type.equals("JDBCDataSourceRuntime"))
				{
					String componentName = (String)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "Name");
					String state = (String)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "State");
					Integer ActiveConnectionsAverageCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ActiveConnectionsAverageCount");
					Integer ActiveConnectionsCurrentCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ActiveConnectionsCurrentCount");
					Integer ActiveConnectionsHighCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ActiveConnectionsHighCount");
					Integer CommitOutcomeRetryTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "CommitOutcomeRetryTotalCount");
					Integer ConnectionDelayTime = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ConnectionDelayTime");
					Integer ConnectionsTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ConnectionsTotalCount");
					Integer CurrCapacity = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "CurrCapacity");
					Integer CurrCapacityHighCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "CurrCapacityHighCount");
					Integer FailedRepurposeCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "FailedRepurposeCount");
					Integer FailedReserveRequestCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "FailedReserveRequestCount");
					Integer FailuresToReconnectCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "FailuresToReconnectCount");
					Integer HighestNumAvailable = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "HighestNumAvailable");
					Integer HighestNumUnavailable = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "HighestNumUnavailable");
					Integer LeakedConnectionCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "LeakedConnectionCount");
					Integer NumAvailable = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "NumAvailable");
					Integer NumUnavailable = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "NumUnavailable");
					Integer PrepStmtCacheAccessCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheAccessCount");
					Integer PrepStmtCacheAddCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheAddCount");
					
					Integer PrepStmtCacheCurrentSize = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheCurrentSize");
					Integer PrepStmtCacheDeleteCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheDeleteCount");
					Integer PrepStmtCacheHitCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheHitCount");
					Integer PrepStmtCacheMissCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "PrepStmtCacheMissCount");
					Integer RepurposeCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "RepurposeCount");
					Integer ReserveRequestCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ReserveRequestCount");
					Integer ResolvedAsCommittedTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ResolvedAsCommittedTotalCount");
					Integer ResolvedAsNotCommittedTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ResolvedAsNotCommittedTotalCount");
					Integer UnresolvedTotalCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "UnresolvedTotalCount");
					Integer WaitSecondsHighCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitSecondsHighCount");
					Integer WaitingForConnectionCurrentCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitingForConnectionCurrentCount");
					
					Integer WaitingForConnectionFailureTotal = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitingForConnectionFailureTotal");
					Integer WaitingForConnectionHighCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitingForConnectionHighCount");
					Integer WaitingForConnectionSuccessTotal = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitingForConnectionSuccessTotal");
					Integer WaitingForConnectionTotal = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "WaitingForConnectionTotal");

				}
				else if(type.equals("WebAppComponentRuntime"))
				{
					String componentName = (String)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "ComponentName");
					Integer openSessionsCurrentCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "OpenSessionsCurrentCount");
					Integer openSessionsHighCount = (Integer)OpenShiftStartup.runtimeServer.getAttribute(componentRuntimes[k], "OpenSessionsHighCount");
					
					appMonStr.append("\n[ComponentName]:").append(componentName)
							.append("\t[OpenSessionsCurrentCount]:").append(openSessionsCurrentCount)
							.append("\t[OpenSessionsHighCount]:").append(openSessionsHighCount);
				}
				

			}
		}
		
		
		ObjectName[] serverChannelRuntimes = (ObjectName[])OpenShiftStartup.runtimeServer.getAttribute(server, "ServerChannelRuntimes");
		StringBuffer trafficMonStr = new StringBuffer();
		for(int i = 0; i < serverChannelRuntimes.length; i++)
		{
			String name = (String)OpenShiftStartup.runtimeServer.getAttribute(serverChannelRuntimes[i], "Name");
			if(name.equals("Default[http]"))
			{
				Long bytesReceivedCount = (Long)OpenShiftStartup.runtimeServer.getAttribute(serverChannelRuntimes[i], "BytesReceivedCount"); 
				Long bytesSentCount = (Long)OpenShiftStartup.runtimeServer.getAttribute(serverChannelRuntimes[i], "BytesSentCount"); 
				Long connectionsCount = (Long)OpenShiftStartup.runtimeServer.getAttribute(serverChannelRuntimes[i], "ConnectionsCount"); 
				
				trafficMonStr.append("\n[BytesReceivedCount]:").append(bytesReceivedCount)
						.append("\t[BytesSentCount]:").append(bytesSentCount)
						.append("\t[ConnectionsCount]:").append(connectionsCount);
			}
		}
		
		
		String now = OpenShiftStartup.sdf.format(new Date());
		
		System.out.println("[MONITOR][" + now + "][Server]:" + serverName
				+ appMonStr.toString()
				+ trafficMonStr.toString()
				+ "[Throughput]:" + throughput 
				+ "\t[QueueLength]:" + queueLength
				+ "\t[StuckThreadCount]:" + stuckThreadCount
				+ "\t[ExecuteThreadIdleCount]:" + executeThreadIdleCount
				+ "\t[ExecuteThreadTotalCount]:" + executeThreadTotalCount
				+ "\t[HeapFreeCurrent]:" + heapFreeCurrent
				+ "\t[HeapFreePercent]:" + heapFreePercent
				+ "\t[HeapSizeCurrent]:" + heapSizeCurrent
				+ "\t[HeapSizeMax]:" + heapSizeMax);
	}
	
	
	public ObjectName getServerRuntime() throws Exception 
	{
		return (ObjectName) OpenShiftStartup
				.runtimeServer
				.getAttribute(OpenShiftStartup.runtimeService, "ServerRuntime");
	}

}
