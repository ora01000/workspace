package com.tg.mobile.modules.menu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.tg.jfound.dao.DAOSqlHandler;
import com.tg.jfound.dao.DAOStatementExecutor;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.util.cmnUtil;
import com.tg.jfound.vo.box.Box;
import com.tg.mobile.dao.TMRSCMNDAO;

public class TMRSMENU02  extends DAOSqlHandler{
	
	    /**
     * 
     * @param pconn
     * @param ubox
     * @param input_value
     * @return
     * @throws PException
     */
	public JSONObject selectCpTreeList(Connection pconn,Box ubox, String input_value[]) throws PException{
    	String trxResultCode = "Y";
    	String trxResultMsg = "Sucess";
    	
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        JSONObject jsonObj = new JSONObject();
        ResultSetMetaData rm = null;

		//DAORtnObjBuilder rob = new DAORtnObjBuilder();
		DAOStatementExecutor xe = new DAOStatementExecutor();

		try{

			if(pconn == null){
				conn = getConnection(ubox.getString("db"));
				rs = xe.doExecuteSelectLoggable( conn,  pstmt, rs, ubox,  input_value);			  
			}else{
				rs = xe.doExecuteSelectLoggable( pconn,  pstmt, rs, ubox,  input_value);
			}
			
			jsonObj = trasformJsonCpTree(rs);

        }catch (PException pe){
			Logging.err.println(this.getClass().getName()+"."+"selectListJson()"+"=>\n" + pe+"\n" );
		}finally{
			if(pconn == null){
				try{close(conn, rs,  pstmt);}catch(Exception e){}
			}else{
				try{close( pstmt);}catch(Exception e){}
				try{close( rs);}catch(Exception e){}
			}
        }
        
        jsonObj.put("trxResultCode", trxResultCode);
        jsonObj.put("trxResultMsg", trxResultMsg);

        return jsonObj;
    }
    
	/**
	 * 
     * 
     * @param dataBox
     * @return JSONObject
     * @throws PException
     *    {
        "text" : "SR ?젙蹂닿?由?" ,"id" : 1000 ,"leaf" : false ,"cls" : "folder"
        ,"children" : [
                    {"text" : "SR 愿?由?"            ,"id" : 1100 ,"leaf" : true ,"cls" : "file","url":"/web/pages/sr/tgcssr01.jsp"}
                   ,{"text" : "My SR 愿?由?"         ,"id" : 1200 ,"leaf" : true ,"cls" : "file","url":"/web/pages/sr/tgcssr03.jsp"}
                   ,{"text" : "Support Mail 愿?由?"  ,"id" : 1300 ,"leaf" : true ,"cls" : "file","url":"/web/pages/mail/tgcsmail01.jsp"}
        ]
    },{
        "text" : "SR 泥섎━ ?쁽?솴" ,"id" : 2000 ,"leaf" : false ,"cls" : "folder"
        ,"children" : [
                    {"text" : "SR蹂? 泥섎━ ?쁽?솴"      ,"id" : 2100 ,"leaf" : true ,"cls" : "file","url":"/web/pages/sr/tgcssr02.jsp"}
                   ,{"text" : "?씪蹂? 吏??썝 ?쁽?솴"       ,"id" : 2200 ,"leaf" : true ,"cls" : "file","url":"/web/pages/sr/tgcssr04.jsp"}
        ]
    }
	 */
	public JSONObject trasformJsonCpTree( ResultSet rs) throws PException {
		
		cmnUtil cmnUtil = new cmnUtil();
		
		//boolean 
			
		String t_cp_id = "", t_root_cp_id = "", t_cp_name = "", t_cp_url = "", t_auth_level = "", t_level = "", t_type = "", t_cls  = "";
		String r_cp_id = "", r_root_cp_id = "", r_cp_name = "", r_cp_url = "", r_auth_level = "", r_level = "", r_type = "", r_cls = "";
		boolean t_leaf = true, r_leaf = true;
		
		int b_level = 2;
		int c_level = 2;
		
		
		String errMsg = "";
		JSONObject rtnMsg =  new JSONObject();//"Sucess";

       // Logging.dev.println("< DaoHandler >< RmDataTojson >  Execute Start   ");
    	JSONObject jsonObj   = new JSONObject();
    	JSONObject topFolder   = null;
    	JSONObject subFolder   = null;

    	JSONArray  cplaceArray = new JSONArray();
    	JSONArray  folderArray = new JSONArray();
    	JSONArray  childArray = new JSONArray();
    	
        ResultSetMetaData rm = null;
        
        boolean root_tree = true;

    	try {

    		JSONObject cobj = new JSONObject();
    		String temp_data = "";
    		int row_no = 0;

    		while(rs.next()){
    			
 
                t_cp_id      = rs.getString("CP_ID");
            	t_root_cp_id = rs.getString("CP_ROOT_ID");
            	t_cp_name    = rs.getString("CP_NAME");
            	t_cp_url     = cmnUtil.checkNull2Value(rs.getString("CP_URL"),"");
            	t_level      = rs.getString("LEVEL");
            	t_type       = rs.getString("CP_CONTENTS_TYPE");
            	c_level      = rs.getInt("LEVEL");
            	if(t_type.equals("GRP")){
            		t_cls = "folder";
            		t_leaf = false;

            	}else{
            		t_cls = "file";
            		t_leaf = true;
            	}
            	
                Logging.dev.println("Row No:"+row_no+"==> LEVEL:"+c_level+"/"+b_level+","+"CP TYPE:"+t_type+","+"CP_NAME:"+t_cp_name+","+"CP_ID:"+t_cp_id+","+"ROOT_CP_ID:"+t_root_cp_id+","+"CP_URL:"+t_cp_url);

            	if(c_level == 2){
            		if(rs.isFirst()){
            			Logging.dev.println("Row No:"+row_no);
        				topFolder = createFolderNode(t_cp_name, t_cp_id, t_leaf, t_cls, t_cp_url);
            		}else{
            			if(b_level == 4){
	            			subFolder.put("children", childArray);
		        			childArray.clear();
		        			folderArray.add(subFolder);
        					topFolder.put("children", folderArray);
        				}
        				if( b_level == 3){
        					topFolder.put("children", folderArray);
        				}
        				if(b_level == 2){
        					topFolder.put("children", new JSONArray());
        				}
        				cplaceArray.add(topFolder);
        				Logging.dev.println("Folder Array Clean");
        				Logging.dev.println("Folder Array Clear Before:"+folderArray.toString());
        				folderArray.clear();
        				Logging.dev.println("Folder Array Clean");
        				Logging.dev.println("Folder Array Clear After:"+folderArray.toString());
        				topFolder = createFolderNode(t_cp_name, t_cp_id, t_leaf, t_cls, t_cp_url);
            		}
            		
            	}else if(c_level == 3){
            		
                	if(t_type.equals("GRP")){
	    				if(c_level < b_level){
	            			Logging.dev.println("Row No:"+row_no);
	            			subFolder.put("children", childArray);
		        			childArray.clear();
	    				}
	    				//if(subFolder != null){
	    					//folderArray.add(subFolder);
	    				//}
	        			subFolder = createFolderNode(t_cp_name, t_cp_id, t_leaf, t_cls, t_cp_url);
                	}else{
                		folderArray.add(createLinkNode(t_cp_name, t_cp_id, t_leaf, t_cls, t_cp_url));
                	}
            	
            	}else if(c_level == 4){
            		childArray.add(createLinkNode(t_cp_name, t_cp_id, t_leaf, t_cls, t_cp_url));  
            	}
            	
				Logging.dev.println("Folder Array --------:"+folderArray.toString());

            	row_no++;
            	b_level = c_level;
            	
    		}
    		
 //   		if(b_level == 4)
    		/*
    		subFolder.put("children", childArray);
			folderArray.add(subFolder);
			topFolder.put("children", folderArray);
			folderArray.add(subFolder);
			cplaceArray.add(topFolder);
    		//*/
    		if(b_level == 4){
    			subFolder.put("children", childArray);
    			folderArray.add(subFolder);
				topFolder.put("children", folderArray);
    		}else if(b_level == 3){
    			Logging.dev.println("Row No:"+row_no);
				topFolder.put("children", folderArray);
    		}else if(b_level == 2){
				topFolder.put("children", new JSONArray());

    		}
    		
    		Logging.dev.println("Last Folder Array:"+folderArray.toString());
			cplaceArray.add(topFolder);

    		Logging.dev.println(cplaceArray.toString());

            
            jsonObj.put("DATA_LIST", cplaceArray);

        }catch(SQLException se){
        	errMsg = "<"+this.getClass().getName()+">< trasformJsonTree() > Error: "+se.getMessage();
			Logging.err.println(errMsg);
			rtnMsg.put("responseMsg", errMsg);
			se.printStackTrace();

        }finally{
	        jsonObj.put("success", true);
	        jsonObj.put("data", rtnMsg);
			return jsonObj;
        }
    }
	
	public JSONObject createLinkNode(String cp_name, String cp_id, boolean leaf, String cls, String url){
		JSONObject childObj = new JSONObject();
		childObj.put("text", cp_name);
		childObj.put("id", cp_id);
		childObj.put("leaf",leaf);
		childObj.put("cls", cls);
		childObj.put("url", url);
		Logging.dev.println(childObj.toString());
		return childObj;
	}
	
	
	public JSONObject createFolderNode(String cp_name, String cp_id, boolean leaf, String cls, String url, JSONArray node_array){
		JSONObject folder = new JSONObject();
		folder.put("text"    , cp_name);
		folder.put("id"      , cp_id);
		folder.put("leaf"    , leaf);
		folder.put("cls"     , cls);
		folder.put("url"     , url);
		folder.put("children", node_array);
		return folder;
	}
	public JSONObject createFolderNode(String cp_name, String cp_id, boolean leaf, String cls, String url){
		JSONObject folder = new JSONObject();
		folder.put("text"    , cp_name);
		folder.put("id"      , cp_id);
		folder.put("leaf"    , leaf);
		folder.put("cls"     , cls);
		folder.put("url"     , url);
		return folder;
	}
	
	
	
}
