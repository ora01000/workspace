package com.tg.jfound.web;

/**
 * @(#) BaseAbstractServlet.java
 * @version KDSKIT
 * Copyright
 * All rights reserved.
 * �옉�꽦 :
 * @author 源��룞�떇, dongskim@solupia.co.kr
 *         SOLUPIA e-Biz Team
 *
 */

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tg.jfound.logging.Logging;

/**
 * FRAME WORK�뿉�꽌 �젣怨듬릺�뒗 理쒖긽�쐞 abstract generic servlet �씠�떎. <BR>
 * <BR>
 * �씠 servlet�� form processing�씠 post諛⑹떇�씠�뱺吏� get 諛⑹떇�씠�뱺吏� �긽愿��뾾�씠 catchService() method瑜�
 * �샇異쒗븯�룄濡� �릺�뼱 �엳�떎. <BR>
 * �떎�젣�쟻�씤 catchService()�뒗 Project�쓽 prototype team�뿉�꽌 援ы쁽�븯�뿬�빞 �븷寃껋씠�굹 channel-command
 * pattern�쓣 �궗�슜�븳 defaultServet�쓣 com.servet.channel package�뿉 �옉�꽦�빐 �몢�뿀�쑝�땲 李멸퀬�븯�씪.
 * (DefaultChannelServelt, DefaultMultipartChannelServlet) <BR>
 * �씠 generic servlet�� jsp dispatch瑜� �쐞�븳 method�� 理쒖긽�쐞 html error page method,
 * multipart 寃��궗 method�벑�쓽 湲곕뒫�쓣 媛뽰텛怨� �엳�쑝硫� <BR>
 * channel-command pattern�쓣 �쐞�븳 method�굹 multipart request瑜� 援щ텇�븯湲� �쐞�븳 湲곕뒫�� �뾾�떎�뒗 �젏�뿉
 * �쑀�쓽�븯�뿬�빞 �븳�떎. <BR>
 * <BR>
 * 李멸퀬濡� 紐뉖챺 媛쒕컻�댋(IBM Visual Age for Java)�뿉�꽌�뒗 �씠 LAbstractServet�쓣 �긽�냽諛쏆쑝硫� service()
 * method媛� �깮�꽦�릺湲곕룄 �븯�뒗�뜲, <BR>
 * �씠 service() method�뒗 override�븯硫� �븞�릺怨� 諛섎뱶�떆 吏��썙二쇱뼱�빞 �븳�떎.
 * 
 * @see servlet.channel.DefaultChannelServlet
 * @see servlet.channel.DefaultMultipartChannelServlet
 * 
 */

public abstract class XAsyncAbstractServlet extends HttpServlet {

	private final List nodes = new ArrayList();

	private final String encode;

	/**
	 * BaseAbstractServlet �깮�꽦�옄
	 */
	public XAsyncAbstractServlet() {
		super();
		this.encode = "UTF-8";

	}

	public XAsyncAbstractServlet(String encode) {
		super();
		this.encode = encode;
	}

	/**
	 * BaseAbstractServlet�쓣 �긽�냽諛쏆� servlet�뱾�쓽 entry-point. main()�뿭�븷�쓣 �븯�뒗 �븿�닔. <BR>
	 * BaseAbstractServlet瑜� �긽�냽諛쏆쑝硫� 諛섎뱶�떆 援ы쁽�븯�뿬�빞 �븳�떎. �떎�젣濡� servlet�뿉�꽌 �븷 �씪�뿉 愿��젴�맂 logic�쓣
	 * 援ъ꽦�븳�떎. <BR>
	 * ��遺�遺� form processing�쓣 �쐞�븳 以�鍮꾩옉�뾽 / �떎�젣 logic �샇異� / result page濡� redirect�젙�룄�쓽
	 * 濡쒖쭅�쑝濡� 援ъ꽦�맂�떎.
	 * 
	 * @param req
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletRequest. form processing�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @param res
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletResponse. result redirect�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @return void
	 */
	protected abstract void procXAsyncService(HttpServletRequest req,
			HttpServletResponse res);

	/**
	 * get 諛⑹떇�쓽 form processing�떆�뿉 entry-point濡쒖꽌 �옉�룞�븳�떎. �쁽�옱�뒗 catchService() method濡�
	 * redirect�븳�떎.
	 * 
	 * @param req
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletRequest. form processing�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @param res
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletResponse. result redirect�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @return void
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		procXAsyncService(req, res);
	}

	/**
	 * post 諛⑹떇�쓽 form processing�떆�뿉 entry-point濡쒖꽌 �옉�룞�븳�떎. �쁽�옱�뒗 catchService() method濡�
	 * redirect�븳�떎.
	 * 
	 * @param req
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletRequest. form processing�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @param res
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletResponse. result redirect�쓣 �쐞�븯�뿬 �궗�슜�븳�떎.
	 * @return void
	 */

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		procXAsyncService(req, res);
	}

	/**
	 * content-type�씠 "multipart/form-data"�씤吏�瑜� 寃��궗�븳�떎. <BR>
	 * request.getContentType() method瑜� �궗�슜�븯硫� �씠�븣 援ы빐吏��뒗 臾몄옄�뿴�쓣 �쟻�젅�븳 �겕湲곕줈 �옒�씪�꽌 鍮꾧탳�븯�뒗
	 * logic�쓣 �궗�슜�븳�떎. <BR>
	 * (李멸퀬) html�쓽 form�뿉�꽌 �듅蹂꾪븯寃� enctype�쓣 吏��젙�븯吏� �븡�뒗寃쎌슦 content-Type��
	 * application/x-www-form-urlencoded�씠�떎.
	 * 
	 * @param req
	 *            servlet�뿉�꽌 �쟾�떖諛쏆� HttpServletRequest. �떎�젣�쓽 content type�쓣 援ы븯湲� �쐞�빐
	 *            �궗�슜�븳�떎.
	 * @return boolean "multipart/form-data"�씤寃쎌슦 true, 洹몃젃吏� �븡�쓣寃쎌슦 false.
	 */
	protected boolean isMultipart(HttpServletRequest req) {
		String contentType = null;
		String multipartContentType = "multipart/form-data";
		contentType = req.getContentType();

		return (contentType != null && contentType.length() > 19 && multipartContentType
				.equals(contentType.substring(0, 19))) ? true : false;
	}

	protected void printResponseWrite(HttpServletRequest req,
			HttpServletResponse res, String resStr) {

		res.setHeader("Cache-Control", "no-cache");
		res.setContentType("application/json; charset=" + this.encode);

		PrintWriter pw = null;
		OutputStream os = null;// res.getOutputStream();
		OutputStreamWriter osw = null;// new OutputStreamWriter(os ,
										// this.encode);

		try {
			os = res.getOutputStream();
			osw = new OutputStreamWriter(os, this.encode);
			pw = new PrintWriter(osw);
			System.out.println("-------------------------------------------->" + resStr);
			pw.write(resStr);
			// Logging.debug.println(resStr);

		} catch (Exception e) {
			resStr = "{\"success\":false,\"responseMsg\":\"" + e.getMessage()
					+ "\"}";
			// "success":true,"data":{"responseMsg":"Sucess"},"trxResultCode":"Y","trxResultMsg":"Success To Request!"}
			e.printStackTrace(Logging.err);
			pw.write(resStr);
		} finally {
			pw.flush();
			pw.close();
		}

	}
	
	protected void printResponseWriteContextType(HttpServletRequest req,
			HttpServletResponse res, String resStr, String ContextType) {

		res.setHeader("Cache-Control", "no-cache");
		res.setContentType(ContextType + "; charset=" + this.encode);

		PrintWriter pw = null;
		OutputStream os = null;// res.getOutputStream();
		OutputStreamWriter osw = null;// new OutputStreamWriter(os ,
										// this.encode);
		try {
			os = res.getOutputStream();
			osw = new OutputStreamWriter(os, this.encode);
			pw = new PrintWriter(osw);
			pw.write(resStr);
			// Logging.debug.println(resStr);

		} catch (Exception e) {
			resStr = "{\"success\":false,\"responseMsg\":\"" + e.getMessage()
					+ "\"}";
			// "success":true,"data":{"responseMsg":"Sucess"},"trxResultCode":"Y","trxResultMsg":"Success To Request!"}
			e.printStackTrace(Logging.err);
			pw.write(resStr);
		} finally {
			pw.flush();
			pw.close();
		}

	}

	protected void printErrorWrite(HttpServletRequest req,
			HttpServletResponse res, String customMsg, Exception e) {

		res.setHeader("Cache-Control", "no-cache");
		res.setContentType("application/json; charset=" + this.encode);
		
		PrintWriter pw = null;
		OutputStream os = null;// res.getOutputStream();
		OutputStreamWriter osw = null;// new OutputStreamWriter(os ,
										// this.encode);

		StringBuffer buf = new StringBuffer();
		try {

			buf.append("{\"success\":false,\"responseMsg\":\"");
			buf.append(customMsg + "<br>");
			buf.append(e.getMessage() + "<br>");
			// buf.append("\"},\"DATA_LIST\":[{\"responseMsg\":\"");
			// buf.append(customMsg+"<br>");
			// buf.append(e.getMessage()+"<br>");
			// buf.append("\"}], \"TOTAL_COUNT\":1}");
			buf.append("\"}");

			os = res.getOutputStream();
			osw = new OutputStreamWriter(os, this.encode);
			pw = new PrintWriter(osw);
			System.out.println("----------------------------------->"+buf.toString());
			pw.write(buf.toString());
			

			//Logging.err.println(buf.toString());
			//e.printStackTrace(Logging.err);

		} catch (Exception ex) {
			// buf.append("{\"success\":false,\"data\":{\"responseMsg\":\""+ex.getMessage()+"\"}}");
			buf.append("{\"success\":false,\"responseMsg\":\"" + e.getMessage()
					+ "\"}");
			e.printStackTrace(Logging.err);
			pw.write(buf.toString());
		} finally {
			pw.flush();
			pw.close();
		}

	}
	
	
	protected void printErrorWriteContextType(HttpServletRequest req,
			HttpServletResponse res, String customMsg, String ContextType, Exception e) {

		res.setHeader("Cache-Control", "no-cache");
		res.setContentType(ContextType + "; charset=" + this.encode);
		
		PrintWriter pw = null;
		OutputStream os = null;// res.getOutputStream();
		OutputStreamWriter osw = null;// new OutputStreamWriter(os ,
										// this.encode);

		StringBuffer buf = new StringBuffer();
		try {

			buf.append("{\"success\":false,\"responseMsg\":\"");
			buf.append(customMsg + "<br>");
			buf.append(e.getMessage() + "<br>");
			// buf.append("\"},\"DATA_LIST\":[{\"responseMsg\":\"");
			// buf.append(customMsg+"<br>");
			// buf.append(e.getMessage()+"<br>");
			// buf.append("\"}], \"TOTAL_COUNT\":1}");
			buf.append("\"}");

			os = res.getOutputStream();
			osw = new OutputStreamWriter(os, this.encode);
			pw = new PrintWriter(osw);
			System.out.println("----------------------------------->"+buf.toString());
			pw.write(buf.toString());
			

			//Logging.err.println(buf.toString());
			//e.printStackTrace(Logging.err);

		} catch (Exception ex) {
			// buf.append("{\"success\":false,\"data\":{\"responseMsg\":\""+ex.getMessage()+"\"}}");
			buf.append("{\"success\":false,\"responseMsg\":\"" + e.getMessage()
					+ "\"}");
			e.printStackTrace(Logging.err);
			pw.write(buf.toString());
		} finally {
			pw.flush();
			pw.close();
		}

	}
	
}
