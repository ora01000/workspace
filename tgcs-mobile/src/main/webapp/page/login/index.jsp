<%-- Program info --%>
<%
	/*
	 *  Program Type : JSP
	 *  Program ID   :
	 *
	 *  DESC         :
	 *  Author       :
	 *  Date         :
	 *  Update       :
	 */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box"%>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility"%>

<%
	Box sessionbox = CollectionUtility.getBoxWithSession(request);
	//System.out.println(sessionbox);
	String user_status = sessionbox.getString("U_STATUS");

	String user_id = "";
	String rtn_code = "";
	String rtn_msg = "";
	String err_msg = "";

	if (user_status == null) {

	} else {

		rtn_code = sessionbox.getString("returnCode");
		rtn_msg = sessionbox.getString("returnMsg");

		if (user_status.equals("Y")) {
%>
<META HTTP-EQUIV="refresh"
	CONTENT="0;URL='${pageContext.request.contextPath}/home'">
<%
	} else {
			err_msg = rtn_code + ": " + rtn_msg;
		}

	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Enterprise Solution Team</title>
<link rel="shortcut icon" href="favicon.ico">

<%@ include file="../common/common_script.jsp" %>

</head>
<body>

	<div data-role="page" class="jqm-demos" data-quicklinks="true">


		<form action="${pageContext.request.contextPath}/home" method="post"
			class="ui-body ui-body-a ui-corner-all">

			<!--  Contents  -->
			<div role="main" class="ui-content jqm-content">

				<div class="ui-corner-all custom-corners">
					<div class="ui-bar ui-bar-a">
						<h4>Login</h4>
					</div>
					<div class="ui-body ui-body-a">

						<ul data-role="listview">

							<li data-role="fieldcontain"><label for="name">ID :</label>
								<input type="text" name="name" id="name" value="" /></li>

							<li data-role="fieldcontain"><label for="name">PWD :</label>
								<input name="password" id="password" type="password" value=""
								autocomplete="off"></li>

							<li class="ui-body ui-body-b">

								<fieldset class="ui-grid-a">
									<div class="ui-block-a">
										<input id=submit type="submit" value="Submit" data-theme="a">
									</div>
									<div class="ui-block-b">
										<input id=reset type="reset" value="Reset" data-theme="b">
									</div>
								</fieldset>
							</li>

						</ul>

					</div>
				</div>

			</div>
		</form>

	</div>
</body>
</html>