package com.tg.jfound.proc; 


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tg.jfound.exception.PException;
import com.tg.jfound.proc.msg.ProcessMsg;


public interface XAsyncProcessor 
{ 
    //public ProcessMsg procXAsyncRequest(HttpServletRequest req, HttpServletResponse res) throws PException;
    public void  procXAsyncRequest(HttpServletRequest req, HttpServletResponse res) throws PException;

} 
