<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB00" %>
<%@ page import="net.sf.json.JSONObject" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";

    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }

    }
    TGMOBCMN cmn = new TGMOBCMN(); 
    JSONObject userList = cmn.getUserListOfDept(sessionbox);
    JSONObject supType = cmn.getSupType(sessionbox);
    JSONObject svcType = cmn.getSvcType(sessionbox);
    JSONObject stsType = cmn.getSrStatus(sessionbox);
    String today = DateUtils.getToday("MM/dd/yyyy");
    
    TGMOB01 mob01 = null; 
    JSONObject schedule = null;
    if(!"".equals(sessionbox.getString("MOB_ID"))){
    	mob01 = new TGMOB01(); 
        schedule = mob01.getUserSchedule(sessionbox);	
    }
    
    TGMOB00 mob00 = new TGMOB00(); 
    JSONObject requestJob = mob00.getUserRequest(sessionbox);
    
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Enterprise Solution Team</title>
	
	<%@ include file="../common/common_script.jsp" %>

<script type="text/javascript">

$( document ).ready(function() {
	$.validator.addMethod("specialCk",  function( value, element ) {
		var flag = true;
		var specialChars="~`!@#$%^&*-=+\|[](){};:'<.,>/?_";
		var wordadded = value;
		for (i = 0; i < wordadded.length; i++) {
			for (j = 0; j < specialChars.length; j++) {         
				if (wordadded.charAt(i) == specialChars.charAt(j)){         
					flag=false;
					break;
				}
			}
		}
		return this.optional(element) || flag;
	},'특수문자는 사용할 수 없습니다. '); 
	
	
	//주소창 자동 닫힘 
	$( "#modifyRequest" ).validate({ 
		submitHandler: function( form ) {
			
			var f = confirm("저장하시겠습니까?");
			if(f){
				return true;
			}else{
				return false;
			}
		},
		//규칙
		rules : {
			REQ_TITLE : {
				required : true
				},
			REQ_SITE : {
				required : true,
				maxlength : 10,
				specialCk : true
				},
			REQ_USER : {
				required : true
				},
			REQ_DATE : {
				required : true
				},
			REQ_DESC : {
				maxlength : 1000
				}
		},
		//메세지
		messages : {
			REQ_TITLE : {
				required : "* 필수입력 사항입니다.."
				},
			REQ_SITE : {
				required : "* 필수입력 사항입니다..",
				maxlength : "* 최대 {0}글자이하여야 합니다. ",
				specialCk : "* 특수문자는 사용할 수 없습니다. "
				},
			REQ_USER : {
				required : "* 필수입력 사항입니다.."
				},
			REQ_DATE : {
				required : "* 필수입력 사항입니다.."
			},
			REQ_DESC : {
				maxlength : "* 최대 {0}글자이하여야 합니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});
});

</script>
	
</head>
<body>

<div data-role="page" class="jqm-demos" data-quicklinks="true">

	<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
    <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
    
	<!-- /header -->
	<!-- 
    <div data-role="header" class="jqm-header">
		<p></p>		
        <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
        <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
		<p></p>
    </div>-->
    <!-- /header -->
    <p></p>
	<!--  Contents  -->
	
    <div role="main" class="ui-content jqm-content">
    	
		<c:set var="requestJob" value="<%=requestJob %>" />
    	<form  id="modifyRequest" action="${pageContext.request.contextPath}/page/req/modify_ok.jsp" method="post"  data-ajax="false"
			class="ui-body ui-body-a ui-corner-all">
			
		<!-- 
		<select data-mini="true" data-inline="true" class="ui-btn-left" id="font-size">
	        <option value="16px">Default</option>
	        <option value="10px">10px</option>
	        <option value="11px">11px</option>
	        <option value="12px">12px</option>
	        <option value="13px">13px</option>
	        <option value="14px">14px</option>
	        <option value="15px">15px</option>
	    </select>
    	 -->
		 
    	<div data-role="fieldcontain">
			<label for="REQ_TITLE" data-mini="true">SR제목* :</label>
             <input type="text" name="REQ_TITLE" id="REQ_TITLE" value="${requestJob.DATA_LIST[0].REQ_TITLE}"  data-mini="true" data-clear-btn="true" >
             <!-- ${schedule.DATA_LIST[0].MOB_USER} -->
		 </div>
		 
		 <c:if test="${requestJob.DATA_LIST[0].MOB_ID ne null && requestJob.DATA_LIST[0].MOB_ID != ''}">
    	 <div data-role="fieldcontain">
    	 	<label for="PRE_MOB_ID" data-mini="true">관련일정* :</label>
	    	<font style='font-weight: bold;font-size: 1em;'>
	    	[
	    	${requestJob.DATA_LIST[0].MOB_DATE}
	    	<c:if test="${requestJob.DATA_LIST[0].MOB_SCH eq 'Y'}">오전<c:if test="${requestJob.DATA_LIST[0].MOB_SCH1 eq 'Y' || requestJob.DATA_LIST[0].MOB_SCH2 eq 'Y' || requestJob.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${requestJob.DATA_LIST[0].MOB_SCH1 eq 'Y'}">오후<c:if test="${requestJob.DATA_LIST[0].MOB_SCH2 eq 'Y' || requestJob.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${requestJob.DATA_LIST[0].MOB_SCH2 eq 'Y'}">야간<c:if test="${requestJob.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${requestJob.DATA_LIST[0].MOB_SCH3 eq 'Y'}">철야</c:if>
	    	]${requestJob.DATA_LIST[0].MOB_SITE}(${requestJob.DATA_LIST[0].MOB_USER_NAME})<!-- ${schedule.DATA_LIST[0]} -->
	    	<input type="hidden" name="PRE_MOB_ID" id="PRE_MOB_ID" value="${requestJob.DATA_LIST[0].MOB_ID}" />
	    	</font>
		 </div>
		 </c:if>
		 
		 <div data-role="fieldcontain">
			<label for="REQ_SITE" class="select" data-mini="true">사이트* :</label>
			<input type="search" name="REQ_SITE" id="REQ_SITE" value="${requestJob.DATA_LIST[0].REQ_SITE}"  data-mini="true" />
		</div>
		
		<div data-role="fieldcontain"  class="ui-field-contain">
			<label for="REQ_USER" data-mini="true">요청자* :</label>
             <input type="text" name="REQ_USER" id="REQ_USER" value="${requestJob.DATA_LIST[0].REQ_USER}"  data-mini="true" data-clear-btn="true" >
		 </div>
		 
		 <div data-role="fieldcontain">
			<label for="REQ_DATE" data-mini="true">요청일*:</label>
		    <input type="datetime-local" data-clear-btn="true" name="REQ_DATE" id="REQ_DATE" value="${requestJob.DATA_LIST[0].REQ_DATE_DESC}">
		</div>
		
		<div data-role="fieldcontain">
		<label for="REQ_DESC" data-mini="true">요청내용 :</label>
			<textarea name="REQ_DESC" id="REQ_DESC" data-mini="true"  data-mini="true" >${requestJob.DATA_LIST[0].REQ_DESC}</textarea>
		</div>
		 
		 <div data-role="fieldcontain">
			<label for="SUP_TYPE" class="select" data-mini="true" >지원방식 :</label>
			
			<c:set var="supType" value="<%=supType %>" />
			<select name="SUP_TYPE" id="SUP_TYPE" data-native-menu="false"  data-mini="true"  data-mini="true">
				<option value="  ">미정</option>
				<c:forEach var="result" items="${supType.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"
				<c:if test="${requestJob.DATA_LIST[0].SUP_TYPE eq result.CODE}">selected</c:if>
				><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<div data-role="fieldcontain">
			<label for="SVC_TYPE" class="select" data-mini="true" >서비스방식 :</label>
			<c:set var="svcType" value="<%=svcType %>" />
			<select name="SVC_TYPE" id="SVC_TYPE" data-native-menu="false"  data-mini="true">
				<option value="  ">미정</option>
				<c:forEach var="result" items="${svcType.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"
				<c:if test="${requestJob.DATA_LIST[0].SVC_TYPE eq result.CODE}">selected</c:if>
				><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		
    	<div data-role="fieldcontain">
			<label for="REQ_ENG" class="select" data-mini="true" >담당엔지니어:</label>
			<select name="REQ_ENG" id="REQ_ENG" data-native-menu="false"  data-mini="true">
				<option value="    ">미지정</option>
				<c:set var="userList" value="<%=userList %>" />
				<c:forEach var="result" items="${userList.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.USER_ID}"/>"
				<c:if test="${result.USER_ID eq requestJob.DATA_LIST[0].REQ_ENG}">selected</c:if> 
				><c:out value="${result.USER_NAME}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<div data-role="fieldcontain">
			<label for="REQ_STS" class="select" data-mini="true" >진행상태 :</label>
			
			<c:set var="stsType" value="<%=stsType %>" />
			<select name="REQ_STS" id="REQ_STS" data-native-menu="false"  data-mini="true">
				<option value="  ">미진행</option>
				<c:forEach var="result" items="${stsType.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"
				<c:if test="${requestJob.DATA_LIST[0].REQ_STS eq result.CODE}">selected</c:if>
				><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<div data-role="fieldcontain">
			<label for="REQ_COMP_DATE" data-mini="true">완료일*:</label>
		    <input type="datetime-local" data-clear-btn="true" name="REQ_COMP_DATE" id="REQ_COMP_DATE" value="${requestJob.DATA_LIST[0].REQ_COMP_DATE_DESC}">
		</div>
		 
		<div data-role="fieldcontain">
		<label for="SUP_DESC" data-mini="true">처리내용 :</label>
			<textarea name="SUP_DESC" id="SUP_DESC" data-mini="true"  data-mini="true" >${requestJob.DATA_LIST[0].SUP_DESC}</textarea>
		</div>
		
		<div class="errorTxt"></div>
		<c:choose>
			<c:when test="${( requestJob.DATA_LIST[0].SUP_TYPE == 'IS' || requestJob.DATA_LIST[0].SUP_TYPE == 'RS' || requestJob.DATA_LIST[0].SUP_TYPE == 'PP'  )}">
			<fieldset class="ui-grid-a">
				<div class="ui-block-a">
					<input id=submit type="submit" value="변경" data-theme="a">
				</div>
				<div class="ui-block-b">
					<input id=reset type="reset" value="Reset" data-theme="b">
				</div>
			</fieldset>
			<fieldset class="ui-grid-a">
				<div class="ui-block-a">
					<a class="ui-shadow ui-btn ui-corner-all" href="${pageContext.request.contextPath}/page/main_trans/list.jsp?REQ_ID=${requestJob.DATA_LIST[0].REQ_ID}">일정매핑</a>
				</div>
				<div class="ui-block-b">
					<a class="ui-shadow ui-btn ui-corner-all" href="${pageContext.request.contextPath}/page/req/delete_ok.jsp?REQ_ID=${requestJob.DATA_LIST[0].REQ_ID}">삭제</a>
				</div>
			</fieldset>
			</c:when>
			<c:otherwise>
			<fieldset class="ui-grid-a">
				<div class="ui-block-a">
					<input id=submit type="submit" value="변경" data-theme="a">
				</div>
				<div class="ui-block-b">
					<input id=reset type="reset" value="Reset" data-theme="b">
				</div>
			</fieldset>
			<a class="ui-shadow ui-btn ui-corner-all" href="${pageContext.request.contextPath}/page/req/delete_ok.jsp?REQ_ID=${requestJob.DATA_LIST[0].REQ_ID}">삭제</a>
			</c:otherwise>
		</c:choose>
		
		<input type="hidden" name="REQ_ID" id="REQ_ID" value="${requestJob.DATA_LIST[0].REQ_ID}" />
		
		</form> 
    </div>

    <!--  Contents  -->
    
    <%@ include file="../common/menu.jsp" %>
    
    <%@ include file="../common/footer.jsp" %>
    
    <%@ include file="../common/search_req.jsp" %>    
    
</div>
 
</body>
</html>