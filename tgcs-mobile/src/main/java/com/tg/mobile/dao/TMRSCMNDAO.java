package com.tg.mobile.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.tg.jfound.dao.DAOSqlHandler;
import com.tg.jfound.dao.DAOStatementExecutor;
import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;

public class TMRSCMNDAO extends DAOSqlHandler {

	public ProcessMsg getCodeList(Box sbox) throws PException {
		String mn = "getCodeList";

		Logging.debug.println("<" + this.getClass().getName() + ">< " + mn
				+ "() > execute Starting");

		sbox.put("query", StatementManager.getStatement("tmrsCMNDAO", mn));
		sbox.put("db", "default");

		String input_value[] = { sbox.getString("p_group_id") };

		DaoHandler dao = new DaoHandler();
		JSONObject jsonObj = dao.selectListJson(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName() + " >< " + mn
				+ "() > execute Ended");
		return new ProcessMsg(jsonObj);

	}

	/***
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg getDeptList(Box sbox) throws PException {
		String mn = "getDeptList";

		Logging.debug.println("<" + this.getClass().getName() + ">< " + mn
				+ "() > execute Starting");

		sbox.put("query", StatementManager.getStatement("TMRSCMNDAO", mn));
		sbox.put("db", "default");

		String input_value[] = {};

		DaoHandler dao = new DaoHandler();
		JSONObject jsonObj = dao.selectListJson(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName() + " >< " + mn
				+ "() > execute Ended");
		return new ProcessMsg(jsonObj);

	}

	/***
	 * 遺??꽌蹂? ?궗?슜?옄 ?젙蹂? 議고쉶
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg getUserListOfDept(Box sbox) throws PException {

		String mn = "getUserListOfDept";

		Logging.debug.println("<" + this.getClass().getName() + ">< " + mn
				+ "() > execute Starting");

		sbox.put("query",
				StatementManager.getStatement("TMRSCMNDAO", "" + mn + ""));
		sbox.put("db", "default");

		String input_value[] = { sbox.getString("p_dept_id") };

		DaoHandler dao = new DaoHandler();
		JSONObject jsonObj = dao.selectListJson(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName() + " >< " + mn
				+ "() > execute Ended");
		return new ProcessMsg(jsonObj);

	}

	public JSONObject selectMenuTreeList(Connection pconn, Box ubox,
			String input_value[]) throws PException {
		String trxResultCode = "Y";
		String trxResultMsg = "Sucess";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		JSONObject jsonObj = new JSONObject();
		ResultSetMetaData rm = null;

		// DAORtnObjBuilder rob = new DAORtnObjBuilder();
		DAOStatementExecutor xe = new DAOStatementExecutor();

		try {

			if (pconn == null) {
				conn = getConnection(ubox.getString("db"));
				rs = xe.doExecuteSelectLoggable(conn, pstmt, rs, ubox,
						input_value);
			} else {
				rs = xe.doExecuteSelectLoggable(pconn, pstmt, rs, ubox,
						input_value);
			}

			jsonObj = trasformJsonTree(rs);

		} catch (PException pe) {
			Logging.err.println(this.getClass().getName() + "."
					+ "selectListJson()" + "=>\n" + pe + "\n");
			pe.printStackTrace();
		} finally {
			if (pconn == null) {
				try {
					close(conn, rs, pstmt);
				} catch (Exception e) {
				}
			} else {
				try {
					close(pstmt);
				} catch (Exception e) {
				}
				try {
					close(rs);
				} catch (Exception e) {
				}
			}
		}

		jsonObj.put("trxResultCode", trxResultCode);
		jsonObj.put("trxResultMsg", trxResultMsg);

		return jsonObj;
	}

	/**
	 * List BO ?뜲?씠?꽣瑜? json Data 濡? 蹂??솚
	 * 
	 * @param dataBox
	 * @return JSONObject
	 * @throws PException
	 *             { "text" : "SR ?젙蹂닿?由?" ,"id" : 1000 ,"leaf" : false ,"cls" :
	 *             "folder" ,"children" : [ {"text" : "SR 愿?由?" ,"id" : 1100
	 *             ,"leaf" : true ,"cls" :
	 *             "file","url":"/web/pages/sr/tmrssr01.jsp"} ,{"text" :
	 *             "My SR 愿?由?" ,"id" : 1200 ,"leaf" : true ,"cls" :
	 *             "file","url":"/web/pages/sr/tmrssr03.jsp"} ,{"text" :
	 *             "Support Mail 愿?由?" ,"id" : 1300 ,"leaf" : true ,"cls" :
	 *             "file","url":"/web/pages/mail/tmrsmail01.jsp"} ] },{ "text" :
	 *             "SR 泥섎━ ?쁽?솴" ,"id" : 2000 ,"leaf" : false ,"cls" : "folder"
	 *             ,"children" : [ {"text" : "SR蹂? 泥섎━ ?쁽?솴" ,"id" : 2100 ,"leaf" :
	 *             true ,"cls" : "file","url":"/web/pages/sr/tmrssr02.jsp"}
	 *             ,{"text" : "?씪蹂? 吏??썝 ?쁽?솴" ,"id" : 2200 ,"leaf" : true ,"cls" :
	 *             "file","url":"/web/pages/sr/tmrssr04.jsp"} ] }
	 */
	public JSONObject trasformJsonTree(ResultSet rs) throws PException {

		boolean cflag = false;
		boolean rtnflag = true;

		String t_menu_id = "", t_root_menu_id = "", t_menu_name = "", t_menu_url = "", t_auth_level = "", t_level = "";
		String r_menu_id = "", r_root_menu_id = "", r_menu_name = "", r_menu_url = "", r_auth_level = "", r_level = "";

		String errMsg = "";
		JSONObject rtnMsg = new JSONObject();// "Sucess";

		// Logging.dev.println("< DaoHandler >< RmDataTojson >  Execute Start   ");
		JSONArray jsonArray = new JSONArray();
		JSONArray childArray = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		ResultSetMetaData rm = null;

		try {

			JSONObject cobj = new JSONObject();
			String temp_data = "";
			int row_no = 0;
			
			while (rs.next()) {
				row_no++;
				
				t_menu_id = rs.getString("MENU_ID");
				t_root_menu_id = rs.getString("ROOT_MENU_ID");
				t_menu_name = rs.getString("MENU_NAME");
				t_menu_url = rs.getString("MENU_URL");
				t_auth_level = rs.getString("AUTH_LEVEL");
				t_level = rs.getString("MENU_LEVEL");
				//t_level = rs.getString("LEVEL");
				
		
				if (t_level.equals("2")) {
					
					if (row_no == 1) {
						r_menu_name = t_menu_name;
						r_menu_id = t_menu_id;
						r_menu_url = t_menu_url;
					} else {
						JSONObject root_obj = new JSONObject();
						root_obj.put("text", r_menu_name);
						root_obj.put("id", r_menu_id);
						root_obj.put("leaf", false);
						root_obj.put("cls", "folder");
						root_obj.put("url", r_menu_url);
						root_obj.put("children", childArray);

						r_menu_name = t_menu_name;
						r_menu_id = t_menu_id;
						r_menu_url = t_menu_url;
						jsonArray.add(root_obj);
						childArray.clear();
					}
				} else {
					JSONObject child_obj = new JSONObject();
					child_obj.put("text", t_menu_name);
					child_obj.put("id", t_menu_id);
					child_obj.put("leaf", true);
					child_obj.put("cls", "file");
					child_obj.put("url", t_menu_url);
					childArray.add(child_obj);
				}
			}
			if( childArray.size() > 0){
				JSONObject root_obj = new JSONObject();
				root_obj.put("text", r_menu_name);
				root_obj.put("id", r_menu_id);
				root_obj.put("leaf", false);
				root_obj.put("cls", "folder");
				root_obj.put("url", r_menu_url);
				root_obj.put("children", childArray);
	
				jsonArray.add(root_obj);
				childArray.clear();
			}
			
			System.out.println("childArray : "+ childArray.toString());
			System.out.println("jsonArray : "+ jsonArray.toString());
			jsonObj.put("DATA_LIST", jsonArray);

		} catch (SQLException se) {
			errMsg = "<" + this.getClass().getName()
					+ ">< trasformJsonTree() > Error: " + se.getMessage();
			Logging.err.println(errMsg);
			rtnMsg.put("responseMsg", errMsg);
			se.printStackTrace();

		}
		jsonObj.put("success", rtnflag);
		jsonObj.put("data", rtnMsg);

		return jsonObj;
	}
}
