<%-- Program info --%>
<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB00" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>
<%@ page import="net.sf.json.JSONObject" %>

<%@ page import="java.util.Enumeration" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="com.tg.jfound.logging.Logging" %>
<%@ page import="com.tg.jfound.web.BaseAbstractServlet" %>


<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");
    

    Enumeration params = request.getParameterNames();
	while (params.hasMoreElements()){
	    String name = (String)params.nextElement();
	    Logging.dev.println("===============>>>"+name + " : " +request.getParameter(name));
	}
    
	
    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";

    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
        	
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }
    }
    
    TGMOB01 mob01 = null; 
    JSONObject schedule = null;
    if(!"".equals(sessionbox.getString("MOB_ID"))){
    	mob01 = new TGMOB01(); 
        schedule = mob01.getUserSchedule(sessionbox);	
    }
    
    TGMOB00 mob00 = new TGMOB00(); 
    JSONObject requestList = mob00.getUserRequestList(sessionbox);
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Enterprise Solution Team</title>
	
	<%@ include file="../common/common_script.jsp" %>
<script type="text/javascript">
$(document).ready(function () {
	
	/*
	$.validator.addMethod("specialCk",  function( value, element ) {
		var flag = true;
		var specialChars="~`!@#$%^&*-=+\|[](){};:'<.,>/?_";
		var wordadded = value;
		for (i = 0; i < wordadded.length; i++) {
			for (j = 0; j < specialChars.length; j++) {         
				if (wordadded.charAt(i) == specialChars.charAt(j)){         
					flag=false;
					break;
				}
			}
		}
		return this.optional(element) || flag;
	},'특수문자는 사용할 수 없습니다. ');
	
	
	$.validator.addMethod("checkCk",  function( value, element ) {
		var flag = true;
		if(
			$("#AM").prop("checked") == false &&
			$("#PM").prop("checked") == false &&
			$("#NIGHT").prop("checked") == false &&
			$("#ALLNIGHT").prop("checked") == false
		){
			flag = false ;
		}
		return flag;
	},'[오전][오후][야간][철야]지원을 선택해주세요. ');
	*/
	
	//주소창 자동 닫힘 
	$( "#search-form" ).validate({ 
		submitHandler: function( form ) {
			return true;
		},
		//규칙
		rules : {
			SEARCH_USER_NAME : {
				maxlength : 5
				},
			S_DATE : {
				minlength : 10,
				maxlength : 10
				},
			E_DATE : {
				minlength : 10,
				maxlength : 10
				},
			SEARCH_MOB_DESC : {
				maxlength : 250
				}
		},
		//메세지
		messages : {
			SEARCH_USER_NAME : {
				maxlength : " 최대 {0}글자이하여야 합니다. "
				},
			S_DATE : {
				minlength : " 최소 {0}글자이상이여야 합니다. ",
				maxlength : " 최대 {0}글자이하여야 합니다. "
			},
			E_DATE : {
				minlength : " 최소 {0}글자이상이여야 합니다. ",
				maxlength : " 최대 {0}글자이하여야 합니다. "
			},
			SEARCH_MOB_DESC : {
				maxlength : " 최대 {0}글자이하여야 합니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});	
});
</script>	
	
</head>
<body>

<div data-role="page" class="jqm-demos" data-quicklinks="true">

	<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
    <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	<!-- /header -->
	<!-- 
    <div data-role="header" class="jqm-header">
		<p></p>		
        <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
        <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
		<p></p>
    </div>-->
    <!-- /header -->
     
    <p></p>
    
	<!--  Contents  -->
    <div role="main" class="ui-content jqm-content">
    	<c:set var="schedule" value="<%=schedule %>" />
    	<c:if test="${schedule.DATA_LIST[0].MOB_ID ne null}">
    	<font style='color: #ff0000; font-weight: bold;font-size: 1em;'>
    	[
    	${schedule.DATA_LIST[0].MOB_DATE}
    	<c:if test="${schedule.DATA_LIST[0].MOB_SCH eq 'Y'}">오전<c:if test="${schedule.DATA_LIST[0].MOB_SCH1 eq 'Y' || schedule.DATA_LIST[0].MOB_SCH2 eq 'Y' || schedule.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${schedule.DATA_LIST[0].MOB_SCH1 eq 'Y'}">오후<c:if test="${schedule.DATA_LIST[0].MOB_SCH2 eq 'Y' || schedule.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${schedule.DATA_LIST[0].MOB_SCH2 eq 'Y'}">야간<c:if test="${schedule.DATA_LIST[0].MOB_SCH3 eq 'Y'}">,</c:if></c:if><c:if test="${schedule.DATA_LIST[0].MOB_SCH3 eq 'Y'}">철야</c:if>
    	]${schedule.DATA_LIST[0].MOB_SITE}<!-- ${schedule.DATA_LIST[0]} -->
    	</font>
		</c:if>
		<div data-role="fieldcontain">
		<table class="ui-body-d ui-shadow table-stripe ui-responsive" id="table-custom-4" data-role="table" data-mode="columntoggle" data-column-popup-theme="a" data-column-btn-text="Columns" data-column-btn-theme="b">
		 
         <thead>
           <tr class="ui-bar-d th-groups">
             <th data-priority="1">사이트</th>
             <th data-priority="1">요청일</th>
             <th data-priority="1">내용</th>
             <th data-priority="1">담당자</th>
             <th data-priority="1">상태</th>
           </tr>
         </thead>
         <tbody>
		 <c:set var="requestList" value="<%=requestList %>" />
         <c:forEach var="result" items="${requestList.DATA_LIST}" varStatus="status" >

		 <tr class="tr-row<c:if test="${status.index mod 2 eq 0}">-w</c:if>">
             <td><a href="${pageContext.request.contextPath}/page/req_trans/detail.jsp?REQ_ID=${result.REQ_ID}&MOB_ID=${schedule.DATA_LIST[0].MOB_ID}" data-rel="external">${result.REQ_SITE}</a></td>
             <!-- <td><a href="#" data-rel="external">${result.MOB_DATE}</a></td> -->
             <td>${result.REQ_DATE}</td>
             <td>${result.REQ_TITLE}</td>
             <td>${result.REQ_USER}</td>
             <td>${result.STS_TYPE_NAME}</td>
          </tr>
         </c:forEach>
           
         </tbody>
       </table>
       
       </div>
	 
    </div>
    <!--  Contents  -->
    
    <%@ include file="../common/menu.jsp" %>
    
    <%@ include file="../common/footer.jsp" %>
    
    <%@ include file="../common/search_req.jsp" %>
    
</div>
 
</body>
</html>