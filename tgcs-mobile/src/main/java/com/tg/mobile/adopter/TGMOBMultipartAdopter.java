package com.tg.mobile.adopter;

import java.io.File;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


import com.tg.jfound.config.AppConfigException;
import com.tg.jfound.config.AppConfigManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.proc.XAsyncStrProcessor;
import com.tg.jfound.util.cmnUtil;
import com.tg.jfound.vo.box.Box;
import com.tg.jfound.vo.box.CollectionUtility;
import com.tg.jfound.web.ExtMultipartRequest;
import com.tg.jfound.web.XAsyncAbstractServlet;
import com.tg.jfound.web.channel.CommandException;
import com.tg.jfound.web.channel.ContentTypeException;
import com.tg.mobile.proc.XAsyncWebRequestProcessor;
import com.tg.mobile.vo.FileVO;


/***
 * 
 * 
 * @author Jonathan Cho
 *
 */
public class TGMOBMultipartAdopter extends XAsyncAbstractServlet {
	
	private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
    
    // location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "upload_files";
    private static final String uploadDirConf = "webApp.upload.dir";
    
	/**
	 * MultipartAdapter constructor comment.
	 */
    
	public TGMOBMultipartAdopter() {
		super();
	}

	
	protected void procXAsyncService(javax.servlet.http.HttpServletRequest req,	javax.servlet.http.HttpServletResponse res) {

		String resStr = ".";
		cmnUtil cmnUtil = new cmnUtil();
		
		try {
            Logging.debug.println("< MultipartAdapter > catchService Initiated by ["+ req.getRemoteAddr() + "]");
            Logging.debug.println("< MultipartAdapter > req.getCharacterEncoding :"+ req.getCharacterEncoding());
            Logging.debug.println("< MultipartAdapter > Request Contet Type: "+req.getContentType());
            Logging.debug.println("< MultipartAdapter > Contents Lengh: "+req.getContentLength());
            
            
            Box sbox = new Box();
            Box sessionbox = CollectionUtility.getBoxWithSession(req);
            
            sbox.put("email_id", sessionbox.get("EMAIL_ID"));
            sbox.put("user_nm", sessionbox.get("USER_NM"));
            sbox.put("company_nm", sessionbox.get("COMPANY_NM"));
            sbox.put("dept_nm", sessionbox.get("DEPT_NM"));
            
            AppConfigManager conf = AppConfigManager.getInstance();
			String rootFileUploadDir = conf.get(uploadDirConf);//+targetDir;
            
			DiskFileItemFactory factory=new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            
            // sets memory threshold - beyond which files are stored in disk
            //factory.setSizeThreshold(MEMORY_THRESHOLD);
            // sets temporary location to store files
            //factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
            //factory.setRepository(new File("C:\\Users\\hp\\Downloads"));
             
            // sets maximum size of upload file
            //upload.setFileSizeMax(MAX_FILE_SIZE);
            // sets maximum size of request (include file + form data)
            //upload.setSizeMax(MAX_REQUEST_SIZE);
            // constructs the directory path to store upload file
            // this path is relative to application's directory
            
            if (ServletFileUpload.isMultipartContent(req)){
            	
            	List<FileItem> formItems = upload.parseRequest(req);
            	String uploadPath = getServletContext().getRealPath("")
                        + File.separator + UPLOAD_DIRECTORY;
                
                if (formItems != null && formItems.size() > 0) {
                    // iterates over form's fields
                    for (FileItem item : formItems) {
                    	
                        // processes only fields that are not form fields
                        if (!item.isFormField()) {

                            String org_file = new File(item.getName()).getName();
                            if(!"".equals(org_file) && item.getSize() > 0){
                            	
                            	String itemName = item.getFieldName();
                            	String itemContentType = item.getContentType();
	                            String prefixname = cmnUtil.getCurrentTimeNoDash(); 
	                            String sys_file =prefixname + "_" + org_file;
	                            String timeDir = cmnUtil.getCurrYear()+cmnUtil.getCurrMonth();
	                            long itemSize = item.getSize();
	                            
	                         // creates the directory if it does not exist
	                            File uploadDir = new File(uploadPath + File.separator + timeDir);
	                            if (!uploadDir.exists()) {
	                                uploadDir.mkdir();
	                            }
	                            
	                            String filePath = uploadDir.getAbsolutePath() + File.separator+ sys_file;
	                            File storeFile = new File(filePath);
	                            
	                            FileVO fvo = new FileVO();
	                            fvo.setName(itemName);
	                            fvo.setValue(org_file);
	                            fvo.setFileName(org_file);
	                            fvo.setContentType(itemContentType);
	                            fvo.setOrgFileName(org_file);
	                            fvo.setSysFileName(sys_file);
	                            fvo.setFilePath(uploadDir.getAbsolutePath());
	                            fvo.setFileLength(itemSize);
	                            sbox.put(itemName, fvo);
	     
	                            // saves the file on disk
	                            item.write(storeFile);
                            }
                        }else{
                        	sbox.put(item.getFieldName(), item.getString() );
                        }
                    }
                }
                
                XAsyncStrProcessor proc = (XAsyncStrProcessor) new XAsyncWebRequestProcessor();
				resStr = proc.procXAsyncMultipartRequest(sbox);
				
				printResponseWriteContextType(req,res,resStr,"text/html");
				Logging.debug.println("< MultipartAdapter > <procXAsyncService> Completed by ["+req.getRemoteAddr()+"]");
            	
            }else{			
				throw new ContentTypeException("fail in MultipartAdapter : ContentType Incorrect. Use normal Servlet not Multipart servlet");
			}
            
		} catch (Exception e) {
			e.printStackTrace();
			Logging.err.println("< MultipartAdapter > Catche the Occurred Exception While User IP ["+ req.getRemoteAddr() + "]\n" + e);
			printErrorWriteContextType(req,res,"<Application Error>", "text/html",e);
		}

	}
}
