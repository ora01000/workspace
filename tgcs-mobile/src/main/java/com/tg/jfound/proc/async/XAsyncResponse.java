package com.tg.jfound.proc.async; 



/**
 * @(#) XAsyncInterface.java
 */
public interface XAsyncResponse {

	public String createResponseXml();

}