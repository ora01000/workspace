<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>
<%@ page import="net.sf.json.JSONObject" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";

    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }

    }
    TGMOBCMN cmn = new TGMOBCMN(); 
    JSONObject userList = cmn.getUserListOfDept(sessionbox);
    JSONObject MOB_TYPE = cmn.getPjtType(sessionbox);
    JSONObject srStatus = cmn.getSrStatus(sessionbox);
    
    TGMOB01 mob01 = new TGMOB01(); 
    JSONObject schedule = mob01.getUserLongSchedule(sessionbox);
    
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Enterprise Solution Team</title>
	
	<%@ include file="../common/common_script.jsp" %>

<script type="text/javascript">
/*
$("#font-size").on("change", function () {
	alert('font-size');
    $("#contents *").css({
        "font-size": $(this).val()
    });
});
*/
$(document).ready(function () {
	
	$.validator.addMethod("specialCk",  function( value, element ) {
		var flag = true;
		var specialChars="~`!@#$%^&*-=+\|[](){};:'<.,>/?_";
		var wordadded = value;
		for (i = 0; i < wordadded.length; i++) {
			for (j = 0; j < specialChars.length; j++) {         
				if (wordadded.charAt(i) == specialChars.charAt(j)){         
					flag=false;
					break;
				}
			}
		}
		return this.optional(element) || flag;
	},'특수문자는 사용할 수 없습니다. '); 
	
	//주소창 자동 닫힘 
	$( "#modifySchedule" ).validate({ 
		submitHandler: function( form ) {
			
			var f = confirm("저장하시겠습니까?");
			if(f){
				return true;
			}else{
				return false;
			}
		},
		//규칙
		rules : {
			MOB_USER : {
				required : true
				},
			MOB_SITE : {
				required : true,
				maxlength : 10,
				specialCk : true
				},
				MOB_SDATE : {
				required : true
				},
				MOB_EDATE : {
				required : true
				},
			MOB_TYPE : {
				required : true
				},
			MOB_STS : {
				required : true
				},
			MOB_DESC : {
				maxlength : 250
				}
		},
		//메세지
		messages : {
			MOB_USER : {
				required : "* 필수입력 사항입니다.."
				},
			MOB_SITE : {
				required : "* 필수입력 사항입니다..",
				maxlength : "* 최대 {0}글자이하여야 합니다. ",
				specialCk : "* 특수문자는 사용할 수 없습니다. "
				},
			MOB_SDATE : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_EDATE : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_TYPE : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_STS : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_DESC : {
				maxlength : "* 최대 {0}글자이하여야 합니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});	
}); 
</script>
	
</head>
<body>

<div data-role="page" class="jqm-demos" data-quicklinks="true">

	<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
    <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
     
	<!-- /header -->
	<!-- 
    <div data-role="header" class="jqm-header">
		<p></p>		
        <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
        <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
		<p></p>
    </div>-->
    <!-- /header -->
    <p></p>
	<!--  Contents  -->
	
    <div role="main" class="ui-content jqm-content">
    	
	    
		<c:set var="schedule" value="<%=schedule %>" />
    	<form  id="modifySchedule" action="${pageContext.request.contextPath}/page/longterm/modify_ok.jsp" method="post"  data-ajax="false"
			class="ui-body ui-body-a ui-corner-all">
			
		<!-- 
		<select data-mini="true" data-inline="true" class="ui-btn-left" id="font-size">
	        <option value="16px">Default</option>
	        <option value="10px">10px</option>
	        <option value="11px">11px</option>
	        <option value="12px">12px</option>
	        <option value="13px">13px</option>
	        <option value="14px">14px</option>
	        <option value="15px">15px</option>
	    </select>
    	 -->
    	<div data-role="fieldcontain">
			<label for="MOB_USER" class="select" data-mini="true" >담당자*:</label>
			<select name="MOB_USER" id="MOB_USER" data-native-menu="false"  data-mini="true" value="${schedule.DATA_LIST[0].MOB_USER}">
			<c:set var="userList" value="<%=userList %>" />
				<c:forEach var="result" items="${userList.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.USER_ID}"/>"
				<c:if test="${result.USER_ID eq schedule.DATA_LIST[0].MOB_USER}">selected</c:if> 
				><c:out value="${result.USER_NAME}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<!-- 
		<div data-role="fieldcontain">
			<label for="select-choice-1" class="select" data-mini="true">사이트*:</label>
			<select name="select-choice-1" id="select-choice-1"  data-mini="true">
				<option value=""></option>
				<option value="삼성화재">삼성화재</option>
				<option value="삼성생명">삼성생명</option>
				<option value="호텔신라">호텔신라</option>
				<option value="OGG교육">OGG교육</option>
			</select>
		</div>
		 -->
		 
		<div data-role="fieldcontain">
			<label for="MOB_SITE" class="select" data-mini="true">사이트* :</label>
			<input type="search" name="MOB_SITE" id="MOB_SITE" data-mini="true" value="${schedule.DATA_LIST[0].MOB_SITE}" />
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_SDATE">시작일*:</label>
     		<input type="date" data-clear-btn="true" name="MOB_SDATE" id="MOB_SDATE" value="${schedule.DATA_LIST[0].MOB_SDATE}" data-mini="true" >
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_EDATE">종료일*:</label>
     		<input type="date" data-clear-btn="true" name="MOB_EDATE" id="MOB_EDATE" value="${schedule.DATA_LIST[0].MOB_EDATE}" data-mini="true" >
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_TYPE" class="select" data-mini="true" >프로젝트* :</label>
			
			<c:set var="MOB_TYPE" value="<%=MOB_TYPE %>" />
			<select name="MOB_TYPE" id="MOB_TYPE" data-native-menu="false"  data-mini="true" value="${schedule.DATA_LIST[0].MOB_TYPE}">
				<c:forEach var="result" items="${MOB_TYPE.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"
				<c:if test="${schedule.DATA_LIST[0].MOB_TYPE eq result.CODE}">selected</c:if>
				><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_STS" class="select" data-mini="true" >상태* :</label>
			<c:set var="srStatus" value="<%=srStatus %>" />
			<select name="MOB_STS" id="MOB_STS" data-native-menu="false"  data-mini="true">
				<c:forEach var="result" items="${srStatus.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"
				<c:if test="${schedule.DATA_LIST[0].MOB_STS eq result.CODE}">selected</c:if>
				><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		 
		<div data-role="fieldcontain">
			<label for="MOB_DESC" data-mini="true">내용 :</label>
             <input type="text" name="MOB_DESC" id="MOB_DESC" value="${schedule.DATA_LIST[0].MOB_DESC}">
		</div>
		
		<div class="errorTxt"></div>

		<fieldset class="ui-grid-a">
			<div class="ui-block-a">
				<input id=submit type="submit" value="변경" data-theme="a">
			</div>
			<div class="ui-block-b">
				<input id=reset type="reset" value="Reset" data-theme="b">
			</div>
		</fieldset>
		
		<a class="ui-shadow ui-btn ui-corner-all" href="${pageContext.request.contextPath}/page/longterm/delete_ok.jsp?MOB_ID=${schedule.DATA_LIST[0].MOB_ID}">삭제</a>
		<input type="hidden" name="MOB_ID" id="MOB_ID" value="${schedule.DATA_LIST[0].MOB_ID}" />
		
		</form> 
    </div>

    <!--  Contents  -->
    
    <%@ include file="../common/menu.jsp" %>
    
    <%@ include file="../common/footer.jsp" %>
    
    <%@ include file="../common/search_longterm.jsp" %>    
    
</div>
 
</body>
</html>