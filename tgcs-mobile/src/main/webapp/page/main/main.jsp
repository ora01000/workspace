<%-- Program info --%>
<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>
<%@ page import="net.sf.json.JSONObject" %>


<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/custom.tld"%>

<%

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");
    String search_date = sessionbox.getString("SEARCH_DATE");

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";
    
    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }
    }
    
    TGMOBCMN cmn = new TGMOBCMN(); 
    JSONObject userList = cmn.getUserListOfDept(sessionbox);
    
    TGMOB01 mob01 = new TGMOB01(); 
    Map scheduleMap = mob01.getUserMainScheduleList(sessionbox);
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
	<title>Enterprise Solution Team</title>
	<%@ include file="../common/common_script.jsp" %>

<script type="text/javascript">
$(document).ready(function () {
	
	/*
	$.validator.addMethod("specialCk",  function( value, element ) {
		var flag = true;
		var specialChars="~`!@#$%^&*-=+\|[](){};:'<.,>/?_";
		var wordadded = value;
		for (i = 0; i < wordadded.length; i++) {
			for (j = 0; j < specialChars.length; j++) {         
				if (wordadded.charAt(i) == specialChars.charAt(j)){         
					flag=false;
					break;
				}
			}
		}
		return this.optional(element) || flag;
	},'특수문자는 사용할 수 없습니다. ');
	
	
	$.validator.addMethod("checkCk",  function( value, element ) {
		var flag = true;
		if(
			$("#AM").prop("checked") == false &&
			$("#PM").prop("checked") == false &&
			$("#NIGHT").prop("checked") == false &&
			$("#ALLNIGHT").prop("checked") == false
		){
			flag = false ;
		}
		return flag;
	},'[오전][오후][야간][철야]지원을 선택해주세요. ');
	*/
	
	//주소창 자동 닫힘 
	$( "#search-form" ).validate({ 
		submitHandler: function( form ) {
			return true;
		},
		//규칙
		rules : {
			SEARCH_USER_NAME : {
				maxlength : 5
				},
			S_DATE : {
				minlength : 10,
				maxlength : 10
				},
			E_DATE : {
				minlength : 10,
				maxlength : 10
				},
			SEARCH_MOB_DESC : {
				maxlength : 250
				}
		},
		//메세지
		messages : {
			SEARCH_USER_NAME : {
				maxlength : " 최대 {0}글자이하여야 합니다. "
				},
			S_DATE : {
				minlength : " 최소 {0}글자이상이여야 합니다. ",
				maxlength : " 최대 {0}글자이하여야 합니다. "
			},
			E_DATE : {
				minlength : " 최소 {0}글자이상이여야 합니다. ",
				maxlength : " 최대 {0}글자이하여야 합니다. "
			},
			SEARCH_MOB_DESC : {
				maxlength : " 최대 {0}글자이하여야 합니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});	
});
</script>
	
	
</head>

<body>


<div data-role="page" class="jqm-demos" data-quicklinks="true">

	<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
    <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
    
	<!-- /header -->
	<!-- 
    <div data-role="header" class="jqm-header">
		<p></p>		
        <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
        <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
		<p></p>
    </div>-->
    <!-- /header -->
    
	<!--  Contents  -->
    <div role="main" class="ui-content jqm-content">
		
		<div data-role="fieldcontain">
		<table class="ui-body-d ui-shadow table-stripe ui-responsive" id="table-custom-4" data-role="table" data-mode="columntoggle" data-column-popup-theme="a" data-column-btn-text="Columns" data-column-btn-theme="b">
		 
         <thead>
           <tr class="ui-bar-d th-groups">
             <th data-priority="2">직급</th>
             <th data-priority="1">이름</th>
             <th data-priority="1">오전</th>
             <th data-priority="1">오후</th>
             <th data-priority="1">야간(철야)</th>
           </tr>
         </thead>
         <tbody>
         
         <c:set var="userListData" value="<%=userList %>" />
         <c:forEach var="result" items="${userListData.DATA_LIST}" varStatus="status" >
           <tr class="tr-row<c:if test="${status.index mod 2 eq 0}">-w</c:if>">
             <td>${result.POSITION_NAME}</td>
             <td>${result.U_NAME}</td>
			
 			 <c:set var="map" value="<%=scheduleMap%>" />
 			 <c:set var="key" value="${result.USER_ID}" />
 			 <c:set var="mob_key" value="${map[key]}" />
             <c:if test="${mob_key eq null}">
	             <td><ct:weekvalue idate="<%=search_date %>" /></td>
	             <td><ct:weekvalue idate="<%=search_date %>" /></td>
	             <td></td>
             </c:if>
             <c:if test="${mob_key ne null}">
             	<!-- style="background-color: #fae100" -->
             	<c:choose>

				    <c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB ne null}">
				       	<td style="background-color: #fae100" colspan=3 >
				       	<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
				       	<c:if test="${mob_key.MOB3 ne null}">(철야)</c:if></td>
			    	</c:when>
			    	<c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB ne null && mob_key.MOB ne null}">
			    		<td style="background-color: #fae100" colspan=3 >
			    		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
			    	</c:when>
			    	<c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB ne null}">
			    		<td style="background-color: #fae100" colspan=2 >
			    		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			    		</td>
				       	<c:choose>
					    <c:when test="${mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB2 ne null}">
				       		<td style="background-color: #fae100" >
				       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />(철야)
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<c:choose>
						    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB2 ne null}">
					       		<td style="background-color: #fae100" >
					       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
					       		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
					    	</c:when>
					    	<c:otherwise>
					    		<td></td>
					    	</c:otherwise>
					    	</c:choose>
				    	
				    	</c:otherwise>
				    	</c:choose>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB1 ne null }">
			       	
			       	<c:choose>
				    <c:when test="${mob_key.MOB ne null}">
			       		<td style="background-color: #fae100" >
			       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			       		</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
			    	</c:otherwise>
			    	</c:choose>
			    	
			    	<c:choose>
				    <c:when test="${mob_key.MOB1 ne null}">
			       		<td style="background-color: #fae100" colspan=2 >
			       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />(철야)
			       		</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
			    	</c:otherwise>
			    	</c:choose>
			       	
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB1 ne null}">
			       	
			    	<c:choose>
				    <c:when test="${mob_key.MOB ne null}">
			       		<td style="background-color: #fae100" >
			       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			       		</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
			    	</c:otherwise>
			    	</c:choose>
				    	
			       	<td style="background-color: #fae100" colspan=2 >
			       	<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
			       	<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB2 == mob_key.MOB3}">
			    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB ne null}">
				       		<td style="background-color: #fae100" >
				       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB1 ne null}">
				       		<td style="background-color: #fae100" >
				       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB3 ne null}">
					    	
					    	<c:choose>
						    <c:when test="${mob_key.MOB2 == mob_key.MOB3}">
					       		<td style="background-color: #fae100">
						    	<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
						    	<c:if test="${mob_key.MOB3 ne null}">(철야)</c:if>
						    	</td>
					    	</c:when>
					    	<c:otherwise>
					    		<td style="background-color: #fae100">
						    	<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
						    	<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if>
						    	</td>
					    	</c:otherwise>
					    	</c:choose>
					    	
				    	</c:when>
				    	<c:otherwise>
				    		<c:choose>
					    	<c:when test="${mob_key.MOB2 ne null || mob_key.MOB3 ne null}">
				    		<td  style="background-color: #fae100">
				    		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
				    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if>
				    		</td>
				    		</c:when>
				    		<c:otherwise>
				    		<td>
				    		</td>
				    		</c:otherwise>
				    		</c:choose>
				    	</c:otherwise>
				    	</c:choose>
			       	
			    	</c:when>
			    	
				    <c:otherwise>
				    
				    	<c:choose>
					    <c:when test="${mob_key.MOB ne null}">
				       		<td style="background-color: #fae100" >
				       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB1 ne null}">
				       		<td style="background-color: #fae100" >
				       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="<%=search_date %>" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	 
				    	<c:choose>
					    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB3 ne null}">
					    	<td style="background-color: #fae100">
					    	<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
					    	<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
				    	</c:when>
				    	<c:otherwise>
				    		<td style="background-color: #fae100">
				    		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
				    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
				    	</c:otherwise>
				    	</c:choose>
				    </c:otherwise>
				</c:choose>
					              
             </c:if>
           </tr>
         </c:forEach>

         </tbody>
       </table>
	 
	 </div>
    </div>
    <!--  Contents  -->
    
    <%@ include file="../common/menu.jsp" %>
    
    <%@ include file="../common/search.jsp" %>
    
    <!--  Footer -->
<% 
	String next_date = "";
	String prev_date = "";
	Date currentDate = null;
	SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
    if("".equals(search_date)){
		currentDate = new Date ();
		search_date = formatter.format ( currentDate ); 
	}
    DateFormat currentFormat = new SimpleDateFormat("yyyyMMdd");
    currentDate = currentFormat.parse(search_date);
    
    Date nextDate = new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 ) );
    Date prevDate = new Date ( currentDate.getTime ( ) - (long) ( 1000 * 60 * 60 * 24 ) );
    
    next_date = formatter.format ( nextDate );
    prev_date = formatter.format ( prevDate );
    
%>
	<div data-role="footer" data-position="fixed">
	<h1>
	<a href="${pageContext.request.contextPath}/page/main/main.jsp?SEARCH_DATE=<%=prev_date %>" title="이전">이전</a>
	| <%=search_date.substring(0,4) %>-<%=search_date.substring(4,6) %>-<%=search_date.substring(6,8) %> | 
	<a href="${pageContext.request.contextPath}/page/main/main.jsp?SEARCH_DATE=<%=next_date %>" title="다음">다음</a></h1>
	</div>
	<!--  Footer -->    
    
</div>
 
</body>
</html>