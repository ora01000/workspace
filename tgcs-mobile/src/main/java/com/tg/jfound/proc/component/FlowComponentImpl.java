package com.tg.jfound.proc.component;

import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;

import org.jdom.Document;

public interface FlowComponentImpl{

    public ProcessMsg executeFlow(Box msg) throws Exception;

}
