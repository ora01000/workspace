<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.util.DateUtils" %>
<%@ page import="net.sf.json.JSONObject" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    //System.out.println(sessionbox);
    String user_status = sessionbox.getString("U_STATUS");

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";

    if(user_status == null){

    }else{
        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }
    }
    TGMOBCMN cmn = new TGMOBCMN(); 
    JSONObject userList = cmn.getUserListOfDept(sessionbox);
    JSONObject srType = cmn.getSrType(sessionbox);
    JSONObject srStatus = cmn.getSrStatus(sessionbox);
    String today = DateUtils.getToday("MM/dd/yyyy");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Enterprise Solution Team</title>
	
	<%@ include file="../common/common_script.jsp" %>

<script type="text/javascript">
/*
$("#font-size").on("change", function () {
	alert('font-size');
    $("#contents *").css({
        "font-size": $(this).val()
    });
});
*/

$(document).ready(function () {
	
	$.validator.addMethod("specialCk",  function( value, element ) {
		var flag = true;
		var specialChars="~`!@#$%^&*-=+\|[](){};:'<.,>/?_";
		var wordadded = value;
		for (i = 0; i < wordadded.length; i++) {
			for (j = 0; j < specialChars.length; j++) {         
				if (wordadded.charAt(i) == specialChars.charAt(j)){         
					flag=false;
					break;
				}
			}
		}
		return this.optional(element) || flag;
	},'특수문자는 사용할 수 없습니다. '); 
	
	$.validator.addMethod("checkCk",  function( value, element ) {
		var flag = true;
		if(
			$("#AM").prop("checked") == false &&
			$("#PM").prop("checked") == false &&
			$("#NIGHT").prop("checked") == false &&
			$("#ALLNIGHT").prop("checked") == false
		){
			flag = false ;
		}
		return flag;
	},'[오전][오후][야간][철야]지원을 선택해주세요. ');
	
	//주소창 자동 닫힘 
	$( "#inputSchedule" ).validate({ 
		submitHandler: function( form ) {
			
			var f = confirm("저장하시겠습니까?");
			if(f){
				return true;
			}else{
				return false;
			}
		},
		//규칙
		rules : {
			MOB_USER : {
				required : true
				},
			MOB_SITE : {
				required : true,
				maxlength : 10,
				specialCk : true
				},
			MOB_DATE : {
				required : true
				},
			MOB_TYPE : {
				required : true
				},
			MOB_STS : {
				required : true
				},
			MOB_DESC : {
				maxlength : 250
				}
		},
		//메세지
		messages : {
			MOB_USER : {
				required : "* 필수입력 사항입니다.."
				},
			MOB_SITE : {
				required : "* 필수입력 사항입니다..",
				maxlength : "* 최대 {0}글자이하여야 합니다. ",
				specialCk : "* 특수문자는 사용할 수 없습니다. "
				},
			MOB_DATE : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_TYPE : {
				required : "* 필수입력 사항입니다.."
			},
			MOB_STS : {
				required : "* 필수입력 사항입니다. "
			},
			MOB_DESC : {
				maxlength : "* 최대 {0}글자이하여야 합니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});	
});
</script>
	
</head>
<body>

<div data-role="page" class="jqm-demos" data-quicklinks="true">

	<a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
    <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
	<!-- /header -->
	<!-- 
    <div data-role="header" class="jqm-header">
		<p></p>		
        <a href="#" class="jqm-navmenu-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-bars ui-nodisc-icon ui-alt-icon ui-btn-left">Menu</a>
        <a href="#" class="jqm-search-link ui-btn ui-btn-icon-notext ui-corner-all ui-icon-search ui-nodisc-icon ui-alt-icon ui-btn-right">Search</a>
		<p></p>
    </div>-->
    <!-- /header -->
    <p></p>
	<!--  Contents  -->
	
    <div role="main" class="ui-content jqm-content">
    	
	    
		
    
    	<form  id="inputSchedule" action="${pageContext.request.contextPath}/page/main/input_ok.jsp" method="post"  data-ajax="false"
			class="ui-body ui-body-a ui-corner-all">
			
		<!-- 
		<select data-mini="true" data-inline="true" class="ui-btn-left" id="font-size">
	        <option value="16px">Default</option>
	        <option value="10px">10px</option>
	        <option value="11px">11px</option>
	        <option value="12px">12px</option>
	        <option value="13px">13px</option>
	        <option value="14px">14px</option>
	        <option value="15px">15px</option>
	    </select>
    	 -->
    	<div data-role="fieldcontain">
			<label for="MOB_USER" class="select" data-mini="true" >담당자*:</label>
			<select name="MOB_USER" id="MOB_USER" data-native-menu="false"  data-mini="true">
			<c:set var="userList" value="<%=userList %>" />
				<c:forEach var="result" items="${userList.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.USER_ID}"/>"
				<c:if test="${result.USER_ID eq sessionScope.USER_ID}">selected</c:if> 
				><c:out value="${result.USER_NAME}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<!-- 
		<div data-role="fieldcontain">
			<label for="select-choice-1" class="select" data-mini="true">사이트*:</label>
			<select name="select-choice-1" id="select-choice-1"  data-mini="true">
				<option value=""></option>
				<option value="삼성화재">삼성화재</option>
				<option value="삼성생명">삼성생명</option>
				<option value="호텔신라">호텔신라</option>
				<option value="OGG교육">OGG교육</option>
			</select>
		</div>
		 -->
		 
		<div data-role="fieldcontain">
			<label for="MOB_SITE" class="select" data-mini="true">사이트* :</label>
			<input type="search" name="MOB_SITE" id="MOB_SITE" value=""  data-mini="true" />
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_DATE">날짜*:</label>
     		<input type="date" data-clear-btn="true" name="MOB_DATE" id="MOB_DATE" value="" data-mini="true">
		</div>
		
		<div data-role="fieldcontain">
			<label for="SCHEDULE" class="select" data-mini="true">지원*:</label>
		    <fieldset name=SCHEDULE data-role="controlgroup" data-type="horizontal">
		    	<input type="checkbox" name="AM" id="AM" class="custom" data-mini="true" />
				<label for="AM">오전</label>
				<input type="checkbox" name="PM" id="PM" class="custom" data-mini="true" />
				<label for="PM">오후</label>
				<input type="checkbox" name="NIGHT" id="NIGHT" class="custom" data-mini="true" />
				<label for="NIGHT">야간</label>
				<input type="checkbox" name="ALLNIGHT" id="ALLNIGHT" class="custom" data-mini="true" />
				<label for="ALLNIGHT">철야</label>
		    </fieldset>
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_RMT" class="select" data-mini="true" >방문/리모트* :</label>
			<select name="MOB_RMT" id="MOB_RMT" data-native-menu="false"  data-mini="true">
				<option value="IS" >방문지원</option>
				<option value="RS" >리모트지원</option>
				<option value="PP" >상주지원</option>
			</select>
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_TYPE" class="select" data-mini="true" >SRType* :</label>
			
			<c:set var="srType" value="<%=srType %>" />
			<select name="MOB_TYPE" id="MOB_TYPE" data-native-menu="false"  data-mini="true">
				<c:forEach var="result" items="${srType.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		
		<%--
		<div data-role="fieldcontain">
			<label for="MOB_SDATE" data-mini="true">시작*:</label>
		    <input type="datetime-local" data-clear-btn="true" name="MOB_SDATE" id="MOB_SDATE" value="${requestJob.DATA_LIST[0].MOB_SDATE_DESC}">
		</div>
		
		<div data-role="fieldcontain">
			<label for="MOB_EDATE" data-mini="true">종료*:</label>
		    <input type="datetime-local" data-clear-btn="true" name="MOB_EDATE" id="MOB_EDATE" value="${requestJob.DATA_LIST[0].MOB_EDATE_DESC}">
		</div>
		 --%>
		 
		<div data-role="fieldcontain">
			<label for="MOB_STS" class="select" data-mini="true" >상태* :</label>
			
			<c:set var="srStatus" value="<%=srStatus %>" />
			<select name="MOB_STS" id="MOB_STS" data-native-menu="false"  data-mini="true">
				<c:forEach var="result" items="${srStatus.DATA_LIST}" varStatus="status" >
				<option value="<c:out value="${result.CODE}"/>"><c:out value="${result.CODE_VALUE}"/></option>
				</c:forEach>
			</select>
		</div>
		 
		<div data-role="fieldcontain">
		<label for="MOB_DESC" data-mini="true">내용 :</label>
			<textarea name="MOB_DESC" id="MOB_DESC" data-mini="true"  data-mini="true" ></textarea>
		</div>
		
		<div class="errorTxt"></div>
		
		<fieldset class="ui-grid-a">
			<div class="ui-block-a">
				<input id=submit type="submit" value="등록" data-theme="a">
			</div>
			<div class="ui-block-b">
				<input id=reset type="reset" value="취소" data-theme="b">
			</div>
		</fieldset>
		</form> 
    </div>

    <!--  Contents  -->
    
    <%@ include file="../common/menu.jsp" %>
    
    <%@ include file="../common/footer.jsp" %>
    
    <%@ include file="../common/search.jsp" %>    
    
</div>
 
</body>
</html>