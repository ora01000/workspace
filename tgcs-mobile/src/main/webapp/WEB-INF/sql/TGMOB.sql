SELECT
MOB_ID
,MOB.MOB_USER
,MOB.MOB_SITE
,TO_CHAR(MOB.MOB_DATE,'YYYY-MM-DD') MOB_DATE
,MOB.MOB_SCH
,MOB.MOB_SCH1
,MOB.MOB_SCH2
,MOB.MOB_SCH3
,MOB.MOB_TYPE
,(SELECT CODE_VALUE FROM TGCS_CODE X WHERE MOB.MOB_TYPE = X.CODE AND GROUP_ID = '1080' ) MOB_TYPE_NAME
,MOB.MOB_DESC
,MOB.CREATE_DATE
,MOB.UPDATE_DATE
,MOB.CREATE_USER
,MOB.UPDATE_USER
,USR.U_NAME USER_NAME
,USR.U_POSITION_CODE POSITION_CODE
,(SELECT CODE_VALUE FROM TGCS_CODE A WHERE USR.U_POSITION_CODE = A.CODE AND GROUP_ID = '1003' ) POSITION_NAME
,USR.U_DEPT_CODE DEPT_CODE
,(SELECT CODE_VALUE FROM TGCS_CODE B WHERE USR.U_DEPT_CODE = B.CODE AND GROUP_ID = '1043' ) DEPT_NAME
,USR.U_JOB_CODE JOB_CODE
,(SELECT CODE_VALUE FROM TGCS_CODE C WHERE USR.U_JOB_CODE = C.CODE AND GROUP_ID = '1002' ) JOB_NAME
FROM
TGCS_MOB01 MOB, TGCS_USER USR
WHERE MOB.MOB_USER = USR.USER_ID

select 
  usr.user_id user_id,
  usr.u_name u_name,
  cd.code_value u_position_name,
  usr.u_name || ' ' || cd.code_value user_name,
  usr.attribute1
 from tgcs_user usr, tgcs_code cd
 where cd.group_id = '1003'
 and usr.u_position_code = cd.code
 and usr.u_dept_code = 'TEC01'
and usr.u_status = 'Y'
 order by cd.attribute1, usr.u_name
 
 SELECT
	MOB_ID
	,MOB.MOB_USER
	,MOB.MOB_SITE
	,TO_CHAR(MOB.MOB_DATE,'YYYY-MM-DD') MOB_DATE
	,MOB.MOB_SCH
	,MOB.MOB_SCH1
	,MOB.MOB_SCH2
	,MOB.MOB_SCH3
	,MOB.MOB_TYPE
	,(SELECT CODE_VALUE FROM TGCS_CODE X WHERE MOB.MOB_TYPE = X.CODE AND GROUP_ID = '1080' ) MOB_TYPE_NAME
	,MOB.MOB_DESC
	,MOB.CREATE_DATE
	,MOB.UPDATE_DATE
	,MOB.CREATE_USER
	,MOB.UPDATE_USER
FROM
TGCS_MOB01 MOB
WHERE MOB_DATE = TO_DATE('03/06/2016','MM/DD/YYYY')
ORDER BY MOB.UPDATE_DATE, MOB.CREATE_DATE


UPDATE TGCS_MOB01 SET
,MOB_USER = ?
,MOB_SITE = ?
,MOB_DATE = TO_DATE(?,'YYYY-MM-DD') 
,MOB_SCH = ?
,MOB_SCH1 = ?
,MOB_SCH2 = ?
,MOB_SCH3 = ?
,MOB_TYPE = ?
,MOB_DESC = ?
,UPDATE_DATE = sysdate
,UPDATE_USER = ?
WHERE
MOB_ID = ?

SELECT
	TO_NUMBER(to_char(sysdate, 'yyyymmddhh24miss')||TGCS_CAL.NEXTVAL)
FROM DUAL

insert into tgcs_evt01
	( evt_id, cal_id, evt_titl, evt_stdt, evt_endt, evt_location, evt_notes, create_user, dept_code, create_date )
	values
	(to_number(to_char(sysdate,'yyyymmdd')||TGCS_CAL.NEXTVAL), ? , ? ,to_date(?,'YYYYMMDDHH24MI') , to_date(?,'YYYYMMDDHH24MI') , ? , ? , ?  , ? ,SYSDATE )


SELECT
	CODE_VALUE SR_TYPE
FROM TGCS_CODE
WHERE CODE = ''
AND GROUP_ID = '1080'


select id, title, rownum color
  from (
      select tu.user_id id,
			   tu.u_name || ' ' || get_code(1003, tu.u_position_code, 0) title,
         tu.attribute1 attribute1, null create_date,
			   rownum color
		  from tgcs_user tu
		 where u_dept_code = 'TEC01'
		 and u_status <> 'N'
     union all
    select cal_id id, cal_titl title , 99999 attribute1, create_date,
      rownum color
      from tgcs_cal01
      where dept_code = 'TEC01'
      order by attribute1, create_date
     )




select 
	   rownum id, 
	   sr02.sr_support_main_se_id cid,
	   tu.u_name ||'/'||cus01.cust_company_name title,
	   sr02.sr_support_start_date as  start_date,
	   sr02.sr_support_finish_date as  end_date,
	   '['||prj01.project_name||'/' || prj02.site_name || '] '||sr02.sr_support_title sr_note,
	   prj02.site_location site_location,
	   sr01.sr_id sr_id
  from tgcs_sr01 sr01,
	   tgcs_sr02 sr02,
	   tgcs_prj01 prj01,
	   tgcs_prj02 prj02,
	   tgcs_cus01 cus01,
	   tgcs_cus02 cus02,
	   tgcs_user  tu
 where sr01.sr_id = sr02.sr_id
   and prj01.project_id = prj02.project_id
   and prj01.project_id = sr02.project_id
   and prj02.site_id = sr02.site_id
   and prj01.cust_company_id = cus01.cust_company_id
   and sr01.sr_customer_id = cus02.customer_id(+)
   and sr02.sr_support_status != 'D'
   and sr02.sr_support_main_se_id = tu.user_id
   and sr02.sr_support_type in ('VIS', 'BTR')
   and u_dept_code = 'TEC01'
   --and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') >= ?
   --and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') <= ?
   union all
   select 
   evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_cal01 cal, tgcs_user usr 
	where ev.dept_code = 'TEC01'
	--and to_char(ev.evt_stdt,'yyyy-mm-dd') >= ?
	--and to_char(ev.evt_endt,'yyyy-mm-dd') <= ?
	and ev.create_user = usr.user_id(+)
	and ev.cal_id = cal.cal_id
  
  
  select 
	   rownum id, 
	   sr02.sr_support_main_se_id cid,
	   tu.u_name ||'/'||cus01.cust_company_name title,
	   sr02.sr_support_start_date as  start_date,
	   sr02.sr_support_finish_date as  end_date,
	   '['||prj01.project_name||'/' || prj02.site_name || '] '||sr02.sr_support_title sr_note,
	   prj02.site_location site_location,
	   sr01.sr_id sr_id
  from tgcs_sr01 sr01,
	   tgcs_sr02 sr02,
	   tgcs_prj01 prj01,
	   tgcs_prj02 prj02,
	   tgcs_cus01 cus01,
	   tgcs_cus02 cus02,
	   tgcs_user  tu
 where sr01.sr_id = sr02.sr_id
   and prj01.project_id = prj02.project_id
   and prj01.project_id = sr02.project_id
   and prj02.site_id = sr02.site_id
   and prj01.cust_company_id = cus01.cust_company_id
   and sr01.sr_customer_id = cus02.customer_id(+)
   and sr02.sr_support_status != 'D'
   and sr02.sr_support_main_se_id = tu.user_id
   and sr02.sr_support_type in ('VIS', 'BTR')
   and u_dept_code = 'TEC01'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') >= '2016-03-06'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') <= '2016-03-19'
   union all
   select 
   evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_cal01 cal, tgcs_user usr 
	where ev.dept_code = 'TEC01'
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
	and ev.cal_id = cal.cal_id
  union all
  select 
   ev.evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_mob01 mob, tgcs_user usr 
	where ev.dept_code = 'TEC01'
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
	and ev.evt_id = mob.mod_id
  union all
  select 
   ev.evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_mob01 mob, tgcs_user usr 
	where ev.dept_code = 'TEC01'
  and ev.evt_id = mob.mob_id
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
  
  
  
  select 
	   rownum id, 
	   sr02.sr_support_main_se_id cid,
	   tu.u_name ||'/'||cus01.cust_company_name title,
	   sr02.sr_support_start_date as  start_date,
	   sr02.sr_support_finish_date as  end_date,
	   '['||prj01.project_name||'/' || prj02.site_name || '] '||sr02.sr_support_title sr_note,
	   prj02.site_location site_location,
	   sr01.sr_id sr_id
  from tgcs_sr01 sr01,
	   tgcs_sr02 sr02,
	   tgcs_prj01 prj01,
	   tgcs_prj02 prj02,
	   tgcs_cus01 cus01,
	   tgcs_cus02 cus02,
	   tgcs_user  tu
 where sr01.sr_id = sr02.sr_id
   and prj01.project_id = prj02.project_id
   and prj01.project_id = sr02.project_id
   and prj02.site_id = sr02.site_id
   and prj01.cust_company_id = cus01.cust_company_id
   and sr01.sr_customer_id = cus02.customer_id(+)
   and sr02.sr_support_status != 'D'
   and sr02.sr_support_main_se_id = tu.user_id
   and sr02.sr_support_type in ('VIS', 'BTR')
   and u_dept_code = 'TEC01'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') >= '2016-03-06'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') <= '2016-03-19'
   union all
   select 
   evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_cal01 cal, tgcs_user usr 
	where ev.dept_code = 'TEC01'
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
	and ev.cal_id = cal.cal_id
	union all
  select 
   ev.evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_mob01 mob, tgcs_user usr 
	where ev.dept_code = 'TEC01'
  and ev.evt_id = mob.mob_id
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
  
  
  
  select
  evt.EVT_ID
  ,evt.CAL_ID
  ,cal.CAL_TITL
  ,evt.EVT_TITL
  ,evt.EVT_STDT
  ,evt.EVT_ENDT
  ,evt.EVT_RRULE
  ,evt.EVT_LOCATION
  ,evt.EVT_NOTES
  ,evt.EVT_URL
  ,evt.EVT_ISDAYALL
  ,evt.EVT_REMNDR
  ,evt.CREATE_USER
from tgcs_cal01 cal, tgcs_evt01 evt
where evt.cal_id = cal.cal_id(+)


select 
	   rownum id, 
	   sr02.sr_support_main_se_id cid,
	   tu.u_name ||'/'||cus01.cust_company_name title,
	   sr02.sr_support_start_date as  start_date,
	   sr02.sr_support_finish_date as  end_date,
	   '['||prj01.project_name||'/' || prj02.site_name || '] '||sr02.sr_support_title sr_note,
	   prj02.site_location site_location,
	   sr01.sr_id sr_id
  from tgcs_sr01 sr01,
	   tgcs_sr02 sr02,
	   tgcs_prj01 prj01,
	   tgcs_prj02 prj02,
	   tgcs_cus01 cus01,
	   tgcs_cus02 cus02,
	   tgcs_user  tu
 where sr01.sr_id = sr02.sr_id
   and prj01.project_id = prj02.project_id
   and prj01.project_id = sr02.project_id
   and prj02.site_id = sr02.site_id
   and prj01.cust_company_id = cus01.cust_company_id
   and sr01.sr_customer_id = cus02.customer_id(+)
   and sr02.sr_support_status != 'D'
   and sr02.sr_support_main_se_id = tu.user_id
   and sr02.sr_support_type in ('VIS', 'BTR')
   and u_dept_code = 'TEC01'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') >= '2016-03-06'
   and to_char(sr02.sr_support_start_date,'yyyy-mm-dd') <= '2016-03-19'
   union all
   select 
   evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_cal01 cal, tgcs_user usr 
	where ev.dept_code = 'TEC01'
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and ev.create_user = usr.user_id(+)
	and ev.cal_id = cal.cal_id
	union all
  	select 
   ev.evt_id id,
   ev.cal_id cid,
	usr.u_name||'/'||ev.evt_titl title, 
	ev.evt_stdt start_date, ev.evt_endt end_date , 
	ev.evt_notes sr_note, 
	ev.evt_location site_location, null sr_id 
	from tgcs_evt01 ev, tgcs_mob01 mob, tgcs_user usr 
	where ev.dept_code = 'TEC01'
  and ev.evt_id = mob.mob_id
	and to_char(ev.evt_stdt,'yyyy-mm-dd') >= '2016-03-06'
	and to_char(ev.evt_endt,'yyyy-mm-dd') <= '2016-03-19'
	and mob.mob_user = usr.user_id(+)
  

select * from tgcs_mob01

SELECT
	MOB_ID
	,MOB.MOB_USER
	,MOB.MOB_SITE
	,TO_CHAR(MOB.MOB_DATE,'YYYYMMDD') MOB_SDATE
  ,TO_CHAR(MOB.MOB_DATE,'YYYYMMDD') MOB_EDATE
	,MOB.MOB_TYPE
	,(SELECT CODE_VALUE FROM TGCS_MOBCODE X WHERE MOB.MOB_TYPE = X.CODE AND GROUP_ID = '1001' ) MOB_TYPE_NAME
	,MOB.MOB_DESC
	,MOB.CREATE_DATE
	,MOB.UPDATE_DATE
	,MOB.CREATE_USER
	,MOB.UPDATE_USER
	,USR.U_NAME USER_NAME
	,USR.U_POSITION_CODE POSITION_CODE
	,(SELECT CODE_VALUE FROM TGCS_CODE A WHERE USR.U_POSITION_CODE = A.CODE AND GROUP_ID = '1003' ) POSITION_NAME
	,USR.U_DEPT_CODE DEPT_CODE
	,(SELECT CODE_VALUE FROM TGCS_CODE B WHERE USR.U_DEPT_CODE = B.CODE AND GROUP_ID = '1043' ) DEPT_NAME
	,USR.U_JOB_CODE JOB_CODE
	,(SELECT CODE_VALUE FROM TGCS_CODE C WHERE USR.U_JOB_CODE = C.CODE AND GROUP_ID = '1002' ) JOB_NAME
FROM
TGCS_MOB02 MOB, TGCS_USER USR
WHERE MOB.MOB_USER = USR.USER_ID
AND MOB_DATE BETWEEN  TO_DATE(?,'YYYYMMDD') AND TO_DATE(?,'YYYYMMDD')
ORDER BY MOB.UPDATE_DATE, MOB.CREATE_DATE


select 
  usr.user_id user_id,
  usr.u_name u_name,
  cd.code_value position_name,
  usr.u_name || ' ' || cd.code_value user_name,
  usr.attribute1
 from tgcs_user usr, tgcs_code cd
 where cd.group_id = '1003'
 and usr.u_position_code = cd.code
 and usr.u_dept_code = 'TEC01'
and usr.u_status = 'Y'
 order by usr.attribute1 asc, usr.u_name

  