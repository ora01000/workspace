package com.tg.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MainServlet extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		doJob(req, res);		
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		doJob(req, res);
	}
	
	
	public void doJob(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		HttpSession session = req.getSession();
		String sessionId = session.getId();
		String num = (String)session.getAttribute("num");
		if(num == null)
			num = "1";
		else
			num = String.valueOf(Integer.parseInt(num) + 1);
		
		session.setAttribute("num", num);
		
		System.out.println("Session ID is [" + sessionId + "] and NUM is [" + num + "]");
		
		PrintWriter out = res.getWriter();
		
		out.println("This Response is from JBoss EAP Server");
		
	}
}
