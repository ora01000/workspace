package com.tg.jfound.proc; 


import javax.servlet.http.HttpServletRequest;

import com.tg.jfound.exception.PException;
import com.tg.jfound.web.MultipartRequest;


public interface Processor 
{ 
    // General Web process
	public String processWebRequest(HttpServletRequest req) throws PException;
	// Multipart Process
	public String processMultipartRequest(MultipartRequest mReq,HttpServletRequest req) throws PException;

} 
