package com.tg.mobile.adopter;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

import com.tg.jfound.logging.Logging;
import com.tg.jfound.vo.box.Box;
import com.tg.jfound.vo.box.CollectionUtility;
import com.tg.jfound.web.BaseAbstractServlet;
import com.tg.mobile.modules.login.TGMOBLOGIN;

public class LoginAdopter extends BaseAbstractServlet {

	// public void LoginAdapter(){this.super();}

	protected void catchService(javax.servlet.http.HttpServletRequest req,
			javax.servlet.http.HttpServletResponse res) {
		String rtnUri = "/";

		try {
			HttpSession session = req.getSession(true);
			Box sessionbox = CollectionUtility.getBoxWithSession(req);

			String U_ID =  req.getParameter("U_ID");
			String U_PWD = req.getParameter("U_PWD");
			String taction = req.getParameter("taction");
			
			Enumeration params = req.getParameterNames();
			while (params.hasMoreElements()){
			    String name = (String)params.nextElement();
			    Logging.debug.println(name + " : " +req.getParameter(name));
			}

			Logging.debug.println("<LoginAdapter> taction:" + taction);
			if (taction == null)
				taction = "LOGIN";

			Logging.debug.println("<LoginAdapter> catchService Initiated USER_ID:"	+ U_ID);
			Logging.debug.println("<LoginAdapter> catchService Initiated by [" + req.getRemoteAddr() + "]\n");

			Logging.debug.println("<LoginAdapter> USER_ID:"	+ sessionbox.getString("U_ID"));
			Logging.debug.println("<LoginAdapter> U_STATUS:" + sessionbox.getString("U_STATUS"));

			String U_STATUS = sessionbox.getString("U_STATUS");

			if ((U_STATUS == null) || (U_STATUS.equals("N"))
					|| (U_STATUS.equals(""))) {

				Logging.dev.println("Login.. JSP.... Callled");

				TGMOBLOGIN login = new TGMOBLOGIN();
				login.userLogin(U_ID, U_PWD, session);
				//rtnUri = req.getContextPath() + "/page/login/login.jsp";
				rtnUri = "/page/login/login.jsp";

			} else {
				if (taction.equals("LOGIN")) {
					Logging.dev.println("Login... Go");
					//rtnUri = req.getContextPath() + "/page/main/main.jsp";
					rtnUri = "/page/main/main.jsp";
				} else {
					Logging.dev.println("Log-out...");

					Logging.debug.println("<LoginAdapter> Log-out");
					session.invalidate();
					//rtnUri = req.getContextPath() + "/page/login/login.jsp";
					rtnUri = "/page/login/login.jsp";
				}
			}

			Logging.debug.println("<LoginAdapter> catchService Rtn URL ["
					+ rtnUri + "]\n");
			printJsp(req, res, rtnUri);
		} catch (Exception e) {
			e.printStackTrace();
			Logging.err
					.println("<LoginAdapter> Catche the Occurred Exception While User IP ["
							+ req.getRemoteAddr() + "]\n" + e);
			this.printErr(req, res, "<LoginAdapter> Exception occurred " + e, e);
		}
	}
}
