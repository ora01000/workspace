<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
<meta name="format-detection" content="telephone=no, address=no, email=no" /><!-- 숫자 전화번호 인식 방지 -->
	
<link rel="shortcut icon" href="favicon.ico">

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.mobile-1.4.5.css" />
<link rel="stylesheet" href="../_assets/css/jqm-demos.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery.mobile.datepicker.css">

<style>
<!--
	.th-groups th {
		background-color: rgba(0,0,0,0.07);
		border-right: 1px solid #fff;
		text-align: center;
	}
	.tr-row td {
		/*
		background-color: rgba(0,0,0,0.07);
		*/
		border-right: 1px solid #fff;
		text-align: center;
	}
	.tr-row-w td {
		/*
		background-color: rgba(0,0,0,0.07);
		*/
		border-right: 1px solid rgba(0,0,0,0.07);
		text-align: center;
	}
	
	body { min-height:540px; }
    body[orient="portrait"] { min-height:540px; }
    body[orient="landscape"] { min-height:400px; }
    
    .hasDatepicker {
	    position: relative;
	    z-index: 9;
	}
    
    .button1{
border:1px solid #393B3E;-webkit-box-shadow: #737373 0px 1px 5px ;-moz-box-shadow: #737373 0px 1px 5px ; box-shadow: #737373 0px 1px 5px ; -webkit-border-radius: 23px; -moz-border-radius: 23px;border-radius: 23px;font-size:14px;font-family:arial, helvetica, sans-serif; padding: 10px 20px 10px 20px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(10,10,10,0.5);font-weight:bold; color: #8C8C8C;
 background-color: #515459; background-image: -webkit-gradient(linear, left top, left bottom, from(#515459), to(#3B3C40));
 background-image: -webkit-linear-gradient(top, #515459, #3B3C40);
 background-image: -moz-linear-gradient(top, #515459, #3B3C40);
 background-image: -ms-linear-gradient(top, #515459, #3B3C40);
 background-image: -o-linear-gradient(top, #515459, #3B3C40);
 background-image: linear-gradient(to bottom, #515459, #3B3C40);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#515459, endColorstr=#3B3C40);
}

.button1:hover{
 border:1px solid #393B3E;
 background-color: #3B3C40; background-image: -webkit-gradient(linear, left top, left bottom, from(#3B3C40), to(#202124));
 background-image: -webkit-linear-gradient(top, #3B3C40, #202124);
 background-image: -moz-linear-gradient(top, #3B3C40, #202124);
 background-image: -ms-linear-gradient(top, #3B3C40, #202124);
 background-image: -o-linear-gradient(top, #3B3C40, #202124);
 background-image: linear-gradient(to bottom, #3B3C40, #202124);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#3B3C40, endColorstr=#202124);
}

.button2{
border:1px solid #CCCCCC;-webkit-box-shadow: #FEFFFF 0px 1px 1px ;-moz-box-shadow: #FEFFFF 0px 1px 1px ; box-shadow: #FEFFFF 0px 1px 1px ; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 6px 6px 6px 6px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(255,255,255,1);font-weight:bold; color: #4A4A4A;
 background-color: #F7F5F6; background-image: -webkit-gradient(linear, left top, left bottom, from(#F7F5F6), to(#DDDDDD));
 background-image: -webkit-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -moz-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -ms-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -o-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: linear-gradient(to bottom, #F7F5F6, #DDDDDD);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#F7F5F6, endColorstr=#DDDDDD);
}

.button2:hover{
 border:1px solid #ADADAD;
 background-color: #E0E0E0; background-image: -webkit-gradient(linear, left top, left bottom, from(#E0E0E0), to(#BDBBBC));
 background-image: -webkit-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -moz-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -ms-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -o-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: linear-gradient(to bottom, #E0E0E0, #BDBBBC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E0E0E0, endColorstr=#BDBBBC);
}
.errorTxt{
  border: 0px solid red;
  min-height: 14px;
}
-->
</style>

<!-- 
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="../_assets/js/index.js"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script src="http://cdn.rawgit.com/jquery/jquery-ui/1.10.4/ui/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="http://cdn.rawgit.com/arschmitz/jquery-mobile-datepicker-wrapper/v0.1.1/jquery.mobile.datepicker.js"></script>
-->
<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>   
<script src="../_assets/js/index.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.mobile-1.4.5.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="${pageContext.request.contextPath}/js/jquery.mobile.datepicker.js"></script>

<script type="text/javascript">
var userAgent = navigator.userAgent.toLowerCase(); // 접속 핸드폰 정보
//alert(userAgent);

// 모바일 홈페이지 바로가기 링크 생성 
if(userAgent.match('iphone')) { 
    document.write('<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/apple114.png" />') 
} else if(userAgent.match('ipad')) { 
    document.write('<link rel="apple-touch-icon" sizes="72*72" href="${pageContext.request.contextPath}/apple72.png" />') 
} else if(userAgent.match('ipod')) { 
    document.write('<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/apple114.png" />') 
} else if(userAgent.match('android')) { 
    document.write('<link rel="shortcut icon" href="${pageContext.request.contextPath}/apple114.png" />') 
} 


// 안드로이드 버그 주소창 없애기
window.addEventListener('load', function(){
    document.body.style.height = (document.documentElement.clientHeight + 5) + 'px';
    window.scrollTo(0, 1);
}, false);


/*
window.addEventListener('load', function() {
	alert("load");
    setTimeout(scrollTo, 0, 0, 1);
}, false);

$(document).ready(function () {
	//주소창 자동 닫힘 
	window.addEventListener("load", function(){
		setTimeout(loaded, 100); 
	}, false); 
	function loaded(){ 
		window.scrollTo(0, 1); 
	}	
});
*/
</script>
