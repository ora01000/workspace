<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<script type="text/javascript">
function goPage(url){
	location.href = url;
}
</script>


<div data-role="panel" class="jqm-navmenu-panel" data-position="left" data-display="overlay" data-theme="a">
	
	<ul class="jqm-list ui-alt-icon ui-nodisc-icon">
		<li data-filtertext="demos homepage" data-icon="home"><a href="javascript:goPage('${pageContext.request.contextPath}');">홈</a></li>
		<li data-filtertext="demos homepage" data-icon="clock"><a href="javascript:goPage('${pageContext.request.contextPath}/page/week/list.jsp');">주간일정</a></li>
		<li data-filtertext="demos homepage" data-icon="eye"><a href="javascript:goPage('${pageContext.request.contextPath}/page/main/input.jsp');">일정등록</a></li>
		<li data-filtertext="demos homepage" data-icon="eye"><a href="javascript:goPage('${pageContext.request.contextPath}/page/req/input.jsp');">SR등록</a></li>
		<li data-filtertext="demos homepage" data-icon="clock"><a href="javascript:goPage('${pageContext.request.contextPath}/page/req/misreqlist.jsp');">SR확인</a></li>
		<li data-filtertext="demos homepage" data-icon="shop"><a href="javascript:goPage('${pageContext.request.contextPath}/page/longterm/input.jsp');">프로젝트등록</a></li>
		
		<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
			<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
				<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">조회<span class="ui-collapsible-heading-status">조회</span></a>
			</h3>
			<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
				<ul>
					<li data-filtertext="form checkboxradio widget checkbox input checkboxes controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/main/list.jsp');" data-ajax="false">일정조회</a></li>
					<li data-filtertext="form checkboxradio widget radio input radio buttons controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/req/list.jsp');" data-ajax="false">SR조회</a></li>
					<li data-filtertext="form checkboxradio widget radio input radio buttons controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/longterm/list.jsp');" data-ajax="false">프로젝트조회</a></li>
				</ul>
			</div>
		</li>
		<%--
		<li data-filtertext="demos homepage" data-icon="cloud"><a href="javascript:goPage('${pageContext.request.contextPath}/page/main/list.jsp');">일정조회</a></li>
		<li data-filtertext="demos homepage" data-icon="grid"><a href="javascript:goPage('${pageContext.request.contextPath}/page/longterm/list.jsp');">프로젝트조회</a></li>
		 --%>
		
		<%--
		<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
			<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
				<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">일정<span class="ui-collapsible-heading-status">일정</span></a>
			</h3>
			<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
				<ul>
					<li data-filtertext="form checkboxradio widget checkbox input checkboxes controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/main/list.jsp');" data-ajax="false">목록</a></li>
					<li data-filtertext="form checkboxradio widget radio input radio buttons controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/main/input.jsp');" data-ajax="false">등록</a></li>
				</ul>
			</div>
		</li>
		<li data-role="collapsible" data-enhanced="true" data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right" data-inset="false" class="ui-collapsible ui-collapsible-themed-content ui-collapsible-collapsed">
			<h3 class="ui-collapsible-heading ui-collapsible-heading-collapsed">
				<a href="#" class="ui-collapsible-heading-toggle ui-btn ui-btn-icon-right ui-btn-inherit ui-icon-carat-d">장기일정<span class="ui-collapsible-heading-status">장기일정</span></a>
			</h3>
			<div class="ui-collapsible-content ui-body-inherit ui-collapsible-content-collapsed" aria-hidden="true">
				<ul>
					<li data-filtertext="form checkboxradio widget checkbox input checkboxes controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/longterm/list.jsp');" data-ajax="false">목록</a></li>
					<li data-filtertext="form checkboxradio widget radio input radio buttons controlgroups"><a href="javascript:goPage('${pageContext.request.contextPath}/page/longterm/input.jsp');" data-ajax="false">등록</a></li>
				</ul>
			</div>
		</li>
		 --%>
		<li data-filtertext="demos homepage" data-icon="minus"><a href="javascript:goPage('${pageContext.request.contextPath}/page/login/login_ok.jsp?taction=LOGOUT');">로그아웃</a></li>
	</ul>
</div><!-- /panel -->