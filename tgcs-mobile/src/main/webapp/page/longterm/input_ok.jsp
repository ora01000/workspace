<%-- Program info --%>
<%
	/*
	 *  Program Type : JSP
	 *  Program ID   :
	 *
	 *  DESC         :
	 *  Author       :
	 *  Date         :
	 *  Update       :
	 */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="com.tg.jfound.logging.Logging" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.jfound.web.BaseAbstractServlet" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>

<%

String rtnUri = "/";

try {
	HttpSession httpsession = request.getSession(true);
	Box box = CollectionUtility.getBoxWithSession(request);
	
	Enumeration params = request.getParameterNames();
	while (params.hasMoreElements()){
	    String name = (String)params.nextElement();
	    Logging.dev.println(name + " : " +request.getParameter(name));
	}

/*	
USER_ID
SITE
TDATE
AM
PM
NIGHT
ALLNIGHT
SCHEDULE
SRTYPE
CONTENTS
*/
	box.put("MOB_USER", request.getParameter("MOB_USER"));
	box.put("MOB_SITE", request.getParameter("MOB_SITE"));
	box.put("MOB_SDATE", request.getParameter("MOB_SDATE"));
	box.put("MOB_EDATE", request.getParameter("MOB_EDATE"));
	box.put("MOB_PJTTYPE", request.getParameter("MOB_PJTTYPE"));
	box.put("MOB_STS", request.getParameter("MOB_STS"));
	box.put("MOB_DESC", request.getParameter("MOB_DESC"));
	
	Logging.debug.println("<login ok> USER_ID:"	+ box.getString("U_ID"));
	Logging.debug.println("<login ok> U_STATUS:" + box.getString("U_STATUS"));
	String U_STATUS = box.getString("U_STATUS");

	if ((U_STATUS == null) || (U_STATUS.equals("N"))
			|| (U_STATUS.equals(""))) {
		Logging.dev.println("Login.. JSP.... Callled");
		//rtnUri = req.getContextPath() + "/page/login/login.jsp";
		rtnUri = "/page/login/login.jsp";
	} else if("Y".equals(U_STATUS)) {
		TGMOB01 mob01 = new TGMOB01();
		mob01.createLongSchedule(box);
		//rtnUri = req.getContextPath() + "/page/login/login.jsp";
		rtnUri = "/page/longterm/list.jsp";
	}

	Logging.debug.println("<input ok> catchService Rtn URL ["
			+ rtnUri + "]\n");

	String URL = request.getContextPath()+ rtnUri;
	response.sendRedirect(URL);
	
} catch (Exception e) {
	e.printStackTrace();
	Logging.err
			.println("<login ok> Catche the Occurred Exception While User IP ["
					+ request.getRemoteAddr() + "]\n" + e);
	//this.printErr(request, response, "<login ok> Exception occurred " + e, e);
}
%>