package com.tg.mobile.vo;

public class UserEnt extends com.tg.jfound.vo.entity.JFoundEntity

{
	public String U_ID;
	public String U_MAIL;
	public String U_NAME;
	public String U_STATUS;
	public String U_DEPT;
	public String U_POSITION;
	public String U_JOB_CODE;
	public String U_PMAIL;
	public String U_PHONE_NO;
	public String U_OFFICE_NO;
	public String U_SYS_AUTH;
	public String U_BIZ_AUTH;

	public String returnCode;
	public String returnMsg;

	public UserEnt manager = null;

}