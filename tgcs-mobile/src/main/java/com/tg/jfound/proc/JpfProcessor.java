package com.tg.jfound.proc; 


import javax.servlet.http.HttpServletRequest;

import com.tg.jfound.exception.PException;
import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;
import com.tg.jfound.web.MultipartRequest;


public interface JpfProcessor 
{ 
    // General Web process
	public ProcessMsg procJpfRequest(Box ibox) throws PException;

} 
