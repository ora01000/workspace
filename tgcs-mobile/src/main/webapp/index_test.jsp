<%-- Program info --%>
<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>

<%

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    System.out.println(sessionbox);
    String user_status = sessionbox.getString("U_STATUS");

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";

    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        System.out.println(rtn_code);

        if(user_status.equals("Y")){
%>
<META HTTP-EQUIV="refresh" CONTENT="0;URL='${pageContext.request.contextPath}/home'">
<%
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }

    }
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Enterprise Solution Team</title>
	<link rel="shortcut icon" href="favicon.ico">
	
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="./jquery.mobile-1.4.5/demos/_assets/css/jqm-demos.css">
	<link rel="stylesheet" href="http://cdn.rawgit.com/arschmitz/jquery-mobile-datepicker-wrapper/v0.1.1/jquery.mobile.datepicker.css">
	  
	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="./jquery.mobile-1.4.5/demos/_assets/js/index.js"></script>
	<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<script src="http://cdn.rawgit.com/jquery/jquery-ui/1.10.4/ui/jquery.ui.datepicker.js"></script>
    <script id="mobile-datepicker" src="http://cdn.rawgit.com/arschmitz/jquery-mobile-datepicker-wrapper/v0.1.1/jquery.mobile.datepicker.js"></script>
	
	<script> 
	var userAgent = navigator.userAgent.toLowerCase(); // 접속 핸드폰 정보
	alert(userAgent);
	   
	// 모바일 홈페이지 바로가기 링크 생성 
	if(userAgent.match('iphone')) { 
	    document.write('<link rel="apple-touch-icon" href="/이미지경로/icon.png" />') 
	} else if(userAgent.match('ipad')) { 
	    document.write('<link rel="apple-touch-icon" sizes="72*72" href="/이미지경로/icon.png" />') 
	} else if(userAgent.match('ipod')) { 
	    document.write('<link rel="apple-touch-icon" href="/이미지경로/icon.png" />') 
	} else if(userAgent.match('android')) { 
	    document.write('<link rel="shortcut icon" href="/이미지경로/icon.png" />') 
	} 
	</script>
</head>
<body>

<div data-role="page">
    <div data-role="header">
        <h1>Header</h1>
    </div>
    
    <div data-role="content">
    
    <ul data-role="listview" data-inset="true" data-filter="true">
	<li><a href="#">Acura</a></li>
	<li><a href="#">Audi</a></li>
	<li><a href="#">BMW</a></li>
	<li><a href="#">Cadillac</a></li>
	<li><a href="#">Ferrari</a></li>
</ul>   

<form>
    <input type="button" value="Button">
    <input type="submit" value="Submit">
    <input type="reset" value="Reset">
</form>


<form>
    <input type="button" value="Input - Inherit">
    <input type="button" value="Input - Theme swatch A" data-theme="a">
    <input type="button" value="Input - Theme swatch B" data-theme="b">
    <div class="ui-input-btn ui-btn">
        Enhanced - Inherit
        <input type="button" value="Enhanced - Inherit" data-enhanced="true">
    </div>
    <div class="ui-input-btn ui-btn ui-btn-a">
        Enhanced - Theme swatch A
        <input type="button" value="Enhanced - Theme swatch A" data-enhanced="true">
    </div>
    <div class="ui-input-btn ui-btn ui-btn-b">
        Enhanced - Theme swatch B
        <input type="button" value="Enhanced - Theme swatch B" data-enhanced="true">
    </div>
</form>

<form>
<fieldset data-role="controlgroup" data-mini="true">
    <legend>Vertical controlgroup, icon left, mini sized:</legend>
    <label for="select-native-8">Select A</label>
    <select name="select-native-8" id="select-native-8" data-iconpos="left">
        <option value="#">One</option>
        <option value="#">Two</option>
        <option value="#">Three</option>
    </select>
    <label for="select-native-9">Select B</label>
    <select name="select-native-9" id="select-native-9" data-iconpos="left">
        <option value="#">One</option>
        <option value="#">Two</option>
        <option value="#">Three</option>
    </select>
    <label for="select-native-10">Select C</label>
    <select name="select-native-10" id="select-native-10" data-iconpos="left">
        <option value="#">One</option>
        <option value="#">Two</option>
        <option value="#">Three</option>
    </select>
</fieldset>
</form>

<div class="ui-checkbox">
    <label class="ui-btn ui-corner-all ui-btn-inherit ui-btn-icon-left ui-checkbox-off" for="checkbox-enhanced">I agree</label>
    <input name="checkbox-enhanced" id="checkbox-enhanced" type="checkbox" data-enhanced="true">
</div>



<table class="ui-responsive table-stroke" id="table-column-toggle" data-role="table" data-mode="columntoggle">
     <thead>
       <tr>
         <th data-priority="2">Rank</th>
         <th>Movie Title</th>
         <th data-priority="3">Year</th>
         <th data-priority="1"><abbr title="Rotten Tomato Rating">Rating</abbr></th>
         <th data-priority="5">Reviews</th>
       </tr>
     </thead>
     <tbody>
       <tr>
         <th>1</th>
         <td><a href="http://en.wikipedia.org/wiki/Citizen_Kane" data-rel="external">Citizen Kane</a></td>
         <td>1941</td>
         <td>100%</td>
         <td>74</td>
       </tr>
       <tr>
         <th>2</th>
         <td><a href="http://en.wikipedia.org/wiki/Casablanca_(film)" data-rel="external">Casablanca</a></td>
         <td>1942</td>
         <td>97%</td>
         <td>64</td>
       </tr>
       <tr>
         <th>3</th>
         <td><a href="http://en.wikipedia.org/wiki/The_Godfather" data-rel="external">The Godfather</a></td>
         <td>1972</td>
         <td>97%</td>
         <td>87</td>
       </tr>
       <tr>
         <th>4</th>
         <td><a href="http://en.wikipedia.org/wiki/Gone_with_the_Wind_(film)" data-rel="external">Gone with the Wind</a></td>
         <td>1939</td>
         <td>96%</td>
         <td>87</td>
       </tr>
       <tr>
         <th>5</th>
         <td><a href="http://en.wikipedia.org/wiki/Lawrence_of_Arabia_(film)" data-rel="external">Lawrence of Arabia</a></td>
         <td>1962</td>
         <td>94%</td>
         <td>87</td>
       </tr>
       <tr>
         <th>6</th>
         <td><a href="http://en.wikipedia.org/wiki/Dr._Strangelove" data-rel="external">Dr. Strangelove Or How I Learned to Stop Worrying and Love the Bomb</a></td>
         <td>1964</td>
         <td>92%</td>
         <td>74</td>
       </tr>
       <tr>
         <th>7</th>
         <td><a href="http://en.wikipedia.org/wiki/The_Graduate" data-rel="external">The Graduate</a></td>
         <td>1967</td>
         <td>91%</td>
         <td>122</td>
       </tr>
       <tr>
         <th>8</th>
         <td><a href="http://en.wikipedia.org/wiki/The_Wizard_of_Oz_(1939_film)" data-rel="external">The Wizard of Oz</a></td>
         <td>1939</td>
         <td>90%</td>
         <td>72</td>
       </tr>
       <tr>
         <th>9</th>
         <td><a href="http://en.wikipedia.org/wiki/Singin%27_in_the_Rain" data-rel="external">Singin' in the Rain</a></td>
         <td>1952</td>
         <td>89%</td>
         <td>85</td>
       </tr>
       <tr>
         <th>10</th>
         <td class="title"><a href="http://en.wikipedia.org/wiki/Inception" data-rel="external">Inception</a></td>
         <td>2010</td>
         <td>84%</td>
         <td>78</td>
       </tr>
     </tbody>
   </table>



<table class="ui-body-d ui-shadow table-stripe ui-responsive" id="table-custom-2" data-role="table" data-mode="columntoggle" data-column-popup-theme="a" data-column-btn-text="Columns to display..." data-column-btn-theme="b">
         <thead>
           <tr class="ui-bar-d">
             <th data-priority="2">Rank</th>
             <th>Movie Title</th>
             <th data-priority="3">Year</th>
             <th data-priority="1"><abbr title="Rotten Tomato Rating">Rating</abbr></th>
             <th data-priority="5">Reviews</th>
           </tr>
         </thead>
         <tbody>
           <tr>
             <th>1</th>
             <td><a href="http://en.wikipedia.org/wiki/Citizen_Kane" data-rel="external">Citizen Kane</a></td>
             <td>1941</td>
             <td>100%</td>
             <td>74</td>
           </tr>
           <tr>
             <th>2</th>
             <td><a href="http://en.wikipedia.org/wiki/Casablanca_(film)" data-rel="external">Casablanca</a></td>
             <td>1942</td>
             <td>97%</td>
             <td>64</td>
           </tr>
           <tr>
             <th>3</th>
             <td><a href="http://en.wikipedia.org/wiki/The_Godfather" data-rel="external">The Godfather</a></td>
             <td>1972</td>
             <td>97%</td>
             <td>87</td>
           </tr>
           <tr>
             <th>4</th>
             <td><a href="http://en.wikipedia.org/wiki/Gone_with_the_Wind_(film)" data-rel="external">Gone with the Wind</a></td>
             <td>1939</td>
             <td>96%</td>
             <td>87</td>
           </tr>
           <tr>
             <th>5</th>
             <td><a href="http://en.wikipedia.org/wiki/Lawrence_of_Arabia_(film)" data-rel="external">Lawrence of Arabia</a></td>
             <td>1962</td>
             <td>94%</td>
             <td>87</td>
           </tr>
           <tr>
             <th>6</th>
             <td><a href="http://en.wikipedia.org/wiki/Dr._Strangelove" data-rel="external">Dr. Strangelove Or How I Learned to Stop Worrying and Love the Bomb</a></td>
             <td>1964</td>
             <td>92%</td>
             <td>74</td>
           </tr>
           <tr>
             <th>7</th>
             <td><a href="http://en.wikipedia.org/wiki/The_Graduate" data-rel="external">The Graduate</a></td>
             <td>1967</td>
             <td>91%</td>
             <td>122</td>
           </tr>
           <tr>
             <th>8</th>
             <td><a href="http://en.wikipedia.org/wiki/The_Wizard_of_Oz_(1939_film)" data-rel="external">The Wizard of Oz</a></td>
             <td>1939</td>
             <td>90%</td>
             <td>72</td>
           </tr>
           <tr>
             <th>9</th>
             <td><a href="http://en.wikipedia.org/wiki/Singin%27_in_the_Rain" data-rel="external">Singin' in the Rain</a></td>
             <td>1952</td>
             <td>89%</td>
             <td>85</td>
           </tr>
           <tr>
             <th>10</th>
             <td class="title"><a href="http://en.wikipedia.org/wiki/Inception" data-rel="external">Inception</a></td>
             <td>2010</td>
             <td>84%</td>
             <td>78</td>
           </tr>
         </tbody>
       </table>


<input type="text" data-role="date">


<input type="text" data-role="date" data-inline="true">


 <div data-demo-html="true">
            <input type="text" data-role="date">
        </div>
        
 <div data-demo-html="true">
            <input type="text" data-role="date" data-inline="true">
        </div>
      
        
        

    </div>
    <div data-role="footer" data-position="fixed">
    <h1>Footer</h1>
    </div>
</div>
 
</body>
</html>