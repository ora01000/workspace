package com.tg.mobile.modules.taglibs;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

public class LinkTag	extends TagSupport {

	private static final long serialVersionUID = 1L;
	
	private String var;
	private String mobid;
	private String type;

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getMobid() {
		return mobid;
	}

	public void setMobid(String mobid) {
		this.mobid = mobid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int doStartTag()
		throws JspException
    {
        try {
        	String[] arrVar = var.split("\\|\\|"); 
        	String[] arrMobid = mobid.split("\\|\\|");
        	String[] arrType = type.split("\\|\\|");
        	
        	StringBuffer sb = new StringBuffer();
        	for(int i = 0 ; i < arrVar.length; i++){
        		sb.append("<a href='");
        		if("L".equals(arrType[i])){
        			sb.append("/mobile/page/longterm/detail.jsp?MOB_ID=");
        		}else{
        			sb.append("/mobile/page/main/detail.jsp?MOB_ID=");
        		}
        		sb.append(arrMobid[i]);
        		sb.append("' style='text-decoration: none' >");
        		sb.append(arrVar[i]);
        		sb.append("</a>");
        		if(i < arrVar.length - 1)
        			sb.append(",");
        	}
            pageContext.getOut().print(sb.toString());
        } catch(IOException ioe) {
          throw new JspTagException("Error:  IOException while writing to the user");
        }
        return SKIP_BODY;
    }
}