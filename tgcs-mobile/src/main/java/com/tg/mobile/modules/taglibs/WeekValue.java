package com.tg.mobile.modules.taglibs;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.tg.mobile.util.DateUtils;

public class WeekValue	extends TagSupport {

	private static final long serialVersionUID = 1L;
	
	private String idate;
	
	public String getIdate() {
		return idate;
	}

	public void setIdate(String idate) {
		this.idate = idate;
	}

	public int doStartTag()
		throws JspException
    {
        try {
        	int week = 0;
        	if("".equals(idate))
        		week = DateUtils.dayOfWeek(DateUtils.getToday("yyyyMMdd"));
        	else
        		week = DateUtils.dayOfWeek(idate);
        	
        	pageContext.getOut().print((week==1 ||week == 7)?"-":"蹂몄궗");
            
        } catch(IOException ioe) {
          throw new JspTagException("Error:  IOException while writing to the user");
        }
        return SKIP_BODY;
    }
}