package com.tg.mobile.modules.mob;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.vo.box.Box;
import com.tg.mobile.util.DateUtils;


public class TGMOB01 {
	String resultCode = "FAIL";
	
	/***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserSchedule(Box sbox)throws PException{
    	
    	String mn = "getUserSchedule";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = {sbox.get("MOB_ID")};
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    /***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserLongSchedule(Box sbox)throws PException{
    	
    	String mn = "getUserLongSchedule";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = {sbox.get("MOB_ID")};
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
	
	/***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public Map getUserMainScheduleList(Box sbox)throws PException{
    	
    	String mn = "getUserMainScheduleList";
    	String search_date = "";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        if("".equals(sbox.get("SEARCH_DATE"))){
        	SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA ); 
        	Date currentTime = new Date (); 
        	search_date = formatter.format ( currentTime );
        }else{
        	search_date = sbox.get("SEARCH_DATE");
        }
        String input_value[] = {search_date, sbox.get("U_DEPT_CODE")};
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        JSONArray jsonArray = jsonObj.getJSONArray("DATA_LIST");
        
        Map<String,Object> userMap = new ConcurrentHashMap<String,Object>();
        Map<String,Object> scheduleMap = null;
        for(int i = 0 ; i < jsonArray.size() ; i++ ){
        	scheduleMap = (Map<String, Object>) userMap.get(jsonArray.getJSONObject(i).get("MOB_USER"));
        	if(scheduleMap == null)
        		scheduleMap = new ConcurrentHashMap<String,Object>();
        	for(int cnt = 0 ; cnt < 4 ; cnt++){
    			String key = (cnt==0?"":""+cnt);
    			if(jsonArray.getJSONObject(i).get("MOB_SCH"+key).equals("Y")) {
        			scheduleMap.put("MOB_SCH"+key,jsonArray.getJSONObject(i).get("MOB_SCH"+key));
        			if(scheduleMap.get("SCH"+key) == null){
        				scheduleMap.put("SCH"+key,jsonArray.getJSONObject(i).get("MOB_SITE"));
        				scheduleMap.put("MOB"+key,jsonArray.getJSONObject(i).get("MOB_ID"));
        				scheduleMap.put("TP"+key,"S");
        			}else{
        				scheduleMap.put("SCH"+key, scheduleMap.get("SCH"+key)+"||"+jsonArray.getJSONObject(i).get("MOB_SITE"));
        				scheduleMap.put("MOB"+key, scheduleMap.get("MOB"+key)+"||"+jsonArray.getJSONObject(i).get("MOB_ID"));
        				scheduleMap.put("TP"+key, scheduleMap.get("TP"+key)+"||S");
        			}
        		}	
    		}
    		userMap.put((String)jsonArray.getJSONObject(i).get("MOB_USER")+"", scheduleMap);
        }
        //Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        
        mn = "getUserWeekLongScheduleList";
        //Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        String input_long_value[] = {sbox.get("U_DEPT_CODE"),search_date, search_date, search_date, search_date};
        
        JSONObject jsonLongObj = dao.selectListJson(sbox,input_long_value);
        JSONArray jsonLongArray = jsonLongObj.getJSONArray("DATA_LIST");
        
        for(int i = 0 ; i < jsonLongArray.size() ; i++ ){
        	
        	String rsdate = jsonLongArray.getJSONObject(i).getString("MOB_SDATE");
        	String redate = jsonLongArray.getJSONObject(i).getString("MOB_EDATE");
        	
    		if(
    			DateUtils.getComputedDate(redate , "yyyyMMdd", search_date, "yyyyMMdd") >= 0 
    			&& 
    			DateUtils.getComputedDate( search_date , "yyyyMMdd", rsdate, "yyyyMMdd") >= 0
    		){
	        	scheduleMap = (Map<String, Object>) userMap.get(jsonLongArray.getJSONObject(i).get("MOB_USER")+"");
	        	if(scheduleMap == null)
	        		scheduleMap = new ConcurrentHashMap<String,Object>();
	        	scheduleMap.put("MOB_SCH","Y");
	        	scheduleMap.put("MOB_SCH1","Y");
				
	        	//scheduleMap.put("SCH",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
				//scheduleMap.put("SCH1",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
	        	if(scheduleMap.get("SCH") == null){
    				scheduleMap.put("SCH",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
    				scheduleMap.put("MOB",jsonLongArray.getJSONObject(i).get("MOB_ID"));
    				scheduleMap.put("TP","L");
    			}else{
    				scheduleMap.put("SCH", scheduleMap.get("SCH")+"||"+jsonLongArray.getJSONObject(i).get("MOB_SITE"));
    				scheduleMap.put("MOB", scheduleMap.get("MOB")+"||"+jsonLongArray.getJSONObject(i).get("MOB_ID"));
    				scheduleMap.put("TP", scheduleMap.get("TP")+"||L");
    			}
	        	if(scheduleMap.get("SCH1") == null){
    				scheduleMap.put("SCH1",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
    				scheduleMap.put("MOB1",jsonLongArray.getJSONObject(i).get("MOB_ID"));
    				scheduleMap.put("TP1","L");
    			}else{
    				scheduleMap.put("SCH1", scheduleMap.get("SCH1")+"||"+jsonLongArray.getJSONObject(i).get("MOB_SITE"));
    				scheduleMap.put("MOB1", scheduleMap.get("MOB1")+"||"+jsonLongArray.getJSONObject(i).get("MOB_ID"));
    				scheduleMap.put("TP1", scheduleMap.get("TP1")+"||L");
    			}
	        	
				userMap.put((String)jsonLongArray.getJSONObject(i).get("MOB_USER")+"", scheduleMap);
    		}
        }
                
        //Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonLongObj);
        return userMap;
    }
    
    
    /***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public Map getUserWeekScheduleList(Box sbox)throws PException{
    	
    	String mn = "getUserWeekScheduleList";
    	String start_date = sbox.get("START_DATE");
    	String end_date = sbox.get("END_DATE");
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        String input_value[] = {start_date, end_date, sbox.get("U_DEPT_CODE")};
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        JSONArray jsonArray = jsonObj.getJSONArray("DATA_LIST");
        
        Map<String,Object> userMap = new ConcurrentHashMap<String,Object>();
        Map<String,Object> scheduleMap = null;
        
        String [] dayWeek = new String[7];
        for(int i = 0 ; i < 7 ; i++)
        	dayWeek[i] = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", i, 0, 0 );
        
        for(int i = 0 ; i < jsonArray.size() ; i++ ){
        	for(int j = 0 ; j < dayWeek.length ; j++){
        	
        		if(dayWeek[j].equals(jsonArray.getJSONObject(i).get("MOB_DATE"))){
		        	scheduleMap = (Map<String, Object>) userMap.get(jsonArray.getJSONObject(i).get("MOB_USER")+dayWeek[j]);
		        	if(scheduleMap == null)
		        		scheduleMap = new ConcurrentHashMap<String,Object>();
		        	for(int cnt = 0 ; cnt < 4 ; cnt++){
		    			String key = (cnt==0?"":""+cnt);
		    			if(jsonArray.getJSONObject(i).get("MOB_SCH"+key).equals("Y")) {
		        			scheduleMap.put("MOB_SCH"+key,jsonArray.getJSONObject(i).get("MOB_SCH"+key));
		        			
		        			String remote_YN = "";
		        			if(jsonArray.getJSONObject(i).get("MOB_RMT").equals("RS")) remote_YN = "[R]";
		        			
		        			if(scheduleMap.get("SCH"+key) == null){
		        				scheduleMap.put("SCH"+key,jsonArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
		        				scheduleMap.put("MOB"+key,jsonArray.getJSONObject(i).get("MOB_ID"));
		        				scheduleMap.put("TP"+key,"S");
		        				scheduleMap.put("RMT"+key,jsonArray.getJSONObject(i).get("MOB_RMT"));
		        			}else{
		        				scheduleMap.put("SCH"+key, scheduleMap.get("SCH"+key)+"||"+jsonArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
		        				scheduleMap.put("MOB"+key, scheduleMap.get("MOB"+key)+"||"+jsonArray.getJSONObject(i).get("MOB_ID"));
		        				scheduleMap.put("TP"+key, scheduleMap.get("TP"+key)+"||S");
		        				scheduleMap.put("RMT"+key,jsonArray.getJSONObject(i).get("MOB_RMT"));
		        			}
		        		}	
		    		}
		        	userMap.put((String)jsonArray.getJSONObject(i).get("MOB_USER")+dayWeek[j], scheduleMap);
        		}
        	}
        }
        
        
        mn = "getUserWeekLongScheduleList";
        //Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        String input_long_value[] = {sbox.get("U_DEPT_CODE"),start_date, start_date, end_date, end_date};
        
        JSONObject jsonLongObj = dao.selectListJson(sbox,input_long_value);
        JSONArray jsonLongArray = jsonLongObj.getJSONArray("DATA_LIST");
        
        for(int i = 0 ; i < 7 ; i++)
        	dayWeek[i] = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", i, 0, 0 );
        
        for(int i = 0 ; i < jsonLongArray.size() ; i++ ){
        	
        	String rsdate = jsonLongArray.getJSONObject(i).getString("MOB_SDATE");
        	String redate = jsonLongArray.getJSONObject(i).getString("MOB_EDATE");
        	for(int j = 0 ; j < dayWeek.length ; j++){
        		//Logging.dev.println("1 - "+redate + " - " + dayWeek[j] + " = " + DateUtils.getComputedDate(redate , "yyyyMMdd", dayWeek[j], "yyyyMMdd"));
        		//Logging.dev.println("2 - "+dayWeek[j] + " - " + rsdate + " = " + DateUtils.getComputedDate( dayWeek[j] , "yyyyMMdd", rsdate, "yyyyMMdd"));
        		//Logging.dev.println(dayWeek[j] + " = " + DateUtils.dayOfWeek(dayWeek[j]));

        		if(DateUtils.dayOfWeek(dayWeek[j]) == 7 || DateUtils.dayOfWeek(dayWeek[j]) == 1){
        		}else if(
        			DateUtils.getComputedDate(redate , "yyyyMMdd", dayWeek[j], "yyyyMMdd") >= 0 
        			&& 
        			DateUtils.getComputedDate( dayWeek[j] , "yyyyMMdd", rsdate, "yyyyMMdd") >= 0
        		){
		        	scheduleMap = (Map<String, Object>) userMap.get(jsonLongArray.getJSONObject(i).get("MOB_USER")+dayWeek[j]);
		        	if(scheduleMap == null)
		        		scheduleMap = new ConcurrentHashMap<String,Object>();
		        	scheduleMap.put("MOB_SCH","Y");
		        	scheduleMap.put("MOB_SCH1","Y");
					//scheduleMap.put("SCH",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
					//scheduleMap.put("SCH1",jsonLongArray.getJSONObject(i).get("MOB_SITE"));
		        	
		        	String remote_YN = "";
        			if(jsonArray.getJSONObject(i).get("MOB_RMT").equals("RS")) remote_YN = "[R]";
					
					if(scheduleMap.get("SCH") == null){
	    				scheduleMap.put("SCH",jsonLongArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
	    				scheduleMap.put("MOB",jsonLongArray.getJSONObject(i).get("MOB_ID"));
	    				scheduleMap.put("TP","L");
	    				scheduleMap.put("RMT",jsonArray.getJSONObject(i).get("MOB_RMT"));
	    			}else{
	    				scheduleMap.put("SCH", scheduleMap.get("SCH")+"||"+jsonLongArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
	    				scheduleMap.put("MOB", scheduleMap.get("MOB")+"||"+jsonLongArray.getJSONObject(i).get("MOB_ID"));
	    				scheduleMap.put("TP", scheduleMap.get("TP")+"||L");
	    				scheduleMap.put("RMT",jsonArray.getJSONObject(i).get("MOB_RMT"));
	    			}
		        	if(scheduleMap.get("SCH1") == null){
	    				scheduleMap.put("SCH1",jsonLongArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
	    				scheduleMap.put("MOB1",jsonLongArray.getJSONObject(i).get("MOB_ID"));
	    				scheduleMap.put("TP1","L");
	    				scheduleMap.put("RMT",jsonArray.getJSONObject(i).get("MOB_RMT"));
	    			}else{
	    				scheduleMap.put("SCH1", scheduleMap.get("SCH1")+"||"+jsonLongArray.getJSONObject(i).get("MOB_SITE")+remote_YN);
	    				scheduleMap.put("MOB1", scheduleMap.get("MOB1")+"||"+jsonLongArray.getJSONObject(i).get("MOB_ID"));
	    				scheduleMap.put("TP1", scheduleMap.get("TP1")+"||L");
	    				scheduleMap.put("RMT",jsonArray.getJSONObject(i).get("MOB_RMT"));
	    			}
	        	
					userMap.put((String)jsonLongArray.getJSONObject(i).get("MOB_USER")+dayWeek[j], scheduleMap);
        		}
        	}
        }
                
        //Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return userMap;
    }
	/***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserScheduleList(Box sbox)throws PException{
    	
    	String mn = "getUserScheduleList";
    	String s_date = "";
    	String e_date = "";
    	String search_user_name = "";
    	String search_mob_desc = "";
    	
        //Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        Date currentDate = null;
    	SimpleDateFormat formatter = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
    	
    	if("".equals(sbox.get("S_DATE"))){
    		currentDate = new Date ();
    		Date sDate = new Date ( currentDate.getTime ( ) - (long) ( 1000 * 60 * 60 * 24 * 7) );
            s_date = formatter.format ( sDate );
    	}else{
    		s_date = sbox.get("S_DATE");
    	}
        if("".equals(sbox.get("E_DATE"))){
    		currentDate = new Date ();
            Date eDate = new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 * 7 ) );
            e_date = formatter.format ( eDate );
    	}else{
    		e_date = sbox.get("E_DATE");
    	}
        
        if("".equals(sbox.get("SEARCH_USER_NAME"))){
        	search_user_name = "ALL";
    	}else{
    		search_user_name = sbox.get("SEARCH_USER_NAME");
    	}
        if("".equals(sbox.get("SEARCH_MOB_DESC"))){
            search_mob_desc = "ALL";
    	}else{
    		search_mob_desc = sbox.get("SEARCH_MOB_DESC");
    	}
        
        String input_value[] = {s_date, e_date, sbox.get("U_DEPT_CODE")
        		, search_user_name, search_user_name
        		, search_mob_desc, search_mob_desc
        		};
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        //Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    /***
     * 사용자 스케쥴 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserLongScheduleList(Box sbox)throws PException{
    	
    	String mn = "getUserLongScheduleList";
    	String s_date = "";
    	String e_date = "";
    	String search_user_name = "";
    	String search_mob_desc = "";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn+""));
        sbox.put("db","default");
        
        Date currentDate = null;
    	SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
    	if("".equals(sbox.get("S_DATE"))){
    		currentDate = new Date ();
    		Date sDate = new Date ( currentDate.getTime ( ) - (long) ( 1000 * 60 * 60 * 24 * 7) );
            s_date = formatter.format ( sDate );
    	}else{
    		s_date = sbox.get("S_DATE");
    	}
        if("".equals(sbox.get("E_DATE"))){
    		currentDate = new Date ();
            Date eDate = new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 * 7 ) );
            e_date = formatter.format ( eDate );
    	}else{
    		e_date = sbox.get("E_DATE");
    	}
        if("".equals(sbox.get("SEARCH_USER_NAME"))){
        	search_user_name = "ALL";
    	}else{
    		search_user_name = sbox.get("SEARCH_USER_NAME");
    	}
        if("".equals(sbox.get("SEARCH_MOB_DESC"))){
            search_mob_desc = "ALL";
    	}else{
    		search_mob_desc = sbox.get("SEARCH_MOB_DESC");
    	}
        
        //String input_value[] = {s_date, e_date, sbox.get("U_DEPT_CODE")};
        String input_value[] = {
        		s_date, s_date, e_date, e_date, 
        		sbox.get("U_DEPT_CODE"),
        		search_user_name, search_user_name,
        		search_mob_desc, search_mob_desc
        		};
        //String input_value[] = {sbox.get("U_DEPT_CODE")};
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
	
	/***
	 * 
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject createSchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< createSchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		
		try {
			
			DaoHandler dao = new DaoHandler();

			// 1. KEY
			String mn = "getScheduleKey";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			sbox.put("db", "default");
			String key = dao.selectOneData(sbox, null);

			// 2. SCHEDULE
			mn = "createSchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			
			String input_value[] = { 
				key,
				sbox.getString("MOB_USER"),
				sbox.getString("MOB_SITE"),
				sbox.getString("MOB_DATE"),
				("on".equals(sbox.getString("AM"))?"Y":"N"),
				("on".equals(sbox.getString("PM"))?"Y":"N"), 
				("on".equals(sbox.getString("NIGHT"))?"Y":"N"), 
				("on".equals(sbox.getString("ALLNIGHT"))?"Y":"N"),
				sbox.getString("MOB_TYPE"),
				sbox.getString("MOB_STS"),
				
				sbox.getString("REQ_ID"),
				sbox.getString("MOB_RMT"),
				sbox.getString("MOB_SDATE").replaceAll("T", " "),
				sbox.getString("MOB_EDATE").replaceAll("T", " "),
				//sbox.getString("JTIME"),
				
				sbox.getString("MOB_DESC"),
				sbox.getString("U_DEPT_CODE"),
				sbox.getString("USER_ID"),
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value);
			
			/*
			// 3. SR_TYPE
			mn = "getSrType";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			String input_value2[] = {sbox.getString("SRTYPE")};
			String sr_type = dao.selectOneData(sbox, input_value2);
			
			// 4. CAL
			mn = "createScheduleEvent";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			
			String sdate = "";
			String edate = "";
			String tdate = sbox.getString("TDATE").substring(6,10) + sbox.getString("TDATE").substring(0,2) + sbox.getString("TDATE").substring(3,5);
			
			if("on".equals(sbox.getString("AM"))){
				sdate=tdate + "0900";
				edate=tdate + "1200";
			}
			if("on".equals(sbox.getString("PM"))){
				sdate=("".equals(sdate))?tdate + "1300":sdate;
				edate=tdate + "1800";
			}
			if("on".equals(sbox.getString("NIGHT"))){
				sdate=("".equals(sdate))?tdate + "1800":sdate;
				edate=tdate + "2400";
			}
			if("on".equals(sbox.getString("ALLNIGHT"))){
				
				String nextdate = "";
				SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
			    Date currentDate = new SimpleDateFormat("yyyyMMdd").parse(tdate);
			    nextdate = formatter.format ( new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 ) ) );
				
				sdate=("".equals(sdate))?nextdate + "2400":sdate;
				edate=nextdate + "0900";
			}
			
			//evt_id, cal_id, evt_titl, evt_stdt, evt_endt, evt_location, evt_notes, create_user, dept_code
			String input_value3[] = { 
				key,
				sbox.getString("SEL_USER_ID"),
				sbox.getString("SITE") + "(" + sr_type + ")", 
				sdate, // stdt
				edate, // etdt
				sbox.getString("SITE"), // evt_location
				sbox.getString("CONTENTS"), // evt_notes
				sbox.getString("USER_ID"), // create_user
				sbox.getString("U_DEPT_CODE")
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value3);
			*/
			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			jsonObj.accumulate("key", key);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< createSchedule() > execute Ended");
		return jsonObj;
	}
	
	
	/***
	 * 
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject modifySchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< modifySchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		String mn = "modifySchedule";
		
		try {
	        
			DaoHandler dao = new DaoHandler();

			// 1. ModifySchedule
			mn = "modifySchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
	        sbox.put("db","default");
			
			String input_value[] = { 
				sbox.getString("MOB_USER"),
				sbox.getString("MOB_SITE"),
				sbox.getString("MOB_DATE"),
				("on".equals(sbox.getString("AM"))?"Y":"N"),
				("on".equals(sbox.getString("PM"))?"Y":"N"), 
				("on".equals(sbox.getString("NIGHT"))?"Y":"N"), 
				("on".equals(sbox.getString("ALLNIGHT"))?"Y":"N"),
				sbox.getString("MOB_TYPE"),
				sbox.getString("MOB_STS"),
				
				sbox.getString("REQ_ID"),
				sbox.getString("MOB_RMT"),
				sbox.getString("MOB_SDATE").replaceAll("T", " "),
				sbox.getString("MOB_EDATE").replaceAll("T", " "),
				
				sbox.getString("U_DEPT_CODE"),
				sbox.getString("MOB_DESC"),
				sbox.getString("USER_ID"),
				sbox.getString("MOB_ID")
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value);
			
			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			jsonObj.accumulate("key", sbox.getString("REQ_ID"));
			jsonObj.accumulate("mobkey", sbox.getString("MOB_ID"));
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< createSchedule() > execute Ended");
		return jsonObj;
	}
	
	
	
	/***
	 * 
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject deleteSchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< deleteSchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		String mn = "";
		
		try {
	        
			DaoHandler dao = new DaoHandler();

			// 1. deleteSchedule
			mn = "deleteSchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
	        sbox.put("db","default");
			
			String input_value[] = { 
				sbox.getString("MOB_ID")
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value);

			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< createSchedule() > execute Ended");
		return jsonObj;
	}
	
	/***
	 * 
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject createLongSchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< createSchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		
		try {
			
			DaoHandler dao = new DaoHandler();

			// 1. KEY
			String mn = "getScheduleKey";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			sbox.put("db", "default");
			String key = dao.selectOneData(sbox, null);

			// 2. SCHEDULE
			mn = "createLongSchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			
			String input_value[] = { 
				key,
				sbox.getString("MOB_USER"),
				sbox.getString("MOB_SITE"),
				sbox.getString("MOB_SDATE"),
				sbox.getString("MOB_EDATE"),
				sbox.getString("MOB_TYPE"),
				sbox.getString("MOB_STS"),
				sbox.getString("U_DEPT_CODE"),
				sbox.getString("MOB_DESC"),
				sbox.getString("USER_ID"),
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value);
			
			
			/*
			// 3. SR_TYPE
			mn = "getSrType";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			String input_value2[] = {sbox.getString("SRTYPE")};
			String sr_type = dao.selectOneData(sbox, input_value2);
			
			// 4. CAL
			mn = "createScheduleEvent";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			
			String sdate = "";
			String edate = "";
			String tdate = sbox.getString("TDATE").substring(6,10) + sbox.getString("TDATE").substring(0,2) + sbox.getString("TDATE").substring(3,5);
			
			if("on".equals(sbox.getString("AM"))){
				sdate=tdate + "0900";
				edate=tdate + "1200";
			}
			if("on".equals(sbox.getString("PM"))){
				sdate=("".equals(sdate))?tdate + "1300":sdate;
				edate=tdate + "1800";
			}
			if("on".equals(sbox.getString("NIGHT"))){
				sdate=("".equals(sdate))?tdate + "1800":sdate;
				edate=tdate + "2400";
			}
			if("on".equals(sbox.getString("ALLNIGHT"))){
				
				String nextdate = "";
				SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
			    Date currentDate = new SimpleDateFormat("yyyyMMdd").parse(tdate);
			    nextdate = formatter.format ( new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 ) ) );
				
				sdate=("".equals(sdate))?nextdate + "2400":sdate;
				edate=nextdate + "0900";
			}
			
			//evt_id, cal_id, evt_titl, evt_stdt, evt_endt, evt_location, evt_notes, create_user, dept_code
			String input_value3[] = { 
				key,
				sbox.getString("SEL_USER_ID"),
				sbox.getString("SITE") + "(" + sr_type + ")", 
				sdate, // stdt
				edate, // etdt
				sbox.getString("SITE"), // evt_location
				sbox.getString("CONTENTS"), // evt_notes
				sbox.getString("USER_ID"), // create_user
				sbox.getString("U_DEPT_CODE")
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value3);
			*/
			
			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< createSchedule() > execute Ended");
		return jsonObj;
	}
	
	/***
	 * 
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject modifyLongSchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< modifyLongSchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		String mn = "";
		
		try {
	        
			DaoHandler dao = new DaoHandler();

			// 1. ModifyLongSchedule
			mn = "modifyLongSchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
	        sbox.put("db","default");
			
			String input_value[] = { 
				sbox.getString("MOB_USER"),
				sbox.getString("MOB_SITE"),
				sbox.getString("MOB_SDATE"),
				sbox.getString("MOB_EDATE"),
				sbox.getString("MOB_TYPE"),
				sbox.getString("MOB_STS"),
				sbox.getString("U_DEPT_CODE"),
				sbox.getString("MOB_DESC"),
				sbox.getString("USER_ID"),
				sbox.getString("MOB_ID")
			};
			
			resultCode = dao.updateObjectLoggable(sbox, input_value);
			
			/*
			// 2. SR_TYPE
			mn = "getSrType";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
			String input_value2[] = {sbox.getString("SRTYPE")};
			String sr_type = dao.selectOneData(sbox, input_value2);
			
			// 3. ModifySchedule
			mn = "modifyScheduleEvent";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
	        sbox.put("db","default");
	        
	        String sdate = "";
			String edate = "";
			String tdate = sbox.getString("TDATE").substring(6,10) + sbox.getString("TDATE").substring(0,2) + sbox.getString("TDATE").substring(3,5);
			
			if("on".equals(sbox.getString("AM"))){
				sdate=tdate + "0900";
				edate=tdate + "1200";
			}
			if("on".equals(sbox.getString("PM"))){
				sdate=("".equals(sdate))?tdate + "1300":sdate;
				edate=tdate + "1800";
			}
			if("on".equals(sbox.getString("NIGHT"))){
				sdate=("".equals(sdate))?tdate + "1800":sdate;
				edate=tdate + "2400";
			}
			if("on".equals(sbox.getString("ALLNIGHT"))){
				
				String nextdate = "";
				SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
			    Date currentDate = new SimpleDateFormat("yyyyMMdd").parse(tdate);
			    nextdate = formatter.format ( new Date ( currentDate.getTime ( ) + (long) ( 1000 * 60 * 60 * 24 ) ) );
				
				sdate=("".equals(sdate))?nextdate + "2400":sdate;
				edate=nextdate + "0900";
			}
			
			//cal_id , evt_titl , evt_stdt , evt_endt , evt_location , evt_notes , update_user , dept_code 
			//evt_id = ?
			String input_value3[] = { 
				sbox.getString("SEL_USER_ID"),
				sbox.getString("SITE") + "(" + sr_type + ")", 
				sdate, // stdt
				edate, // etdt
				sbox.getString("SITE"), // evt_location
				sbox.getString("CONTENTS"), // evt_notes
				sbox.getString("USER_ID"), // create_user
				sbox.getString("U_DEPT_CODE"),
				sbox.getString("MOB_ID")
			};
			resultCode = dao.updateObjectLoggable(sbox, input_value3);
			*/
			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< modifySchedule() > execute Ended");
		return jsonObj;
	}
	
	
	/***
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public JSONObject deleteLongSchedule(Box sbox) throws PException{

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< deleteLongSchedule() > execute Starting");
		JSONObject jsonObj = new JSONObject();
		String mn = "";
		
		try {
	        
			DaoHandler dao = new DaoHandler();

			// 1. deleteLongSchedule
			mn = "deleteLongSchedule";
			sbox.put("query",StatementManager.getStatement("TGMOB01", ""+mn.trim()+""));
	        sbox.put("db","default");
			
			String input_value[] = { 
				sbox.getString("MOB_ID")
			};
			
			resultCode = dao.updateObjectLoggable(sbox, input_value);
			
			jsonObj.accumulate("success", "true");
			jsonObj.accumulate("data", resultCode);
			
		} catch (Exception e) {
			e.printStackTrace();
			resultCode = "{success:false,data:{responseMsg:'FAIL'}}";
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< deleteSchedule() > execute Ended");
		return jsonObj;
	}
	
}

