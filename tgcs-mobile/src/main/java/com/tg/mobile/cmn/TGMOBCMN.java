package com.tg.mobile.cmn;

import net.sf.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.vo.box.Box;


/***
 * 
 * @author kds
 *
 */
public class TGMOBCMN {
	
    /***
     * 부서별 사용자 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserListOfDept(Box sbox)throws PException{
    	
    	String mn = "getUserListOfDept";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = {sbox.getString("U_DEPT_CODE")};
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    /***
     * 부서별 사용자 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserListOfDeptMain(Box sbox)throws PException{
    	
    	String mn = "getUserListOfDeptMain";
    	String search_date = "";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
        
        if("".equals(sbox.get("SEARCH_DATE"))){
        	SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA ); 
        	Date currentTime = new Date (); 
        	search_date = formatter.format ( currentTime );
        }else{
        	search_date = sbox.get("SEARCH_DATE");
        }
        String input_value[] = {
        		sbox.get("U_DEPT_CODE"), 
        		search_date, sbox.get("U_DEPT_CODE")
        		, search_date, search_date, search_date, search_date, sbox.get("U_DEPT_CODE")
        		};
 
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    /***
     * 부서별 사용자 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getUserListOfDeptWeek(Box sbox)throws PException{
    	
    	String mn = "getUserListOfDeptWeek";
    	String start_date = sbox.get("START_DATE");
    	String end_date = sbox.get("END_DATE");
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
        
        String input_value[] = {
        		sbox.get("U_DEPT_CODE") 
        		,start_date, end_date , sbox.get("U_DEPT_CODE")
        		,start_date, start_date, end_date, end_date, sbox.get("U_DEPT_CODE")
        		};
 
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
                
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    
    /***
     * SrType 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getSrType(Box sbox)throws PException{
    	
    	String mn = "getCodeList";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = { "1001" };
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    
    /***
     * SupType 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getSupType(Box sbox)throws PException{
    	
    	String mn = "getCodeList";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = { "4001" };
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    
    /***
     * SvcType 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getSvcType(Box sbox)throws PException{
    	
    	String mn = "getCodeList";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = { "5001" };
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    
    /***
     * SrType 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getSrStatus(Box sbox)throws PException{
    	
    	String mn = "getCodeList";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = { "2001" };
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
    /***
     * SrType 조회
     * @param sbox
     * @return
     * @throws PException
     */ 
    public JSONObject getPjtType(Box sbox)throws PException{
    	
    	String mn = "getCodeList";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< "+mn+"() > execute Starting\n" + sbox);
        
        sbox.put("query",StatementManager.getStatement("TGMOBCMN", ""+mn+""));
        sbox.put("db","default");
 
        String input_value[] = { "3001" };
        
        DaoHandler dao = new DaoHandler();
        JSONObject jsonObj = dao.selectListJson(sbox,input_value);
        
        Logging.debug.println("< "+this.getClass().getName()+" >< "+mn+"() > execute Ended\n"+jsonObj);
        return jsonObj;
    }
    
}
