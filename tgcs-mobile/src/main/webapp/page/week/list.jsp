<%-- Program info --%>
<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB01" %>
<%@ page import="com.tg.mobile.util.DateUtils" %>
<%@ page import="net.sf.json.JSONObject" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/custom.tld"%>

<%

	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");
    
    String start_date = ("".equals(sessionbox.getString("SEARCH_WEEK"))?"":sessionbox.getString("SEARCH_WEEK"));
    String end_date =  ("".equals(start_date)?"":DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", 6, 0, 0 ));

    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";
    
	String today = DateUtils.getToday("yyyyMMdd");
    
    if("".equals(sessionbox.getString("SEARCH_WEEK"))) start_date = DateUtils.firstMondayWeek(today);
    if("".equals(sessionbox.getString("SEARCH_WEEK")))  end_date = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", 6, 0, 0 );
    sessionbox.put("START_DATE", start_date);
    sessionbox.put("END_DATE", end_date);

    if(user_status == null){

    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }
    }
    
    TGMOBCMN cmn = new TGMOBCMN(); 
    JSONObject userList = cmn.getUserListOfDeptWeek(sessionbox);
    
    TGMOB01 mob01 = new TGMOB01(); 
    Map scheduleMap = mob01.getUserWeekScheduleList(sessionbox);
    
    String [] dayWeek = new String[7];
    String [] dayWeekName = {"월","화","수","목","금","토","일"};
    for(int i = 0 ; i < 7 ; i++)
    	dayWeek[i] = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", i, 0, 0 );

%>
<!DOCTYPE html>
<html>
<head>

	<title>Enterprise Solution Team</title>
	<META HTTP-EQUIV="Refresh" CONTENT="300;URL=./list.jsp?SEARCH_WEEK=<%=sessionbox.getString("SEARCH_WEEK")%>">

<style type="text/css">
 <!--
.button1{
border:1px solid #393B3E;-webkit-box-shadow: #737373 0px 1px 5px ;-moz-box-shadow: #737373 0px 1px 5px ; box-shadow: #737373 0px 1px 5px ; -webkit-border-radius: 23px; -moz-border-radius: 23px;border-radius: 23px;font-size:14px;font-family:arial, helvetica, sans-serif; padding: 10px 20px 10px 20px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(10,10,10,0.5);font-weight:bold; color: #8C8C8C;
 background-color: #515459; background-image: -webkit-gradient(linear, left top, left bottom, from(#515459), to(#3B3C40));
 background-image: -webkit-linear-gradient(top, #515459, #3B3C40);
 background-image: -moz-linear-gradient(top, #515459, #3B3C40);
 background-image: -ms-linear-gradient(top, #515459, #3B3C40);
 background-image: -o-linear-gradient(top, #515459, #3B3C40);
 background-image: linear-gradient(to bottom, #515459, #3B3C40);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#515459, endColorstr=#3B3C40);
}

.button1:hover{
 border:1px solid #393B3E;
 background-color: #3B3C40; background-image: -webkit-gradient(linear, left top, left bottom, from(#3B3C40), to(#202124));
 background-image: -webkit-linear-gradient(top, #3B3C40, #202124);
 background-image: -moz-linear-gradient(top, #3B3C40, #202124);
 background-image: -ms-linear-gradient(top, #3B3C40, #202124);
 background-image: -o-linear-gradient(top, #3B3C40, #202124);
 background-image: linear-gradient(to bottom, #3B3C40, #202124);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#3B3C40, endColorstr=#202124);
}

.button2{
border:1px solid #CCCCCC;-webkit-box-shadow: #FEFFFF 0px 1px 1px ;-moz-box-shadow: #FEFFFF 0px 1px 1px ; box-shadow: #FEFFFF 0px 1px 1px ; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 6px 6px 6px 6px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(255,255,255,1);font-weight:bold; color: #4A4A4A;
 background-color: #F7F5F6; background-image: -webkit-gradient(linear, left top, left bottom, from(#F7F5F6), to(#DDDDDD));
 background-image: -webkit-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -moz-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -ms-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -o-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: linear-gradient(to bottom, #F7F5F6, #DDDDDD);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#F7F5F6, endColorstr=#DDDDDD);
}

.button2:hover{
 border:1px solid #ADADAD;
 background-color: #E0E0E0; background-image: -webkit-gradient(linear, left top, left bottom, from(#E0E0E0), to(#BDBBBC));
 background-image: -webkit-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -moz-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -ms-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -o-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: linear-gradient(to bottom, #E0E0E0, #BDBBBC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E0E0E0, endColorstr=#BDBBBC);
}

* { 
	margin: 0; 
	padding: 0; 
}
body { 
	#font: 14px/1.4 Georgia, Serif;
	font: 68.5%/1.3 Helvetica, sans-serif; 
}
#page-wrap {
	margin: 10px;
}

#fontSize {
	font-size:14px;
}
a {
	margin: 10px 0;
	font-size:12px; 
}

	/* 
	Generic Styling, for Desktops/Laptops 
	*/
	table { 
		width: 100%; 
		border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white;
		text-align:center; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px;
		border: 1px solid #ccc; 
		text-align: center; 
	}
	
	 /* 기본 설정*/
     /* nav tag */
      nav ul{padding-top:10px;}                     /*  상단 여백 10px  */
      nav ul li {
         display:inline;                         /*  세로나열을 가로나열로 변경 */
         #border-left:1px solid #999;             /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
         font:bold 12px Dotum;                     /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
         padding:0 20px;                         /* 각 메뉴 간격 */
     }
      nav ul li:first-child{border-left:none;}     /* 메뉴 분류중 제일 왼쪽의 "|"는 삭제    
	-->
</style>	  
<script type="text/javascript">
</script>
	
</head>

<body>

<% 
	String prev_week = "";
	String next_week = "";
	
	prev_week = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", -7, 0, 0 );
	next_week = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", 7, 0, 0 );
    
%>

<div align=center>

	<!-- /header -->
	<nav>
        <ul>
            <li><a class="button1" href="${pageContext.request.contextPath}/page/main/main.jsp" title="홈">홈</a></li>
            <li>
			<a class="button2" href="${pageContext.request.contextPath}/page/week/list.jsp?SEARCH_WEEK=<%=prev_week %>" title="이전">이전</a> | <%=start_date.substring(0,4) %>-<%=start_date.substring(4,6) %>-<%=start_date.substring(6,8) %> ~ <%=end_date.substring(0,4) %>-<%=end_date.substring(4,6) %>-<%=end_date.substring(6,8) %>
			| <a class="button2" href="${pageContext.request.contextPath}/page/week/list.jsp?SEARCH_WEEK=<%=next_week %>" title="다음">다음</a>
			</li>
            <li><a class="button1" href="${pageContext.request.contextPath}/page/main/input.jsp" title="등록">등록</a></li>
        </ul>    
    </nav>  
	
    <!-- /header -->
    
	<!--  Contents  -->
    <div>
		
		<table width="1024px">
         <thead>
         
         <c:set var="dayWeekData" value="<%=dayWeek %>" />
         <c:set var="dayWeekNameData" value="<%=dayWeekName %>" />
         <fmt:formatDate value="<%=new java.util.Date()%>" var="now_date"  pattern="yyyyMMdd"/>
         <tr>
         
         <th width="2%" rowspan=2>직급</th>
         <th width="3%" rowspan=2>이름</th>
         <c:forEach var="result" items="${dayWeekData}" varStatus="status" >
         	 <fmt:parseDate value="${result}" var="dateFmt" pattern="yyyyMMdd"/>
             <fmt:formatDate value="${dateFmt}" var="date_today" pattern="yyyyMMdd"/>
             <th  width="9%" colspan="3"
             <c:if test="${now_date eq date_today}"> style="background-color: #0572ce" </c:if>
             >
             <fmt:formatDate value="${dateFmt}"  pattern="yyyy-MM-dd"/>
             (${dayWeekNameData[status.count-1]})
             </th>
         </c:forEach>
         </tr>
           <tr>
           <c:forEach var="result" items="${dayWeekData}" varStatus="status" >
           	 <fmt:parseDate value="${result}" var="dateFmt" pattern="yyyyMMdd"/>
           	 <fmt:formatDate value="${dateFmt}" var="date_today" pattern="yyyyMMdd"/>
             <th width="3%" <c:if test="${now_date eq date_today}"> style="background-color: #0572ce" </c:if> >오전</th>
             <th width="3%" <c:if test="${now_date eq date_today}"> style="background-color: #0572ce" </c:if> >오후</th>
             <th width="3%" <c:if test="${now_date eq date_today}"> style="background-color: #0572ce" </c:if> >야간(철야)</th>
         </c:forEach>
		</tr>         
         </thead>
         <tbody>
         <c:set var="userListData" value="<%=userList %>" />
         <c:forEach var="result" items="${userListData.DATA_LIST}" varStatus="status" >
         <tr>
         	<td>${result.POSITION_NAME}</td>
         	<td>${result.U_NAME}</td>
         	<c:forEach var="dayresult" items="${dayWeekData}" varStatus="status" >
         	<c:set var="map" value="<%=scheduleMap%>" />
 			<c:set var="key" value="${result.USER_ID}${dayresult}" />
 			<c:set var="mob_key" value="${map[key]}" />
         	
			<c:if test="${mob_key eq null}">
	             <td><ct:weekvalue idate="${dayresult}" /></td>
	             <td><ct:weekvalue idate="${dayresult}" /></td>
	             <td></td>
             </c:if>
             <c:if test="${mob_key ne null}">
             	<!-- bgcolor="#fae100" -->
             	<c:choose>

				    <c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB ne null}">
				       	<td bgcolor="#fae100" colspan=3 >
				       	<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
				       	<c:if test="${mob_key.MOB3 ne null}">(철야)</c:if></td>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB ne null}">
			    		<td bgcolor="#fae100" colspan=3 >
			    		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB == mob_key.MOB1 && mob_key.MOB ne null}">
			    		<td bgcolor="#fae100" colspan=2 >
			    		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
						</td>
				       	<c:choose>
					    <c:when test="${mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB2 ne null}">
				       		<td bgcolor="#fae100" >
				       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />(철야)
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		
				    		<c:choose>
						    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB2 ne null}">
					       		<td bgcolor="#fae100" >
					       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
								<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
					    	</c:when>
					    	<c:otherwise>
					    		<td></td>
					    	</c:otherwise>
					    	</c:choose>
				    	
				    	</c:otherwise>
				    	</c:choose>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB2 == mob_key.MOB3 && mob_key.MOB1 ne null }">
			       	
			       	<c:choose>
				    <c:when test="${mob_key.MOB ne null}">
			       		<td bgcolor="#fae100" >
			       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
						</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="${dayresult}" /></td>
			    	</c:otherwise>
			    	</c:choose>
			    	
			    	<c:choose>
				    <c:when test="${mob_key.MOB1 ne null}">
			       		<td bgcolor="#fae100" colspan=2 >
			       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />(철야)
			       		</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="${dayresult}" /></td>
			    	</c:otherwise>
			    	</c:choose>
			       	
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB1 == mob_key.MOB2 && mob_key.MOB1 ne null}">
			    	
			    	<c:choose>
				    <c:when test="${mob_key.MOB ne null}">
			       		<td style="background-color: #fae100" >
			       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
			       		</td>
			    	</c:when>
			    	<c:otherwise>
			    		<td><ct:weekvalue idate="${dayresult}" /></td>
			    	</c:otherwise>
			    	</c:choose>
			    	
			       	<td bgcolor="#fae100" colspan=2 >
					<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
					<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
			    	</c:when>
			    	
			    	<c:when test="${mob_key.MOB2 == mob_key.MOB3}">
			    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB ne null}">
				       		<td bgcolor="#fae100" >
				       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="${dayresult}" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB1 ne null}">
				       		<td bgcolor="#fae100" >
				       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="${dayresult}" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB3 ne null}">
					    	
					    	<c:choose>
						    <c:when test="${mob_key.MOB2 == mob_key.MOB3}">
					       		<td bgcolor="#fae100" >
					       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
					    		<c:if test="${mob_key.SCH3 ne null}">(철야)</c:if>
					       		</td>
					    	</c:when>
					    	<c:otherwise>
					    		<td bgcolor="#fae100" >
					       		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
					    		<c:if test="${mob_key.MOB3 ne null}">4(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if>
					       		</td>
					    	</c:otherwise>
					    	</c:choose>
					    	
				    	</c:when>
				    	<c:otherwise>
				    	
				    		<c:choose>
					    	<c:when test="${mob_key.MOB2 ne null || mob_key.MOB3 ne null}">
				    		<td bgcolor="#fae100">
				    		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
				    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if>
				    		</td>
				    		</c:when>
				    		<c:otherwise>
				    		<td>
				    		</td>
				    		</c:otherwise>
				    		</c:choose>
				    		
				    	</c:otherwise>
				    	</c:choose>
			       	
			    	</c:when>
			    	
				    <c:otherwise>
				    
				    	<c:choose>
					    <c:when test="${mob_key.MOB ne null}">
				       		<td bgcolor="#fae100" >
				       		<ct:linktag var="${mob_key.SCH}" mobid="${mob_key.MOB}" type="${mob_key.TP}" />
							</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="${dayresult}" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB1 ne null}">
				       		<td bgcolor="#fae100" >
				       		<ct:linktag var="${mob_key.SCH1}" mobid="${mob_key.MOB1}" type="${mob_key.TP1}" />
				       		</td>
				    	</c:when>
				    	<c:otherwise>
				    		<td><ct:weekvalue idate="${dayresult}" /></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    	<c:choose>
					    <c:when test="${mob_key.MOB2 ne null && mob_key.MOB3 ne null}">
					    	<td bgcolor="#fae100">
							<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
							<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
				    	</c:when>
				    	<c:otherwise>
				    		<td bgcolor="#fae100">
				    		<ct:linktag var="${mob_key.SCH2}" mobid="${mob_key.MOB2}" type="${mob_key.TP2}" />
				    		<c:if test="${mob_key.MOB3 ne null}">(<ct:linktag var="${mob_key.SCH3}" mobid="${mob_key.MOB3}" type="${mob_key.TP3}" />)</c:if></td>
				    	</c:otherwise>
				    	</c:choose>
				    	
				    </c:otherwise>
				    
				</c:choose>
	              
             </c:if>
             
			
         	</c:forEach>
         </tr>
         </c:forEach>
         
         </tbody>
       </table>
	 
    </div>
    <!--  Contents  -->
    
    <!--  Footer -->
	<!--  Footer -->
	    
    
</div>
 
</body>
</html>