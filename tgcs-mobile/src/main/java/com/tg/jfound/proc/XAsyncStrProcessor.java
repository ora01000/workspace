package com.tg.jfound.proc; 


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tg.jfound.exception.PException;
import com.tg.jfound.vo.box.Box;


public interface XAsyncStrProcessor 
{ 
    //public ProcessMsg procXAsyncRequest(HttpServletRequest req, HttpServletResponse res) throws PException;
    public String  procXAsyncRequest(HttpServletRequest req, HttpServletResponse res) throws PException;
    public String  procXAsyncMultipartRequest(Box sbox) throws PException;

} 
