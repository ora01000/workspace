<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.util.DateUtils" %>
<%@ page import="net.sf.json.JSONObject" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>
<%
Box search_sessionbox = CollectionUtility.getBoxWithSession(request);
String search_today = DateUtils.getToday("yyyy-MM-dd");

SimpleDateFormat search_formatter = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
String search_todate = search_formatter.format ( new Date ( (new Date ()).getTime ( ) - (long) ( 1000 * 60 * 60 * 24 * 7) ) );
%>
<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->

<div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a"> 
	<div class="jqm-search">
		<form  id="search-form" action="${pageContext.request.contextPath}/page/longterm/list.jsp" method="post"  data-ajax="false"
			class="ui-body ui-body-a ui-corner-all">

		 <div data-role="fieldcontain">
		 	<label for="select-choice-a-search" data-mini="true" >담당자:</label>
			<input type="text" name="SEARCH_USER_NAME" id="SEARCH_USER_NAME" value=""  data-mini="true" />
		</div>

		<div data-role="fieldcontain">
			<label for="S_DATE">시작:</label>
     		<input type="date" data-clear-btn="true" name="S_DATE" id="S_DATE" value="<%=search_todate%>" data-mini="true" >
		</div>
		<div data-role="fieldcontain">
			<label for="E_DATE">종료:</label>
     		<input type="date" data-clear-btn="true" name="E_DATE" id="E_DATE" value="<%=search_today%>" data-mini="true" >
		</div>
		
		<div data-role="fieldcontain">
			<label for="SEARCH_MOB_DESC" class="select" data-mini="true">내용:</label>
			<input type="text" name="SEARCH_MOB_DESC" id="SEARCH_MOB_DESC" value=""  data-mini="true" />
		</div>
		
		<div class="errorTxt"></div>
		
		<div data-role="fieldcontain">
		<fieldset class="ui-grid-a">
	    <div class="ui-block-a"><input type="submit" value="조회" data-theme="a"></div>
	    <div class="ui-block-b"><input type="reset" value="Reset" data-theme="b"></div>
		</fieldset>
		</div>
		
		</form>
  
	</div>
</div><!-- /panel -->
