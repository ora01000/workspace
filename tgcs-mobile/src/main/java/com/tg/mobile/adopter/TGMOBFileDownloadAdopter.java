package com.tg.mobile.adopter;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

import org.apache.commons.io.IOUtils;

import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.util.OSUtil;
import com.tg.jfound.vo.box.Box;
import com.tg.jfound.vo.box.CollectionUtility;
import com.tg.jfound.web.BaseAbstractServlet;

public class TGMOBFileDownloadAdopter extends BaseAbstractServlet {

	protected void catchService( javax.servlet.http.HttpServletRequest req,
								 javax.servlet.http.HttpServletResponse res)
	{
		Box sbox = CollectionUtility.getBox(req);

		try {
			String FID = sbox.getString("FID");
			Box sessionbox = CollectionUtility.getBoxWithSession(req);
			System.out.println("FID ::  "+ FID);
			System.out.println("sessionbox ::  "+ sessionbox);
			
			sbox.put("query", StatementManager.getStatement("TGFSLIST01","getFileDownload"));
			sbox.put("db", "default");

			String input_value[] = {FID, sessionbox==null?"":"SALT"};

			DaoHandler dao = new DaoHandler();
			Box resultBox = dao.selectListBox(sbox, input_value);
			
			System.out.println("RESULT ::  "+resultBox.toString());
			
			String orgFileName = resultBox.getString("ORG_FILE_NM");
			String sysFileName = resultBox.getString("SYS_FILE_NM");
			String filePath = resultBox.getString("FILE_PATH");
					
			Logging.dev.println("< FileDownloadAdopter > DownLoaings, File Path: "+filePath+OSUtil.getOsDelemeter()+sysFileName);
			File file = new File(filePath+OSUtil.getOsDelemeter()+sysFileName);
			
			byte b[] = new byte[(int)file.length()];
			sysFileName = new String(sysFileName.getBytes("UTF-8"), "ISO-8859-1"); 

			Logging.dev.println("< FileDownloadAdopter > DownLoaings, File: "+orgFileName+" Size: "+file.length());

			res.setContentType("application/download");
			res.setHeader("Content-Disposition", "filename=" + orgFileName + ";");
			res.setHeader("Content-Transfer-Encoding", "binary;");
			res.setHeader("Content-Length", "" + file.length());
			res.setHeader("Pragma", "no-cache;");
			res.setHeader("Expires", "-1;");
			
			FileInputStream fis = new FileInputStream(file);
			OutputStream os = res.getOutputStream();
			

			if (file.isFile()) {
				
				try {
					IOUtils.copy(fis, os);
				} catch (Exception excp) {
				    excp.printStackTrace();
				} finally {
				    IOUtils.closeQuietly(os);
				    IOUtils.closeQuietly(fis);
				}
			    
				/*
				int BUFF_SIZE = 1024;
				byte[] buffer = new byte[BUFF_SIZE];
				
				int byteCount = 0;
				try {
				    do {
				        byteCount = fis.read(buffer);
				        if (byteCount == -1)
				           break;
				        os.write(buffer, 0, byteCount);
				        os.flush();
				    } while (true);
				} catch (Exception excp) {
				    excp.printStackTrace();
				} finally {
				    os.close();
				    fis.close();
				}
				*/
			}
        } catch(Exception e) {
            e.printStackTrace();
			Logging.err.println("<FileDownloadAdopter> Catche the Occurred Exception While User IP ["+req.getRemoteAddr()+"]\n" + e);
			this.printErr(req,res,"<FileDownloadAdopter> Exception occurred " + e, e);
		}
	}
}
