<%-- Program info --%>
<%
	/*
	 *  Program Type : JSP
	 *  Program ID   :
	 *
	 *  DESC         :
	 *  Author       :
	 *  Date         :
	 *  Update       :
	 */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box"%>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility"%>
<%
	Box sessionbox = CollectionUtility.getBoxWithSession(request);
	String user_status = sessionbox.getString("U_STATUS");

	String user_id = "";
	String rtn_code = "";
	String rtn_msg = "";
	String err_msg = "";
	
	String c_u_id = "";
	String c_u_pwd = "";
	String c_maintain = "";
	
	Cookie[] cookies = request.getCookies();            // 요청정보로부터 쿠키를 가져온다.
	for(int i = 0 ; i<cookies.length; i++){                            // 쿠키 배열을 반복문으로 돌린다.
		if("C_ID".equals(cookies[i].getName())) c_u_id = cookies[i].getValue();
		if("C_PWD".equals(cookies[i].getName())) c_u_pwd = cookies[i].getValue();
		if("C_MT".equals(cookies[i].getName())) c_maintain = cookies[i].getValue();
	}
	//System.out.println(" ==> " + c_u_id);
	//System.out.println(" ==> " + c_u_pwd);
	//System.out.println(" ==> " + c_maintain);

	if (user_status == null) {
	} else {

		rtn_code = sessionbox.getString("returnCode");
		rtn_msg = sessionbox.getString("returnMsg");

		if (user_status.equals("Y")) {
			String URL = request.getContextPath()+ "/page/login/login_ok.jsp";
			response.sendRedirect(URL);
		} else {
			err_msg = rtn_code + ": " + rtn_msg;
		}

	}
	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Enterprise Solution Team</title>
<link rel="shortcut icon" href="favicon.ico">

<%@ include file="../common/common_script.jsp" %>
<%
if("on".equals(c_maintain)){
%>
<script type="text/javascript">
$(document).ready(function () {
	$("#login_form").submit();
});
</script>
<%
}else{
%>
<script type="text/javascript">
$(document).ready(function () {
	//주소창 자동 닫힘 
	$( "#login_form" ).validate({ 
		submitHandler: function( form ) {
			return true;
		},
		//규칙
		rules : {
			C_ID : {
				required : true
				},
			C_PWD : {
				required : true
				}
		},
		//메세지
		messages : {
			C_ID : {
				required : " * 필수입력 사항입니다. "
				},
			C_PWD : {
				required : " * 필수입력 사항입니다. "
			}
		},
		errorElement : 'div',
	    errorLabelContainer: '.errorTxt'
	});
});
</script>
<%} %>
</head>
<body>

	<div data-role="page" class="jqm-demos" data-quicklinks="true">

		<form action="${pageContext.request.contextPath}/page/login/login_ok.jsp" method="post" id="login_form"
			class="ui-body ui-body-a ui-corner-all">

			<!--  Contents  -->
			<div role="main" class="ui-content jqm-content">

				<div class="ui-corner-all custom-corners">
					<div class="ui-bar ui-bar-a">
						<h4>Login</h4>
					</div>
					<div class="ui-body ui-body-a">

						<ul data-role="listview">

							<li data-role="fieldcontain"><label for="name">ID :</label>
								<input type="text" name="C_ID" id="C_ID" value="<%=c_u_id %>" /></li>

							<li data-role="fieldcontain"><label for="name">PWD :</label>
								<input name="C_PWD" id="C_PWD" type="password" value="<%=c_u_pwd %>" autocomplete="off"></li>
								
							<li data-role="fieldcontain">
								<label for="slider2">로그인유지:</label>
								<select name="maintain" id="maintain" data-role="slider">
								    <option value="off">Off</option>
								    <option value="on" <%=("on".equals(c_maintain)?"selected":"") %>>On</option>
								</select>
							</li>
							
							<div class="errorTxt"></div>

							<li class="ui-body ui-body-b">

								<fieldset class="ui-grid-a">
									<div class="ui-block-a">
										<input id=submit type="submit" value="Submit" data-theme="a">
									</div>
									<div class="ui-block-b">
										<input id=reset type="reset" value="Reset" data-theme="b">
									</div>
								</fieldset>
							</li>
						</ul>
					</div>
				</div>

			</div>
			<input name="taction" id="taction" type="hidden" value="LOGIN" >
		</form>

	</div>
</body>
</html>