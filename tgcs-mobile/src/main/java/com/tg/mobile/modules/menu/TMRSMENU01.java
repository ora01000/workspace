package com.tg.mobile.modules.menu;

import net.sf.json.JSONObject;

import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;
import com.tg.mobile.dao.TMRSCMNDAO;
import com.tg.mobile.dao.TMRSDAO;
import com.tg.jfound.util.cmnUtil;

/***
 * 
 * @author kds
 * 
 */
public class TMRSMENU01 {
	String resultCode = "FAIL";
	cmnUtil cmnUtil = new cmnUtil();

	public ProcessMsg searchMenuTreeList(Box sbox) throws PException {

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchMenuTreeList() > execute Starting");

		sbox.put("query", StatementManager.getStatement("TMRSMENU01",
				"searchMenuTreeList"));
		sbox.put("db", "default");

		//String input_value[] = { sbox.getString("U_SYS_AUTH") };// sbox.getString("p_root_menu_id")};
		String input_value[] = { };

		TMRSCMNDAO dao = new TMRSCMNDAO();

		JSONObject jsonObj = dao.selectMenuTreeList(null, sbox, input_value);
		String rtn_str = jsonObj.get("DATA_LIST").toString();

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchMenuTreeList() > execute Ended");

		return new ProcessMsg(rtn_str);

	}

	public ProcessMsg searchWikiMenuTreeList(Box sbox) throws PException {

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchWikiMenuTreeList() > execute Starting");

		sbox.put("query", StatementManager.getStatement("TMRSMENU01",
				"searchWikiMenuTreeList"));
		sbox.put("db", "default");

		//String input_value[] = { sbox.getString("U_SYS_AUTH") };// sbox.getString("p_root_menu_id")};
		String input_value[] = { };
		
		TMRSCMNDAO dao = new TMRSCMNDAO();

		JSONObject jsonObj = dao.selectMenuTreeList(null, sbox, input_value);
		String rtn_str = jsonObj.get("DATA_LIST").toString();

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchWikiMenuTreeList() > execute Ended");

		return new ProcessMsg(rtn_str);

	}	
	
	
	
	////////////////////////////////////////////////////
	
	
	
	
	/***
	 * 
	 * ?떆?뒪?뀥 硫붾돱 議고쉶
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg searchMenuList(Box sbox) throws PException {

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchMenuList() > execute Starting");

		sbox.put("query",
				StatementManager.getStatement("TMRSMENU01", "searchMenuList"));
		sbox.put("db", "default");

		String input_value[] = { sbox.getString("p_root_menu_id") };

		DaoHandler dao = new DaoHandler();
		JSONObject jsonObj = dao.selectListJson(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< searchMenuList() > execute Ended");
		return new ProcessMsg(jsonObj);

	}

	/**
	 * 
	 * ?떆?뒪?뀥 硫? ?깮?꽦
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg addMenu(Box sbox) throws PException {

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< addMenu() > execute Starting");

		sbox.put("query",
				StatementManager.getStatement("TMRSMENU01", "addMenu"));
		sbox.put("db", "default");

		String input_value[] = { sbox.getString("p_menu_id"),
				sbox.getString("p_root_menu_id"),
				sbox.getString("p_menu_name"), sbox.getString("p_menu_url"),
				sbox.getString("p_auth_level") };

		resultCode = TMRSDAO.updateTrx(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< addMenu() > execute Ended");
		return new ProcessMsg(resultCode);

	}

	/***
	 * 
	 * ?떆?뒪?뀥 硫? ?젙蹂? ?닔?젙
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg updateMenu(Box sbox) throws PException {

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< updateMenu() > execute Starting");

		sbox.put("query",
				StatementManager.getStatement("TMRSMENU01", "updateMenu"));
		sbox.put("db", "default");

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< updateMenu() > Update Menu ID:"
				+ sbox.getString("p_org_menu_id"));

		String input_value[] = { sbox.getString("p_menu_id"),
				sbox.getString("p_menu_name"), sbox.getString("p_menu_url"),
				sbox.getString("p_auth_level"), sbox.getString("p_org_menu_id")

		};

		resultCode = TMRSDAO.updateTrx(sbox, input_value);

		Logging.debug.println("< " + this.getClass().getName()
				+ " >< updateMenu() > execute Ended");
		return new ProcessMsg(resultCode);

	}

	/***
	 * 
	 * ?떆?뒪?뀥 肄붾뱶 ?젙蹂? ?궘?젣
	 * 
	 * @param sbox
	 * @return
	 * @throws PException
	 */
	public ProcessMsg removeMenu(Box sbox) throws PException {

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< removeMenu() > execute Starting");

		sbox.put("query",
				StatementManager.getStatement("TMRSMENU01", "removeMenu"));
		sbox.put("db", "default");

		Logging.debug.println("<" + this.getClass().getName()
				+ ">< removeMenu() > Delete Menu Id:"
				+ sbox.getString("p_menu_id"));

		String input_value[] = { sbox.getString("p_menu_id") };

		resultCode = TMRSDAO.updateTrx(sbox, input_value);

		if (sbox.getString("p_flag").equals("R")) {
			Logging.debug.println("<" + this.getClass().getName()
					+ ">< removeMenu() > removeChildMenu");
			sbox.put("query", StatementManager.getStatement("TMRSMENU01",
					"removeChildMenu"));
			resultCode = TMRSDAO.updateTrx(sbox, input_value);
		}
		Logging.debug.println("< " + this.getClass().getName()
				+ " >< removeMenu() > execute Ended");
		return new ProcessMsg(resultCode);

	}

}
