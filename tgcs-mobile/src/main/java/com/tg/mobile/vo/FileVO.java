package com.tg.mobile.vo;


public class FileVO {
    
    private String name;

    private String value;

    private String fileName;

    private String contentType;

    private String orgFileName;
    
    private String sysFileName;
    
    private String filePath;

    private long fileLength;
    
    public FileVO() {
    }
    
    public FileVO( String name ) {
        this.name = name;
    }
    public void incFileLength(int fileLength ) {
        this.fileLength += fileLength;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getOrgFileName() {
		return orgFileName;
	}

	public void setOrgFileName(String orgFileName) {
		this.orgFileName = orgFileName;
	}

	public String getSysFileName() {
		return sysFileName;
	}

	public void setSysFileName(String sysFileName) {
		this.sysFileName = sysFileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public long getFileLength() {
		return fileLength;
	}

	public void setFileLength(long fileLength) {
		this.fileLength = fileLength;
	}

	@Override
	public String toString() {
		return "FileVO [name=" + name + ", value=" + value + ", fileName=" + fileName + ", contentType=" + contentType
				+ ", orgFileName=" + orgFileName + ", sysFileName=" + sysFileName + ", filePath=" + filePath
				+ ", fileLength=" + fileLength + "]";
	}
    
}
