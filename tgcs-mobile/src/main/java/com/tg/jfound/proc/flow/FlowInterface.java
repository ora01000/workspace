package com.tg.jfound.proc.flow;

import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;

import net.sf.json.JSONObject;


public interface FlowInterface{
    public ProcessMsg executeFlow(Box msg) throws Exception;
}
