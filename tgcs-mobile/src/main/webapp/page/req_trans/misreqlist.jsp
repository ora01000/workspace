<%-- Program info --%>
<%
    /*
     *  Program Type : JSP
     *  Program ID   :
     *
     *  DESC         :
     *  Author       :
     *  Date         :
     *  Update       :
     */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Map" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB00" %>
<%@ page import="com.tg.mobile.util.DateUtils" %>
<%@ page import="net.sf.json.JSONObject" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>

<!-- 선언부 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/custom.tld"%>

<%
	int gap = 3 ;

	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    Box sessionbox = CollectionUtility.getBoxWithSession(request);
    String user_status = sessionbox.getString("U_STATUS");
    
    String start_date = ("".equals(sessionbox.getString("SEARCH_GAP"))?"":sessionbox.getString("SEARCH_GAP"));
    String end_date =  ("".equals(start_date)?"":DateUtils.getFormattedDateAdd(start_date, "yyyy-MM-dd", "yyyy-MM-dd", 2, 0, 0 ));
    
    String user_id = "";
    String rtn_code= "";
    String rtn_msg = "";
    String err_msg = "";
    
	String today = DateUtils.getToday("yyyy-MM-dd");
    if("".equals(sessionbox.getString("SEARCH_GAP"))) start_date = DateUtils.getFormattedDateAdd(today, "yyyy-MM-dd", "yyyy-MM-dd", -(gap-1), 0, 0 );
    if("".equals(sessionbox.getString("SEARCH_GAP")))  end_date = today;
    
    sessionbox.put("START_DATE", start_date);
    sessionbox.put("END_DATE", end_date);

    if(user_status == null){
    }else{

        rtn_code = sessionbox.getString("returnCode");
        rtn_msg = sessionbox.getString("returnMsg");

        if(user_status.equals("N") || "".equals(user_status)){
        	String URL = request.getContextPath()+ "/page/login/login.jsp";
			response.sendRedirect(URL);
        }else{
            err_msg = rtn_code+": "+rtn_msg;
        }
    }
    
    TGMOB00 mob00 = new TGMOB00(); 
    JSONObject requestList = mob00.getUserMisRequestList(sessionbox);

    //String [] dayWeek = new String[7];
    //String [] dayWeekName = {"월","화","수","목","금","토","일"};
    //for(int i = 0 ; i < 7 ; i++)
    //	dayWeek[i] = DateUtils.getFormattedDateAdd(start_date, "yyyyMMdd", "yyyyMMdd", i, 0, 0 );

%>
<!DOCTYPE html>
<html>
<head>

	<title>Enterprise Solution Team</title>
	<META HTTP-EQUIV="Refresh" CONTENT="300;URL=./misreqlist.jsp?SEARCH_3DAY=<%=sessionbox.getString("SEARCH_3DAY")%>">

<style type="text/css">
 <!--
.button1{
border:1px solid #393B3E;-webkit-box-shadow: #737373 0px 1px 5px ;-moz-box-shadow: #737373 0px 1px 5px ; box-shadow: #737373 0px 1px 5px ; -webkit-border-radius: 23px; -moz-border-radius: 23px;border-radius: 23px;font-size:14px;font-family:arial, helvetica, sans-serif; padding: 10px 20px 10px 20px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(10,10,10,0.5);font-weight:bold; color: #8C8C8C;
 background-color: #515459; background-image: -webkit-gradient(linear, left top, left bottom, from(#515459), to(#3B3C40));
 background-image: -webkit-linear-gradient(top, #515459, #3B3C40);
 background-image: -moz-linear-gradient(top, #515459, #3B3C40);
 background-image: -ms-linear-gradient(top, #515459, #3B3C40);
 background-image: -o-linear-gradient(top, #515459, #3B3C40);
 background-image: linear-gradient(to bottom, #515459, #3B3C40);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#515459, endColorstr=#3B3C40);
}

.button1:hover{
 border:1px solid #393B3E;
 background-color: #3B3C40; background-image: -webkit-gradient(linear, left top, left bottom, from(#3B3C40), to(#202124));
 background-image: -webkit-linear-gradient(top, #3B3C40, #202124);
 background-image: -moz-linear-gradient(top, #3B3C40, #202124);
 background-image: -ms-linear-gradient(top, #3B3C40, #202124);
 background-image: -o-linear-gradient(top, #3B3C40, #202124);
 background-image: linear-gradient(to bottom, #3B3C40, #202124);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#3B3C40, endColorstr=#202124);
}

.button2{
border:1px solid #CCCCCC;-webkit-box-shadow: #FEFFFF 0px 1px 1px ;-moz-box-shadow: #FEFFFF 0px 1px 1px ; box-shadow: #FEFFFF 0px 1px 1px ; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 6px 6px 6px 6px; text-decoration:none; display:inline-block;text-shadow: 0px 1px 0 rgba(255,255,255,1);font-weight:bold; color: #4A4A4A;
 background-color: #F7F5F6; background-image: -webkit-gradient(linear, left top, left bottom, from(#F7F5F6), to(#DDDDDD));
 background-image: -webkit-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -moz-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -ms-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: -o-linear-gradient(top, #F7F5F6, #DDDDDD);
 background-image: linear-gradient(to bottom, #F7F5F6, #DDDDDD);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#F7F5F6, endColorstr=#DDDDDD);
}

.button2:hover{
 border:1px solid #ADADAD;
 background-color: #E0E0E0; background-image: -webkit-gradient(linear, left top, left bottom, from(#E0E0E0), to(#BDBBBC));
 background-image: -webkit-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -moz-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -ms-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: -o-linear-gradient(top, #E0E0E0, #BDBBBC);
 background-image: linear-gradient(to bottom, #E0E0E0, #BDBBBC);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#E0E0E0, endColorstr=#BDBBBC);
}

* { 
	margin: 0; 
	padding: 0; 
}
body { 
	#font: 14px/1.4 Georgia, Serif;
	font: 68.5%/1.3 Helvetica, sans-serif; 
}
#page-wrap {
	margin: 10px;
}

#fontSize {
	font-size:14px;
}
a {
	margin: 10px 0;
	font-size:12px; 
}

	/* 
	Generic Styling, for Desktops/Laptops 
	*/
	table { 
		width: 100%; 
		border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white;
		text-align:center; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px;
		border: 1px solid #ccc; 
		text-align: center; 
	}
	
	 /* 기본 설정*/
     /* nav tag */
      nav ul{padding-top:10px;}                     /*  상단 여백 10px  */
      nav ul li {
         display:inline;                         /*  세로나열을 가로나열로 변경 */
         #border-left:1px solid #999;             /* 각 메뉴의 왼쪽에 "|" 표시(분류 표시) */
         font:bold 12px Dotum;                     /* 폰트 설정 - 12px의 돋움체 굵은 글씨로 표시 */
         padding:0 20px;                         /* 각 메뉴 간격 */
     }
      nav ul li:first-child{border-left:none;}     /* 메뉴 분류중 제일 왼쪽의 "|"는 삭제    
	-->
</style>	  
<script type="text/javascript">
</script>
	
</head>

<body>

<% 
	String prev_gap = DateUtils.getFormattedDateAdd(start_date, "yyyy-MM-dd", "yyyy-MM-dd", -gap, 0, 0 );
	String next_gap = DateUtils.getFormattedDateAdd(end_date, "yyyy-MM-dd", "yyyy-MM-dd", 1, 0, 0 );
    
%>

<div align=center>

	<!-- /header -->
	<nav>
        <ul>
            <li><a class="button1" href="${pageContext.request.contextPath}/page/main/main.jsp" title="홈">홈</a></li>
            <li>
			<a class="button2" href="${pageContext.request.contextPath}/page/req/misreqlist.jsp?SEARCH_GAP=<%=prev_gap %>" title="이전">이전</a> | <%=start_date %> ~ <%=end_date %>
			| <a class="button2" href="${pageContext.request.contextPath}/page/req/misreqlist.jsp?SEARCH_GAP=<%=next_gap %>" title="다음">다음</a>
			</li>
            <li><a class="button1" href="${pageContext.request.contextPath}/page/req/input.jsp" title="등록">등록</a></li>
        </ul>    
    </nav>  
	
    <!-- /header -->
    
	<!--  Contents  -->
    <div>
		<c:set var="requestList" value="<%=requestList %>" />
		<table width="1024px">
         <thead>
		 <tr>
         <th width="2%" >요청일시</th>
         <th width="2%" >사이트</th>
         <th width="1%" >요청자</th>
         <th width="3%" >요청건</th>
         <th width="1%" >진행상태</th>
         <th width="2%" >처리일시</th>
         <th width="1%" >담당자</th>
         <th width="1%" >지원형태</th>
         <th width="1%" >서비스형태</th>
         <th width="4%" >처리내용</th>
		 </tr>
		 </thead>
		 <c:forEach var="result" items="${requestList.DATA_LIST}" varStatus="status" >
		 <c:if test="${result.CHK eq 0}"> 
		 	<c:set value="style='color: #ff0000; font-weight: bold;font-size: 1em;'" var="tg_style" />
		 </c:if>
         <tr>
         	<td ${tg_style} >${result.REQ_DATE}</td>
         	<td ${tg_style} >
         	<a href="${pageContext.request.contextPath}/page/req/detail.jsp?REQ_ID=${result.REQ_ID}&MOB_ID=${result.MOB_ID}" data-rel="external">${result.REQ_SITE}</a>
         	</td>
         	<td  ${tg_style} >${result.REQ_USER}</td>
         	<td  ${tg_style} >${result.REQ_TITLE}</td>
         	
	    	<c:choose>
		    <c:when test="${result.MOB_ID ne null}">
		    <td  ${tg_style} >
			    <c:choose>
				    <c:when test="${result.REQ_STS eq null || result.REQ_STS == '' }">
				      미정
			    	</c:when>
			    	<c:otherwise>
			    	${result.REQ_STS_NAME}
			    	</c:otherwise>
			    </c:choose>
		    </td>
         	<td  ${tg_style} >${result.REQ_COMP_DATE}</td>
			<td  ${tg_style} >${result.MOB_USER_NAME}</td>
         	<td  ${tg_style} >
         		<c:choose>
				    <c:when test="${result.SUP_TYPE == 'IS' || result.SUP_TYPE == 'RS' || result.SUP_TYPE == 'PP' }">
         			<a href="">${result.SUP_TYPE_NAME}</a>
         			</c:when>
         			<c:otherwise>
         			${result.SUP_TYPE_NAME}
         			</c:otherwise>
         		</c:choose>
         	</td>
         	<td  ${tg_style} >${result.SVC_TYPE_NAME}</td>
         	</c:when>
         	<c:otherwise>
         	<td  ${tg_style} >
         		<c:choose>
				    <c:when test="${result.MOB_STS eq null || result.MOB_STS == ''}">
				      미정
			    	</c:when>
			    	<c:otherwise>
			    	${result.MOB_STS_NAME}
			    	</c:otherwise>
			    </c:choose>
         	</td>
         	<td  ${tg_style} >${result.REQ_COMP_DATE}</td>
         	<td  ${tg_style} >${result.MOB_USER_NAME}</td>
         	<td  ${tg_style} >
         		<c:choose>
				    <c:when test="${result.SUP_TYPE == 'IS' || result.SUP_TYPE == 'RS' || result.SUP_TYPE == 'PP' }">
         			<a href="">${result.SUP_TYPE_NAME}</a>
         			</c:when>
         			<c:otherwise>
         			${result.SUP_TYPE_NAME}
         			</c:otherwise>
         		</c:choose>
         	</td>
         	<td  ${tg_style} >${result.SVC_TYPE_NAME}</td>
	    	</c:otherwise>
	    	</c:choose>
         	
         	<td  ${tg_style} >${result.REQ_DESC}</td>
         </tr>
         </c:forEach>
		</table>
		
    </div>
    <!--  Contents  -->
    
    <!--  Footer -->
	<!--  Footer -->
	    
    
</div>
 
</body>
</html>