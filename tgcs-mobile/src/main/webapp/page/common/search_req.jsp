<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.mobile.cmn.TGMOBCMN" %>
<%@ page import="com.tg.mobile.util.DateUtils" %>
<%@ page import="net.sf.json.JSONObject" %>

<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>

<%
Box search_sessionbox = CollectionUtility.getBoxWithSession(request);
String search_today = DateUtils.getToday("yyyyMMdd");

SimpleDateFormat search_formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
String search_todate = search_formatter.format ( new Date ( (new Date ()).getTime ( ) - (long) ( 1000 * 60 * 60 * 24 * 7) ) );

//TGMOBCMN search_cmn = new TGMOBCMN(); 
//JSONObject search_userList = search_cmn.getUserListOfDept(search_sessionbox);
%>
<!-- TODO: This should become an external panel so we can add input to markup (unique ID) -->

<div data-role="panel" class="jqm-search-panel" data-position="right" data-display="overlay" data-theme="a"> 
	<div class="jqm-search">
		<form  id="search-form" action="${pageContext.request.contextPath}/page/main/list.jsp" method="post" data-ajax="false"
			class="ui-body ui-body-a ui-corner-all">
		
		<div data-role="fieldcontain">
			<label for="SEARCH_USER_NAME" class="select" data-mini="true">담당자 :</label>
			<input type="text" name="SEARCH_USER_NAME" id="SEARCH_USER_NAME" value=""  data-mini="true" />
		</div>
		
		<div data-role="fieldcontain">
			<label for="S_DATE" data-mini="true">시작 :</label>
             <input type="text" name="S_DATE" id="S_DATE" value="<%=search_todate%>">
		</div>
		
		<div data-role="fieldcontain">
			<label for="E_DATE" data-mini="true">종료 :</label>
             <input type="text" name="E_DATE" id="E_DATE" value="<%=search_today%>">
		</div>

		<!-- 		
		<div data-role="fieldcontain">
			<label for="S_DATE" class="select" data-mini="true">시작:</label>
			<input type="text" id="S_DATE" data-role="date" data-mini="true" value="<%=search_today%>" >
		</div>
		
		<div data-role="fieldcontain">
			<label for="E_DATE" class="select" data-mini="true">종료:</label>
			<input type="text" id="E_DATE" data-role="date" data-mini="true" value="<%=search_today%>" >
		</div>
		 -->
		
		<div data-role="fieldcontain">
			<label for="SEARCH_MOB_DESC" class="select" data-mini="true">내용:</label>
			<input type="text" name="SEARCH_MOB_DESC" id="SEARCH_MOB_DESC" value=""  data-mini="true" />
		</div>
		
		<div class="errorTxt"></div>
		
		<div data-role="fieldcontain">
		<fieldset class="ui-grid-a">
	    <div class="ui-block-a"><input type="submit" value="조회" data-theme="a"></div>
	    <div class="ui-block-b"><input type="reset" value="Reset" data-theme="b"></div>
		</fieldset>
		</div>
		
		</form>
  
	</div>
</div><!-- /panel -->
