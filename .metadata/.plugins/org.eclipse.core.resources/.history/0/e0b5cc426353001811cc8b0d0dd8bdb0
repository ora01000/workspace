package com.tg.wls.startup;

import java.util.Date;
import java.util.Vector;

import javax.management.Attribute;
import javax.management.ObjectName;

public class PurgeServerThread implements Runnable {
	
	private static int PURGE_INTERVAL_MS = 1000 * 60;	// default 60sec
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		if(System.getenv("PURGE_INTERVAL_MS") != null)
		{
			PURGE_INTERVAL_MS = Integer.parseInt(System.getenv("PURGE_INTERVAL_MS"));
		}
		
		while(true)
		{
			try
			{
				Thread.sleep(PURGE_INTERVAL_MS);
				purge();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Purge managed server tagged with UNKNOWN state
	 * @throws Exception
	 */
	public void purge() throws Exception
	{
		ObjectName[] serverRT = getServerLifeCycleRuntimes();
		Vector<String> unknownServers = new Vector<String>();
		
		int length = (int) serverRT.length;
		for (int i = 0; i < length; i++) 
		{
			String name = (String) OpenShiftStartup.domainRuntimeServer.getAttribute(serverRT[i], "Name");
			String state = (String) OpenShiftStartup.domainRuntimeServer.getAttribute(serverRT[i], "State");
			if("UNKNOWN".equals(state))
			{
				System.out.println("[" + OpenShiftStartup.sdf.format(new Date()) + "][PURGE] Server " + name + " is [" + state + "], is marked as remove...");
				unknownServers.add(name);
			}
		}
		
		try
		{
			if(unknownServers.size() > 0)
			{
				ObjectName domainConfigRoot = startEditSession();
			
				destroyServers(domainConfigRoot, unknownServers);
				
				save();
				activate();
			}
		}
		catch(Exception e)
		{
			cancel();
		}
	}
	
	
	/**
	 * destroy Managed server via JMX
	 * @param domainConfigRoot
	 * @param unknownServerList
	 * @throws Exception
	 */
	public void destroyServers(ObjectName domainConfigRoot, Vector<String> unknownServerList) throws Exception
	{
//		ObjectName cfgMgr = (ObjectName)OpenShiftStartup
//				.editServer.getAttribute(OpenShiftStartup.editService, "ConfigurationManager");
//		
		for(int i = 0; i < unknownServerList.size(); i++)
		{
			String mserverName = unknownServerList.get(i);
			ObjectName mserver = (ObjectName)OpenShiftStartup
					.editServer.invoke(domainConfigRoot,
							"lookupServer",
							new Object[] { mserverName },
							new String[] { "java.lang.String" });
			
			if(mserver == null)
				throw new Exception("Server " + mserverName + " does not exist!");
			else
			{
				if(OpenShiftStartup.DEBUG)
					System.out.println("[" + OpenShiftStartup.sdf.format(new Date()) + "][PURGE] Server " + mserverName + " will be deleted...");
				
				OpenShiftStartup.editServer.setAttribute(mserver, new Attribute("Cluster", null));
				OpenShiftStartup.editServer.setAttribute(mserver, new Attribute("Machine", null));
				OpenShiftStartup.editServer.invoke(domainConfigRoot,
						"destroyServer",
						new Object[] { mserver },
						new String[] { ObjectName.class.getName() });
				
				if(OpenShiftStartup.DEBUG)
					System.out.println("[" + OpenShiftStartup.sdf.format(new Date()) + "][PURGE] Server " + mserverName + " is deleted successfully!!");
			}
		}
	}
	
	/**
	 * start edit session
	 * @return
	 * @throws Exception
	 */
	public ObjectName startEditSession() throws Exception 
	{
		ObjectName cfgMgr = (ObjectName)OpenShiftStartup
				.editServer.getAttribute(OpenShiftStartup.editService, "ConfigurationManager");
		ObjectName domainConfigRoot = (ObjectName)OpenShiftStartup
				.editServer.invoke(cfgMgr,
						"startEdit", 
						new Object[] { new Integer(60000),  new Integer(120000) },
						new String[] { "java.lang.Integer", "java.lang.Integer" });
		
		if(domainConfigRoot == null)
		{
			throw new Exception("Cannot obtain edit lock...");
		}
		
		return domainConfigRoot;			
	}
	
	/**
	 * save the changes
	 * @throws Exception
	 */
	public void save() throws Exception
	{
		ObjectName cfgMgr = (ObjectName)OpenShiftStartup
				.editServer.getAttribute(OpenShiftStartup.editService, "ConfigurationManager");
		OpenShiftStartup.editServer.invoke(cfgMgr,
						"save", 
						null,
						null);
	}
	
	public void cancel() throws Exception
	{
		ObjectName cfgMgr = (ObjectName)OpenShiftStartup
				.editServer.getAttribute(OpenShiftStartup.editService, "ConfigurationManager");
		OpenShiftStartup.editServer.invoke(cfgMgr,
						"cancel", 
						null,
						null);
	}
	
	
	/**
	 * activate configuration
	 * @return
	 * @throws Exception
	 */
	public ObjectName activate() throws Exception
	{
		ObjectName cfgMgr = (ObjectName)OpenShiftStartup
				.editServer.getAttribute(OpenShiftStartup.editService, "ConfigurationManager");
		
		ObjectName task = (ObjectName)OpenShiftStartup
				.editServer.invoke(cfgMgr,
						"activate",
						new Object[] { new Long(120000) },
						new String[] { "java.lang.Long" });
		return task;
	}
	
	public ObjectName[] getServerConfigurations() throws Exception
	{
		ObjectName domainConfigMBean = (ObjectName)OpenShiftStartup.editServer.getAttribute(OpenShiftStartup.editService, "DomainConfiguration");
		return (ObjectName[]) OpenShiftStartup
				.editServer
				.getAttribute(domainConfigMBean, "Servers");
				
	}
	
	public ObjectName[] getServerLifeCycleRuntimes() throws Exception
	{
		ObjectName domainRuntimeMBean = (ObjectName)OpenShiftStartup.domainRuntimeServer.getAttribute(OpenShiftStartup.domainRuntimeService, "DomainRuntime");
		return (ObjectName[]) OpenShiftStartup
				.domainRuntimeServer
				.getAttribute(domainRuntimeMBean, "ServerLifeCycleRuntimes");
	}
	
	public ObjectName[] getServerRuntimes() throws Exception 
	{
		return (ObjectName[]) OpenShiftStartup
				.domainRuntimeServer
				.getAttribute(OpenShiftStartup.domainRuntimeService, "ServerRuntimes");
	}

	public void printServerConfigurations() throws Exception
	{
		ObjectName[] serverCF = getServerConfigurations();
		System.out.println("got server configurations");
		int length = (int)serverCF.length;
		if(OpenShiftStartup.DEBUG)
		{
			for(int i = 0; i < length; i++)
			{
				String name = (String)OpenShiftStartup.editServer.getAttribute(serverCF[i], "Name");
				System.out.println("Server Configuration name: " + name);
			}
		}
	}
	
	public void printNameAndState() throws Exception 
	{
		ObjectName[] serverRT = getServerLifeCycleRuntimes();
		System.out.println("got server runtimes");
		int length = (int) serverRT.length;
		if(OpenShiftStartup.DEBUG)
		{
			for (int i = 0; i < length; i++) 
			{
				String name = (String) OpenShiftStartup.domainRuntimeServer.getAttribute(serverRT[i], "Name");
				String state = (String) OpenShiftStartup.domainRuntimeServer.getAttribute(serverRT[i], "State");
				System.out.println("Server name: " + name + ".   Server state: " + state);
			}
		}
	}

}
