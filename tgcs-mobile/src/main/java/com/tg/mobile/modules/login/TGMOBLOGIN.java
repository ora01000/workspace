package com.tg.mobile.modules.login;

import javax.servlet.http.HttpSession;

import com.tg.jfound.dao.DaoHandler;
import com.tg.jfound.dao.sql.StatementManager;
import com.tg.jfound.exception.PException;
import com.tg.jfound.logging.Logging;
import com.tg.jfound.proc.component.FlowComponent;
import com.tg.jfound.proc.msg.ProcessMsg;
import com.tg.jfound.vo.box.Box;
import com.tg.jfound.vo.box.VectorBox;
import com.tg.mobile.dao.TMRSDAO;

public class TGMOBLOGIN extends FlowComponent{
	
	public TGMOBLOGIN(){}

    public void userLogin(String U_ID, String U_PWD, HttpSession httpsession)throws PException{
    	Logging.debug.println("< TGMOBLOGIN >< userLogin() > User Login EMAIL ID["+ U_ID+"]");
    	
    	Box sbox = new Box();
    	
    	// Logging.dev.println(query);
        sbox.put("query",StatementManager.getStatement("TGMOBLOGIN", "login"));

        sbox.put("db","default");
        String input_value[] = {U_ID , U_PWD};
        if(U_ID ==null){
        	httpsession.setAttribute("U_ID"       ,  U_ID                     );
        	httpsession.setAttribute("U_STATUS"   ,  "N"                         );
        	httpsession.setAttribute("returnCode" , "N10030"                     );
        	httpsession.setAttribute("returnMsg"  , "세션이 종료되었습니다. 다시 로그인 하세요."      );
        }else if(U_ID.equals("")){
        	httpsession.setAttribute("U_ID"       ,  U_ID                     );
        	httpsession.setAttribute("U_STATUS"   ,  "N"                         );
        	httpsession.setAttribute("returnCode" , "N10040"                     );
        	httpsession.setAttribute("returnMsg"  , "사용자 아이디를 입력하세요."      );
        }else{

	        DaoHandler dao = new DaoHandler();
	        Box rbox = dao.selectListBox(sbox, input_value);
	        VectorBox vbox = (VectorBox)rbox;
	        
	    	Logging.debug.println("< TGMOBLOGIN >< userLogin() > vobx ["+vbox+"]");
	
	        int userCnt =Integer.parseInt(vbox.getString("dataCount"));
        	if(userCnt == 1){ 
		    	if(U_ID.equals(vbox.getString("U_MAIL",0))||U_PWD.equals(vbox.getString("PASSWORD",0))){
		    		
		    		httpsession.setAttribute("U_ID"    ,  vbox.getString("U_MAIL"    ,0));
		    		httpsession.setAttribute("USER_ID"    ,  vbox.getString("USER_ID"    ,0));
		    		httpsession.setAttribute("U_MAIL"     ,  vbox.getString("U_MAIL"    ,0));
		    		httpsession.setAttribute("PASSWORD"   ,  vbox.getString("PASSWORD"    ,0));
		    		httpsession.setAttribute("U_STATUS"   ,  "Y"                         );
	    		
		    		httpsession.setAttribute("U_NAME", vbox.getString("U_NAME", 0 )) ;
		    		httpsession.setAttribute("U_STATUS", vbox.getString("U_STATUS", 0 )) ;
		    		httpsession.setAttribute("U_DEPT_CODE", vbox.getString("U_DEPT_CODE", 0 )) ;
		    		httpsession.setAttribute("U_POSITION_CODE", vbox.getString("U_POSITION_CODE", 0 )) ;
		    		httpsession.setAttribute("U_JOB_CODE", vbox.getString("U_JOB_CODE", 0 )) ;
		    		httpsession.setAttribute("U_PHONE_NO", vbox.getString("U_PHONE_NO", 0 )) ;
		    		httpsession.setAttribute("U_OFFICE_PHONE_NO", vbox.getString("U_OFFICE_PHONE_NO", 0 )) ;
		    		httpsession.setAttribute("U_ADDRESS", vbox.getString("U_ADDRESS", 0 )) ;
		    		httpsession.setAttribute("U_SYS_AUTH", vbox.getString("U_SYS_AUTH", 0 )) ;
		    		httpsession.setAttribute("U_BIZ_AUTH", vbox.getString("U_BIZ_AUTH", 0 )) ;
	                                                                                              
		    		httpsession.setAttribute("returnCode" , "Y"                             );          
		    		httpsession.setAttribute("returnMsg"  , "환영합니다."                   );
	
		    	}else{
		    		httpsession.setAttribute("U_ID"       ,  U_ID                     );
		    		httpsession.setAttribute("U_STATUS"   ,  "N"                         );
		    		httpsession.setAttribute("returnCode" , "N10010"                     );
		    		httpsession.setAttribute("returnMsg"  , "패스워드를 확인하세요...."      );
		    	}
			}else if(userCnt == 0){
				httpsession.setAttribute("U_ID"       ,  U_ID                     );
				httpsession.setAttribute("U_STATUS"   ,  "N"                         );
				httpsession.setAttribute("returnCode" , "N10020"                     );
				httpsession.setAttribute("returnMsg"  , "존재하지 않는 아이디 입니다...."      );
			}else{
				httpsession.setAttribute("U_ID"       ,  U_ID                     );
				httpsession.setAttribute("U_STATUS"   ,  "N"                         );
				httpsession.setAttribute("returnCode" , "N10030"                     );
				httpsession.setAttribute("returnMsg"  , "중복등록된 사용자 입니다. 관리자에게 문의 하세요...."      );
			}
        }
       	Logging.debug.println("< TGMOBLOGIN >< userLogin() > execute End  Login Code ["+httpsession.getAttribute("returnCode")+"]");      
    }

    
    
    public Box getUserInfo(String USER_ID)throws PException{
    	Logging.debug.println("< TGMOBLOGIN >< getUserInfo() > User Login ID["+ USER_ID+"]");
    	
    	Box sbox = new Box();
    	Box rbox = new Box();
    	
    	// Logging.dev.println(query);
        sbox.put("query",StatementManager.getStatement("TGMOBLOGIN", "login"));

        sbox.put("db","default");
        String input_value[] = {USER_ID};


        DaoHandler dao = new DaoHandler();
        Box box = dao.selectListBox(sbox, input_value);
        VectorBox vbox = (VectorBox)box;
        
        rbox.put("U_ID"    ,  vbox.getString("U_MAIL"    ,0));
        rbox.put("USER_ID"    ,  vbox.getString("USER_ID"    ,0));
        rbox.put("U_MAIL"     ,  vbox.getString("U_MAIL"    ,0));
        rbox.put("PASSWORD"   ,  vbox.getString("PASSWORD"    ,0));
        rbox.put("U_STATUS"   ,  "Y"                         );
        rbox.put("U_NAME", vbox.getString("U_NAME", 0 )) ;
        rbox.put("U_STATUS", vbox.getString("U_STATUS", 0 )) ;
        rbox.put("U_DEPT_CODE", vbox.getString("U_DEPT_CODE", 0 )) ;
        rbox.put("U_POSITION_CODE", vbox.getString("U_POSITION_CODE", 0 )) ;
        rbox.put("U_JOB_CODE", vbox.getString("U_JOB_CODE", 0 )) ;
        rbox.put("U_PHONE_NO", vbox.getString("U_PHONE_NO", 0 )) ;
        rbox.put("U_OFFICE_PHONE_NO", vbox.getString("U_OFFICE_PHONE_NO", 0 )) ;
        rbox.put("U_ADDRESS", vbox.getString("U_ADDRESS", 0 )) ;
        rbox.put("U_SYS_AUTH", vbox.getString("U_SYS_AUTH", 0 )) ;
        rbox.put("U_BIZ_AUTH", vbox.getString("U_BIZ_AUTH", 0 )) ;
		
        Logging.debug.println("< TGMOBLOGIN >< getUserInfo() > execute End");    
        return rbox;
    }
    
    /***
     * userUpdate
     * 
     * @param sbox
     * @return
     * @throws PException
     */
    public ProcessMsg updateMyInfo(Box sbox)throws PException{
    	
        String resultCode = "FAIL";
    	
        Logging.debug.println("<"+this.getClass().getName()+">< updateMyInfo() > execute Starting");


        sbox.put("query",StatementManager.getStatement("TGMOBLOGIN", "updateMyInfo"));
        sbox.put("db","default");

        String input_value[] = {
        	  sbox.getString("p_password"        )
       		 ,sbox.getString("p_u_dept_code"    )
       		 ,sbox.getString("p_u_position_code"    )
       		 ,sbox.getString("p_u_job_code"         )
       		 ,sbox.getString("p_u_phone_no"         )
       		 ,sbox.getString("p_u_office_phone_no"  )
       		 ,sbox.getString("p_u_address"          )
       		 ,sbox.getString("p_user_id"         )
            };
        
        resultCode = TMRSDAO.updateTrx(sbox, input_value);

        Logging.debug.println("< "+this.getClass().getName()+" >< updateMyInfo() > execute Ended");
        return new ProcessMsg(resultCode);

    }

}
