package com.tg.wls.startup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class OpenShiftStartup {
	
	public static final boolean DEBUG = false;
	
	public String serverType;
	public String adminserverUrl;
	public String protocol;
	public String hostname;
	public String portNumber;
	public String wlsAdmin;
	public String wlsPasswd;
	
	public static MBeanServerConnection connection;
	public static JMXConnector connector;
	public static MBeanServer domainRuntimeServer;
	public static MBeanServer runtimeServer;
	public static MBeanServer editServer;
	
	public static ObjectName domainRuntimeService;
	public static ObjectName runtimeService;
	public static ObjectName editService;

	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	static {
		try
		{
			InitialContext ctx = new InitialContext();
			domainRuntimeServer = (MBeanServer)ctx.lookup("java:comp/jmx/domainRuntime");
			runtimeServer = (MBeanServer)ctx.lookup("java:comp/jmx/runtime");
			editServer = (MBeanServer)ctx.lookup("java:comp/jmx/edit");
			runtimeService = new ObjectName("com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean");
			domainRuntimeService = new ObjectName("com.bea:Name=DomainRuntimeService,Type=weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean");
			editService = new ObjectName("com.bea:Name=EditService,Type=weblogic.management.mbeanservers.edit.EditServiceMBean");
		}
		catch(MalformedObjectNameException e) {
			e.printStackTrace();
		}
		catch(NamingException ne)
		{
			ne.printStackTrace();
		}
	}
	
	public Thread psthread, monthread;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		OpenShiftStartup oss = new OpenShiftStartup();
//		try
//		{
//			oss.initConnection();
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		
		if(oss.serverType.equalsIgnoreCase("adminserver"))
		{
			oss.psthread = new Thread(new PurgeServerThread());
			oss.psthread.start();
			oss.monthread = new Thread(new MonServerThread());
			oss.monthread.start();
			
		}
		else if(oss.serverType.equalsIgnoreCase("managedserver"))
		{
			oss.monthread = new Thread(new MonServerThread());
			oss.monthread.start();
		}
		
	}
	
	public OpenShiftStartup()
	{
		serverType = System.getenv("SERVER_TYPE");
		adminserverUrl = System.getenv("ADMINSERVER_URL");
		wlsAdmin = System.getenv("WLS_ADMIN");
		wlsPasswd = System.getenv("WLS_PASSWD");
		protocol = "t3";
		hostname = adminserverUrl.substring(0, adminserverUrl.indexOf(':'));
		portNumber = adminserverUrl.substring(adminserverUrl.indexOf(':') + 1);
	}
	
	public void initConnection() throws IOException, MalformedURLException, NamingException
	{
//		String protocol = "t3";
//		Integer portInteger = Integer.valueOf(portNumber);
//		int port = portInteger.intValue();
//		String jndiroot = "/jndi/";
//		String mserver = "weblogic.management.mbeanservers.domainruntime";
//		JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + mserver);
//
//		Hashtable<String, Object> h = new Hashtable<String, Object>();
//		h.put(Context.SECURITY_PRINCIPAL, wlsAdmin);
//		h.put(Context.SECURITY_CREDENTIALS, wlsPasswd);
//		h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
//		h.put("jmx.remote.x.request.waiting.timeout", new Long(10000));
//		connector = JMXConnectorFactory.connect(serviceURL, h);
//		connection = connector.getMBeanServerConnection();	
		

	}

}
