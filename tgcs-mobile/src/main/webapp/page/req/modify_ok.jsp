<%-- Program info --%>
<%
	/*
	 *  Program Type : JSP
	 *  Program ID   :
	 *
	 *  DESC         :
	 *  Author       :
	 *  Date         :
	 *  Update       :
	 */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="com.tg.jfound.logging.Logging" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.jfound.web.BaseAbstractServlet" %>
<%@ page import="com.tg.mobile.modules.mob.TGMOB00" %>
<%@ page import="net.sf.json.JSONObject" %>

<%

String rtnUri = "/";

try {
	HttpSession httpsession = request.getSession(true);
	Box box = CollectionUtility.getBoxWithSession(request);
	
	Enumeration params = request.getParameterNames();
	while (params.hasMoreElements()){
	    String name = (String)params.nextElement();
	    Logging.dev.println(name + " : " +request.getParameter(name));
	}
	
/*	
REQ_ID
REQ_DATE
REQ_SITE
REQ_USER
REQ_TITLE
REQ_STS
REQ_COMP_DATE
REQ_ENG
REQ_DESC
SUP_TYPE
SUP_DESC
CREATE_DATE
CREATE_USER
UPDATE_DATE
UPDATE_USER
SVC_TYPE
DEPT_CODE
*/
	
	box.put("REQ_DATE", request.getParameter("REQ_DATE"));
	box.put("REQ_ID", request.getParameter("REQ_ID"));
	box.put("REQ_SITE", request.getParameter("REQ_SITE"));
	box.put("REQ_USER", request.getParameter("REQ_USER"));
	box.put("REQ_TITLE", request.getParameter("REQ_TITLE"));
	box.put("REQ_STS", request.getParameter("REQ_STS"));
	box.put("REQ_COMP_DATE", request.getParameter("REQ_COMP_DATE"));
	box.put("REQ_ENG", request.getParameter("REQ_ENG"));
	box.put("REQ_DESC", request.getParameter("REQ_DESC"));
	box.put("SUP_TYPE", request.getParameter("SUP_TYPE"));
	box.put("SUP_DESC", request.getParameter("SUP_DESC"));
	box.put("SVC_TYPE", request.getParameter("SVC_TYPE"));
	
	box.put("MOB_ID", request.getParameter("MOB_ID"));
	
	Logging.debug.println("<login ok> USER_ID:"	+ box.getString("U_ID"));
	Logging.debug.println("<login ok> U_STATUS:" + box.getString("U_STATUS"));
	String U_STATUS = box.getString("U_STATUS");

	if ((U_STATUS == null) || (U_STATUS.equals("N"))
			|| (U_STATUS.equals(""))) {
		Logging.dev.println("Login.. JSP.... Callled");
		//rtnUri = req.getContextPath() + "/page/login/login.jsp";
		rtnUri = "/page/login/login.jsp";
	} else if("Y".equals(U_STATUS)) {
		TGMOB00 mob00 = new TGMOB00();
		JSONObject jsonObj = mob00.modifyRequest(box);
		//rtnUri = req.getContextPath() + "/page/login/login.jsp";
		System.out.println("==============>"+jsonObj);
		rtnUri = "/page/req/detail.jsp?REQ_ID="+ jsonObj.get("key") + "&MOB_ID="+ jsonObj.get("mobkey");
	}

	Logging.debug.println("<input ok> catchService Rtn URL ["
			+ rtnUri + "]\n");

	String URL = request.getContextPath()+ rtnUri;
	response.sendRedirect(URL);
	
} catch (Exception e) {
	e.printStackTrace();
	Logging.err
			.println("<login ok> Catche the Occurred Exception While User IP ["
					+ request.getRemoteAddr() + "]\n" + e);
	//this.printErr(request, response, "<login ok> Exception occurred " + e, e);
}
%>