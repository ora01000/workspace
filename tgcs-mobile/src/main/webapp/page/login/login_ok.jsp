<%-- Program info --%>
<%
	/*
	 *  Program Type : JSP
	 *  Program ID   :
	 *
	 *  DESC         :
	 *  Author       :
	 *  Date         :
	 *  Update       :
	 */
%>

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="com.tg.jfound.logging.Logging" %>
<%@ page import="com.tg.jfound.vo.box.Box" %>
<%@ page import="com.tg.jfound.vo.box.CollectionUtility" %>
<%@ page import="com.tg.jfound.web.BaseAbstractServlet" %>
<%@ page import="com.tg.mobile.modules.login.TGMOBLOGIN" %>

<%

String rtnUri = "/";

try {
	HttpSession httpsession = request.getSession(true);
	Box sessionbox = CollectionUtility.getBoxWithSession(request);

	String C_ID =  request.getParameter("C_ID");
	String C_PWD = request.getParameter("C_PWD");
	String C_DEPT = request.getParameter("C_DEPT");
	String maintain = request.getParameter("maintain");
	String taction = request.getParameter("taction");
	
	
	Enumeration params = request.getParameterNames();
	while (params.hasMoreElements()){
	    String name = (String)params.nextElement();
	    Logging.dev.println(name + " : " +request.getParameter(name));
	}

	
	Logging.debug.println("<LoginAdapter> maintain:" + maintain);
	Logging.debug.println("<LoginAdapter> taction:" + taction);
	if (taction == null)
		taction = "LOGIN";

	Logging.debug.println("<LoginAdapter> catchService Initiated USER_ID:"	+ C_ID);
	Logging.debug.println("<LoginAdapter> catchService Initiated by [" + request.getRemoteAddr() + "]\n");

	Logging.debug.println("<LoginAdapter> USER_ID:"	+ sessionbox.getString("C_ID"));
	Logging.debug.println("<LoginAdapter> U_STATUS:" + sessionbox.getString("C_STATUS"));

	String U_STATUS = sessionbox.getString("U_STATUS");

	if ((U_STATUS == null) || (U_STATUS.equals("N"))
			|| (U_STATUS.equals(""))) {

		Logging.dev.println("Login.. JSP.... Callled");

		TGMOBLOGIN login = new TGMOBLOGIN();
		login.userLogin(C_ID, C_PWD, httpsession);
		//rtnUri = req.getContextPath() + "/page/login/login.jsp";
		rtnUri = "/page/login/login.jsp";
		
		if("on".equals(maintain)){
			//System.out.println("COOKIES INPUT ===============================================>");
			/* 쿠키 설정 */
    		Cookie c_u_id = new Cookie("C_ID", sessionbox.getString("C_ID")) ;//  쿠키에 지정한다
    		Cookie c_u_pwd = new Cookie("C_PWD", sessionbox.getString("C_PWD")) ;//  쿠키에 지정한다
    		Cookie c_maintain = new Cookie("C_MT", sessionbox.getString("maintain")) ;//  쿠키에 지정한다
		    
    		c_u_id.setComment("USER ID") ;// 쿠키에 설명을 추가한다
    		c_u_pwd.setComment("USER PWD") ;// 쿠키에 설명을 추가한다
    		c_maintain.setComment("MAINTAIN") ;// 쿠키설정여부
		    
    		c_u_id.setMaxAge(60*60*24*7) ;// 쿠키 유효기간을 설정한다. 초단위 : 60*60*24= 1일
    		c_u_pwd.setMaxAge(60*60*24*7) ;// 쿠키 유효기간을 설정한다. 초단위 : 60*60*24= 1일
    		c_maintain.setMaxAge(60*60*24*7) ;// 쿠키 유효기간을 설정한다. 초단위 : 60*60*24= 1일

    		c_u_id.setPath("/");
    		c_u_pwd.setPath("/");
    		c_maintain.setPath("/");
    		
		    response.addCookie(c_u_id) ;// 응답헤더에 쿠키를 추가한다.
		    response.addCookie(c_u_pwd) ;// 응답헤더에 쿠키를 추가한다.
		    response.addCookie(c_maintain) ;// 응답헤더에 쿠키를 추가한다.
			/* 쿠키 설정 */
			
		}

	} else {
		if (taction.equals("LOGIN")) {
			Logging.dev.println("Login... Go");
			//rtnUri = req.getContextPath() + "/page/main/main.jsp";
			rtnUri = "/page/main/main.jsp";
			
		} else {
			Logging.dev.println("Log-out...");

			Logging.debug.println("<LoginAdapter> Log-out");
			httpsession.invalidate();
			//rtnUri = req.getContextPath() + "/page/login/login.jsp";
			
			rtnUri = "/page/login/login.jsp";
			
			/* 쿠키 해제 */
			Cookie[] cookies = request.getCookies();        // 요청정보로부터 쿠키를 가져온다.
			for(int i = 0 ; i<cookies.length; i++){         // 쿠키 배열을 반복문으로 돌린다.
				//System.out.println(" REMOVE " + cookies[i].getDomain() + " - " + cookies[i].getName());
				if("C_ID".equals(cookies[i].getName()) 
						|| "C_PWD".equals(cookies[i].getName())
						|| "C_MT".equals(cookies[i].getName())
						){
					cookies[i].setMaxAge(0);                        // 특정 쿠키를 더 이상 사용하지 못하게 하기 위해서는 쿠키의 유효시간을 만료시킨다.
					cookies[i].setPath("/");
					response.addCookie(cookies[i]);            // 해당 쿠키를 응답에 추가(수정)한다.
				}
			}
			 /*쿠키 해제 */
		}
	}

	Logging.debug.println("<LoginAdapter> catchService Rtn URL ["
			+ rtnUri + "]\n");
	//printJsp(request, response, rtnUri);
	String URL = request.getContextPath()+ rtnUri;
	//System.out.println(U_STATUS + " -- " + taction + " -- " + URL );
	response.sendRedirect(URL);
	
} catch (Exception e) {
	e.printStackTrace();
	Logging.err
			.println("<LoginAdapter> Catche the Occurred Exception While User IP ["
					+ request.getRemoteAddr() + "]\n" + e);
	//this.printErr(request, response, "<LoginAdapter> Exception occurred " + e, e);
}
%>